<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\http\Requests;
use Response;
use DB;
use File;
use Redirect;
use Storage;

class StockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mainStock = DB::select('SELECT products.name_th,stocks.unit_1,stocks.unit_2,stocks.unit_3,stocks.products_id FROM stocks JOIN products ON products.id = stocks.products_id WHERE 1');
        return view('pages.stocks.viewMainStocks',compact('mainStock'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req)
    {
        try {
            DB::table('stocks')
            ->where('products_id',$req->products_id)
            ->update([
                  'unit_1'=> $req->unit_1,
                  'unit_2'=> $req->unit_2,
                  'unit_3'=> $req->unit_3,
                ]
            );
            $status = "success";
            $result_insert = 'บันทึกข้อมูลเรียบร้อย';
            return response()->json($status);
        }catch (\Exception $e) {
            $result_insert = 'กรุณากรอกข้อมูลให้ครบถ้วน';
            $status = "error =".$e->getMessage();;
        return response()->json($status);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function addProductsStock(Request $req){
        $seclectProducts = DB::select('SELECT * FROM `products` WHERE id='.$req->id);
        try {
              DB::table('stocks')->insert( 
                  [
                      'products_id' => $seclectProducts[0]->id, 
                      'unit_1' => $seclectProducts[0]->quantity,
                  ]
              );
              $status = "success";
              $result_insert = 'บันทึกข้อมูลเรียบร้อย';
          }catch (\Exception $e) {
              $teste = $e->getMessage();
              $result_insert = 'กรุณากรอกข้อมูลให้ครบถ้วน';
              $status = "error =".$teste;
          }
        return response()->json($status);
    }
  
    public function deleteProductsStock(Request $req){
        try {
              DB::delete('DELETE FROM `stocks` WHERE products_id ='.$req->id);
              $status = "success";
              $result_insert = 'ลบข้อมูลเรียบร้อย';
        }catch (\Exception $e) {
              $teste = $e->getMessage();
              $result_insert = 'ลบข้อมูลไม่ได้';
              $status = "error =".$teste;
        }
        return response()->json($status);
    }

    public function addNewImportMainStocksQty(Request $req){
      //dd($req->import_file);
      if (!empty($req->import_file)) {
          Storage::disk('local')->put('fileDataStockImport/file.csv',file_get_contents($req->import_file->getRealPath()));             
            $filepath = storage_path("app/fileDataStockImport/file.csv");
            // print_r($filepath);//พาสไฟล์
            // Reading file
            $file = fopen($filepath,"r");
            $prdindata_arr = array();
            $i = 0;
            // While = เข้าไปที่ไฟล์ csv ทีละบรรทัด เริ่มจากคอลัมภ์ทางด้านซ้ายไปขวา
            while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {         
                $num = count($filedata);
                 //$num = จำนวนคอลัมภ์

                 // Skip first row (Remove below comment if you want to skip the first row)
                if($i == 0){
                    $i++;
                    continue; 
                }
                // echo '<br>'.$filedata[1];//คอลัมม
                // dd();
                if (!empty($filedata[1])) {
                    $checkId = DB::select('SELECT COUNT(*) as num FROM products WHERE id ='.$filedata[1]);
                    if ($checkId[0]->num == 1) {
                        DB::table('stocks')->insert( 
                        [
                            'products_id' => $filedata[0], 
                            "unit_1" => $filedata[1],
                            "unit_2" => $filedata[3],
                            "unit_3" => $filedata[5],
                        ]
                        );
                    }
                }
                $i++;
            }
            fclose($file);
            $status['success'] = 'อัฟโหลดไฟล์เรียบร้อย';
            return view('pages.stocks.viewMainStocks',$status);
        }

        if (empty($req->import_file)) {
            $status['error'] = 'อัฟโหลดไม่ได้ กรุณาเลือกไฟล์ที่ต้องการก่อน';
            return view('pages.stocks.viewMainStocks',$status);
        }
    }
}
