<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\http\Requests;
use Response;
use DB;
// use Intervention\Image\Facades\Image;
// use Intervention\Image\Exception\NotReadableException;
use Image;
use File;
use Redirect;
use Storage;
// use Illuminate\Support\Facades\Storage;
use Auth;

class BranchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posAdmin = array(1,10);
        if(in_array(Auth::user()->is_admin, $posAdmin)){
            $allbranch = DB::select('SELECT * FROM `branch` WHERE 1');
        }else{
            $allbranch = DB::select('SELECT branch.* FROM users JOIN pos_user_branch ON users.id = pos_user_branch.user_id JOIN branch ON branch.id = pos_user_branch.branch_id WHERE user_id = '.Auth::id());
        }
        return view('pages.branch.mainBranch',compact('allbranch'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $req)
    {
        // phpinfo();
        // dd();
        // dd($req->all());

        if($req->hasFile('shopLogo')){

            $path = '/shopLogo/';
            $destinationPath = public_path($path);
            // echo $destinationPath;
            // dd();
            $image = $req->file('shopLogo');
            $allowed_extensions = Array('jpg','png','jpeg');    
            if(in_array($image->getClientOriginalExtension(),$allowed_extensions)){
                $imageTime = uniqid();
                $imagename = $imageTime.'.'.$image->getClientOriginalExtension();       
                $photo = Image::make($image->getRealPath())->orientate();
                if (!file_exists($destinationPath)) {
                    mkdir($destinationPath, 0777, true);
                }
                $width = $photo->width();
                if($width > 250){
                    $width = 250;
                }
                $photo->resize($width, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath.'/'.$imagename);
            }
        }else{
            $imagename = 'no';
        }
        try {
            DB::table('branch')->insert( 
                [
                    'shop_logo' => $imagename, 
                    "shop_name" => $req->branchName,
                    "shop_address" => $req->branchAddress,
                    "shop_tax_id" => $req->branchTax,
                    "shop_phone" => $req->branchPhoneNumber,
                ]
            );

            $status = "success";
        }catch (\Exception $e) {
            $teste = $e->getMessage();
            $status = "error =".$teste;
        }
        // echo $status;
        // dd();
       	return redirect()->route('branch');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req)
    {
        try {
    		DB::table('branch')
	    	->where('id',$req->id)
	    	->update([
	                'shop_name' => $req->shopName, 
	                'shop_address' => $req->address,
	                'shop_tax_id' => $req->shoptax,
	                'shop_phone' => $req->shopphone
	            ]);
    	 	$status = "success";
            $result_insert = 'บันทึกข้อมูลเรียบร้อย';
        }catch (\Exception $e) {
            $teste = $e->getMessage();
            $result_insert = 'กรุณากรอกข้อมูลให้ครบถ้วน';
            $status = "error";
        }
		return response()->json($req->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $req)
    {
        $delete = DB::select('DELETE FROM `branch` WHERE id ='.$req->id);
    }
}
