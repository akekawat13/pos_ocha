<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use DB;
use File;
use Redirect;
use Storage;
use Illuminate\Support\Str;
use Auth;

class StocksTransferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $req)
    {
        try{
			DB::table('stocks_transfer')->insert( 
	            [
	            	'branch_id_src' => 0,
	            	'branch_id_des' => $req->barnch,
	            	'transfer_type_id' => 1,
	                'transferNumber' => $req->transferNumber,
	                'dateTo' => $req->dateTo,
                    'user_id' => $req->user_id,
                    'noteTransfer' => $req->noteTransfer,
                    //'nameRecipient' => $req->dateTo,
	            ]
	        );
	        $status['stock_transfer'] = "success";
	        $result_insert = 'บันทึกข้อมูลเรียบร้อย';
		}catch (\Exception $e) {
            $teste = $e->getMessage();
            $result_insert = 'กรุณากรอกข้อมูลให้ครบถ้วน';
            $status['stock_transfer'] = "error =".$teste;
        }
        return view('pages.stocks.stocksBranch');
    }
    public function addTransferBranch(Request $req){
    	//dd($req->all());
        DB::table('stocks_transfer')
        ->where('id',$req->stocksTransferId)
        ->update( 
            [
                'status_id'=> $req->status_id,
            ]
        );
    	foreach ($req->productsID as $key => $value) {
    		//echo $value;
    		for ($i=1; $i <= 3; $i++) { 
    			//echo $te;
    			$unit[$value]['unit'.$i] =  eval('return $'.'req->transferUnit_'.$i.'_'.$value.';');    			
    		}
            for ($u=1; $u <= 3; $u++) { 
                //echo $te;
                $unit[$value]['sunit'.$u] =  eval('return $'.'req->summary_'.$u.'_'.$value.';');                
            }
    	}	
		foreach ($unit as $key => $value) {
			try {
	            DB::table('stocks_transfer_detail')->insert( 
	                [
	                    'stocksTransferId' => $req->stocksTransferId,
	                    'dateTo' => $req->dateTo,
	                    'productsId' => $key,
	                    'transferUnit_1' => $value['unit1'],
	                    'transferUnit_2' => $value['unit2'],
	                    'transferUnit_3' => $value['unit3'],
	                ]
	            );
	            $status[$key] = "success stocks_transfer_detail";
	            $result_insert = 'บันทึกข้อมูลเรียบร้อย';
	        }catch (\Exception $e) {
	            $teste = $e->getMessage();
	            $result_insert = 'กรุณากรอกข้อมูลให้ครบถ้วน';
	            $status[$key] = "error stocks_transfer_detail";
	        }
            try {
                DB::table('stocks')
                    ->where('products_id',$key)
                    ->update( 
                        [
                            'unit_1' => $value['sunit1'],
                            'unit_2' => $value['sunit2'],
                            'unit_3' => $value['sunit3'],
                        ]
                );
                $status[$key] = "success stocks";
            }catch (\Exception $e) {
                $teste = $e->getMessage();
                $status[$key] = "error stocks =".$teste;
            }
		}
        // print_r($status);
        // dd();
		return view('pages.stockTransfer.listStockTransfer');	
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return view('pages.stockTransfer.listStockTransfer');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = DB::select('SELECT stocks_transfer.*,stocks_transfer_detail.*,products.name_th,stocks.*,users.name FROM stocks_transfer JOIN stocks_transfer_detail ON stocks_transfer.id = stocks_transfer_detail.stocksTransferId JOIN products ON products.id = stocks_transfer_detail.productsId JOIN stocks ON stocks.products_id = stocks_transfer_detail.productsId JOIN users ON users.id = stocks_transfer.user_id WHERE stocks_transfer.id = '.$id);
        return view('pages.stockTransfer.viewStockTransfer',compact('data'));
    }
    public function transferPaperReport($id){
        $data_report = DB::select('SELECT stocks_transfer.id AS stocks_transfer_ID,stocks_transfer.branch_id_src AS Assignee,stocks_transfer.transferNumber,stocks_transfer.dateTo,stocks_transfer.noteTransfer,stocks_transfer.status_id,stocks_transfer.receiveDate,stocks_transfer.user_id AS SenderName,stocks_transfer.nameRecipient AS RecipientName,branch.*,stocks_transfer_detail.*,products.name_th AS productsName,products.cost_price1 AS price1
            FROM `stocks_transfer` JOIN branch ON stocks_transfer.branch_id_des = branch.id JOIN stocks_transfer_detail ON stocks_transfer.id = stocks_transfer_detail.stocksTransferId JOIN products ON products.id = stocks_transfer_detail.productsId
            WHERE stocks_transfer.id ='.$id);
        return view('pages.stockTransfer.transferPaperReport',compact('data_report'));
    }
    public function pdfTransferPaper($id){
        $data = DB::select('SELECT stocks_transfer.id AS stocks_transfer_ID,stocks_transfer.branch_id_src AS Assignee,stocks_transfer.transferNumber,stocks_transfer.dateTo,stocks_transfer.noteTransfer,stocks_transfer.status_id,stocks_transfer.receiveDate,stocks_transfer.user_id AS SenderName,stocks_transfer.nameRecipient AS RecipientName,branch.*,stocks_transfer_detail.*,products.name_th AS productsName,products.cost_price1 AS price1
            FROM `stocks_transfer` JOIN branch ON stocks_transfer.branch_id_des = branch.id JOIN stocks_transfer_detail ON stocks_transfer.id = stocks_transfer_detail.stocksTransferId JOIN products ON products.id = stocks_transfer_detail.productsId
            WHERE stocks_transfer.id ='.$id);
        return view('pages.stockTransfer.pdfTransferPaper',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function stockTransfer(){
		$maxid = DB::select('SELECT max(transferNumber) as id FROM  stocks_transfer');
        foreach ($maxid as $key => $value) {
            $last_id = $value->id;
        }
        if ($maxid != null) {
            $iddata = Str::substr($last_id, 4);
        } else {
            $iddata = null;
        }
        if ($iddata == null) {
            $number = '0001';
        } else if ($iddata < 9) {
            $iddata += 1;
            $items = (string)$iddata;
            $number =  0 . 0 . 0  . $items;
        } else if ($iddata < 99) {
            $iddata += 1;
            $items = (string)$iddata;
            $number =  0 . 0 . $items;
        } else if ($iddata < 999) {
            $iddata += 1;
            $items = (string)$iddata;
            $number =  0 . $items;
        } else if ($iddata < 9999) {
            $iddata += 1;
            $items = (string)$iddata;
            $number =  $items;
        } else {
            echo 'full id';
        }
        // echo $number;
        // dd();
		return view('pages.stocks.transferStocks',compact('number'));
	}
}
