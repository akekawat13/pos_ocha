<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Response;
use Illuminate\Support\Facedes\input;
use App\http\Requests;
use DB;

class PartnerSystemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customer = DB::select('SELECT * FROM `partnersystem` WHERE 1');
		return view('pages.partnerSystem.partnerSystem',compact('customer'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $req)
    {
        try{
            DB::table('partnersystem')->insert(
                [
                    'shopName' => $req->shopName,
                    'address' => $req->address,
                    'contactNumber' => $req->contactNumber,
                    'contactName' => $req->contactName
                ]
            );
            $data['result'] = "success";
        }catch (\Exception $e) {
            $teste = $e->getMessage();
            $data['result'] = "error".$teste;
        }
		return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req)
    {
        try {
            DB::table('partnersystem')
            ->where('id',$req->id)
            ->update([
                'shopName' => $req->shopName,
                'address' => $req->address,
                'contactNumber' => $req->contactNumber,
                'contactName' => $req->contactName
            ]);
            $data['result'] = "success";
        }catch (\Exception $e) {
            $teste = $e->getMessage();
            $data['result'] = "error".$teste;
        }
		return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $req)
    {
        try {
            $delete = DB::select('DELETE FROM `partnersystem` WHERE id ='.$req->id);
            $data['result'] = "success";
        }catch (\Exception $e) {
            $teste = $e->getMessage();
            $data['result'] = "error".$teste;
        }
		return response()->json($data);
    }
}
