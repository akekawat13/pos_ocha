<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\http\Requests;
use DB;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mainCategory = DB::select('SELECT * FROM `category` WHERE 1');
		return view('pages.category.category',compact('mainCategory'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_main_category(Request $req)
    {
        try {
			DB::table('category')
		        ->insert([
		                'name' => $req->name,
		            ]
		    );
		    $status['stt'] = "success";
            $data = DB::select('SELECT * FROM `category` WHERE id = (SELECT MAX(id) FROM `category`)');
			$status['max'] = $data[0];
        }catch (\Exception $e) {
            $status['stt'] = "error";
        }
        return response()->json($status);
    }
    public function create_sub_category(Request $req)
    {
        try {
			DB::table('category_sub')
		        ->insert([
		                'category_id' => $req->category_id, 
		                "name" => $req->category_sub,
		            ]
		    );

		    $status_2['stt'] = "success";
            $category_sub = DB::select('SELECT * FROM `category_sub` WHERE id = (SELECT MAX(id) FROM `category_sub`)');
			$status_2['dataCategorySub'] = $category_sub[0];
        }catch (\Exception $e) {
            $status_2['stt'] = "error";
        }	
        return response()->json($status_2);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy_main_category(Request $req)
    {
        $deleteCategory = DB::delete('DELETE FROM `category` WHERE id ='.$req->id);
        return response()->json($deleteCategory);
    }
}
