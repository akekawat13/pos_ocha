<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;

// reference the Dompdf namespace
use Dompdf\Dompdf;
use PDF;
use Illuminate\Support\Facades\App;

class SalePageController extends Controller
{
    public function index()
    {
        $data['products'] = DB::select('SELECT products.id,products.name_th,products.price1,products.category_id,category.name AS name_category FROM `products` JOIN category ON category.id = products.category_id WHERE 1');
        $data['category'] = DB::select('SELECT * FROM `category` WHERE 1');
        $data['option_size'] = DB::select('SELECT * FROM `option_sizes` WHERE 1');
        $data['option_topping'] = DB::select('SELECT * FROM `option_toppings` WHERE 1');
        $data['transction_data_all'] = DB::select('SELECT * FROM `transaction_detail_temp` WHERE trn_id = (SELECT id FROM `transaction_temp` WHERE usr_id = '.Auth::id().' AND status = 1)');
        $data['transction_data_money'] = DB::select('SELECT SUM(vat) AS sum_vat,SUM(total) AS sum_total FROM `transaction_detail_temp` WHERE trn_id = (SELECT id FROM `transaction_temp` WHERE usr_id = '.Auth::id().' AND status = 1)');
        $data['transction_data_trn_doc_no'] = DB::select('SELECT trn_doc_no FROM `transaction_temp` WHERE usr_id = '.Auth::id().' AND status = 1');
        return view('pages.sale_pages.index',compact('data'));
    }

    public function sort_category_submit(Request $req){
        if($req->category_id === "all"){
            $all_pro_select_category = DB::select('SELECT products.id,products.name_th,products.name_en,products.productImg,products.price1,products.category_id,category.name AS name_category FROM `products` JOIN category ON category.id = products.category_id WHERE 1');
        }else{
            $all_pro_select_category = DB::select('SELECT products.id,products.name_th,products.name_en,products.productImg,products.price1,products.category_id,category.name AS name_category FROM `products` JOIN category ON category.id = products.category_id WHERE products.category_id = '.$req->category_id.'');
        }
        return response()->json($all_pro_select_category);
    }
    //search pro
    public function search_products(Request $req){
        $data_pro = DB::select('SELECT products.id,products.name_th,products.name_en,products.productImg,products.price1,products.category_id,category.name AS name_category FROM `products` JOIN category ON category.id = products.category_id WHERE products.name_th LIKE "%'.$req->name_pro.'%"');
        return response()->json($data_pro);
    }

    public function add_transaction_temp(Request $req){
        //$b['org'] = $req->data_toppings;
        // array to string
        //$a[1] = json_encode($req->data_toppings);

        if($req->data_toppings == ''){
            $data_toppings = "-";
            $topping_price = 0;
        }else{
            $data_toppings = json_encode($req->data_toppings);
            foreach($req->data_toppings as $key => $value){
                $b[] = intval($value['price']);
            }
            $topping_price = array_sum($b);
        }
        //$cal_vat = (($req->pro_price+$req->size_price+$topping_price)*7)/100;
        //$sum_vats = round($cal_vat);
        $sum_total = ($req->pro_price+$topping_price);
        $trn_doc_no = date('Y').date('m').date('d').rand(0,999999);
        try {
            DB::table('transaction_temp')
            ->updateOrInsert(
                [
                    'usr_id' => Auth::id(),
                    'status' => 1,
                ],
                [
                    'trn_doc_no' => $trn_doc_no,
                    'created_at' => date('Y/m/d H:i:s'),
                ]
            );
            $result['status_1'] = "success";
        }catch (\Exception $e) {
            $result['status_1'] = "error =".$e->getMessage();
        }
        $transaction_temp_id = DB::select('SELECT MAX(id) AS trn_id FROM `transaction_temp` WHERE usr_id = '.Auth::id().'');
        $transaction_temp_trn_doc_no = DB::select('SELECT trn_doc_no FROM `transaction_temp` WHERE id = '.$transaction_temp_id[0]->trn_id.'');
        try {
            DB::table('transaction_detail_temp')->insert(
                [
                    'trn_id' => $transaction_temp_id[0]->trn_id,
                    'pro_id' => $req->pro_id,
                    'pro_name' => $req->pro_name,
                    'pro_price' => $req->pro_price,
                    //'size_name' => $req->size_name,
                    //'size_price' => $req->size_price,
                    'toppings' => $data_toppings,
                    'pro_quantity' => 1,
                    //'vat' => $sum_vats,
                    'total' => $sum_total,
                    'created_at' => date('Y/m/d H:i:s'),
                ]
            );

            $result['status_2'] = "success";
            $result['pro_charge'] = DB::select('SELECT SUM(total) AS sum_total FROM `transaction_detail_temp` WHERE trn_id = (SELECT id FROM `transaction_temp` WHERE usr_id = '.Auth::id().' AND status = 1)');
            $result['pro_id_trn_id'] = DB::select('SELECT trn_id,pro_id FROM `transaction_detail_temp` WHERE trn_id = (SELECT id FROM `transaction_temp` WHERE usr_id = '.Auth::id().' AND status = 1)');
            $result['trn_doc_no'] = $transaction_temp_trn_doc_no[0]->trn_doc_no;
        }catch (\Exception $e) {
            $teste = $e->getMessage();
            $result['status_2'] = "error =".$teste;
        }
        return response()->json($result);
        //return response()->json($transaction_temp_trn_doc_no);
    }

    //Clear Ticket
    public function clear_ticket_al(Request $req){
        //$transaction_temp_id = DB::select('SELECT id FROM `transaction_temp` WHERE trn_doc_no = '.$req->trn_doc_no.'');
        try {
            DB::table('transaction_detail_temp')->where('trn_id',$req->trn_id)->delete();
            DB::table('transaction_temp')->where('id',$req->trn_id)->delete();
            $result['status'] = "success";
        }catch (\Exception $e) {
            $teste = $e->getMessage();
            $result['status'] = "error =".$teste;
        }
        $result['id_suspend'] = DB::select('SELECT id FROM `transaction_temp` WHERE usr_id = '.Auth::id().' AND status = 0');
        return response()->json($result);
    }

    //Save Ticket
    public function save_ticket_onuser(Request $req){
        try {
            DB::table('transaction_temp')->where('id',$req->trn_id)->update(['status' => 0]);
            $result['status'] = "success";
        }catch (\Exception $e) {
            $teste = $e->getMessage();
            $result['status'] = "error =".$teste;
        }
        $result['id_suspend'] = DB::select('SELECT id FROM `transaction_temp` WHERE usr_id = '.Auth::id().' AND status = 0');
        return response()->json($result);
    }
    public function save_ticket_onuser_open(Request $req){
        $data_transaction_tamp['data_all'] = DB::select('SELECT * FROM `transaction_temp` WHERE usr_id = '.$req->usr_id.' AND status = 0');
        return response()->json($data_transaction_tamp);
    }
    public function open_sale(Request $req){
        DB::table('transaction_temp')->where('id',$req->trn_id)->update(['status' => 1]);
        $data_transaction_tamp['data_all'] = DB::select('SELECT * FROM `transaction_detail_temp` WHERE trn_id = (SELECT id FROM `transaction_temp` WHERE id = '.$req->trn_id.' AND status = 1)');
        $data_transaction_tamp['data_money'] = DB::select('SELECT SUM(vat) AS sum_vat,SUM(total) AS sum_total FROM `transaction_detail_temp` WHERE trn_id = (SELECT id FROM `transaction_temp` WHERE id = '.$req->trn_id.' AND status = 1)');
        return response()->json($data_transaction_tamp);
    }
    public function check_suspend(Request $req){
        $result['id_suspend'] = DB::select('SELECT id FROM `transaction_temp` WHERE usr_id = '.$req->usr_id.' AND status = 0');
        return response()->json($result);
    }

    //Submit Charge
    public function submit_charge(){
        $id_trn_doc_no = DB::select('SELECT trn_doc_no FROM `transaction_temp` WHERE usr_id = '.Auth::id().' AND status = 1');
        // $pro_id = DB::select('SELECT pro_id,SUM(pro_quantity) AS sum_qty FROM `transaction_detail_temp` WHERE trn_id = (SELECT id FROM transaction_temp WHERE trn_doc_no = "'.$id_trn_doc_no[0]->trn_doc_no.'") GROUP BY pro_id');

        try {
            DB::table('transaction_temp')->where('usr_id',Auth::id())->where('trn_doc_no',$id_trn_doc_no[0]->trn_doc_no)->update(['status' => 2]);
        $result['status'] = "success";
        }catch (\Exception $e) {
            $teste = $e->getMessage();
            $result['status'] = "error =".$teste;
        }

        $id_trn_doc_no_2 = DB::select('SELECT id,trn_doc_no FROM `transaction_temp` WHERE usr_id = '.Auth::id().' AND status = 2');
        $pro_id_qty = DB::select('SELECT pro_id,pro_quantity FROM `transaction_detail_temp` WHERE trn_id = '.$id_trn_doc_no_2[0]->id);
        // echo "<pre>";
        // echo "tran temp id =".$id_trn_doc_no_2[0]->id.'<br>';
        // print_r($pro_id_qty);
        // echo "</pre>";
        foreach($pro_id_qty as $key => $value){
            $src_qty_pro = DB::select('SELECT quantity FROM `products` WHERE id = '.$value->pro_id.'');
            $up_qty = $src_qty_pro[0]->quantity - $value->pro_quantity;
            try {
                DB::table('products')->where('id',$value->pro_id)->update(['quantity' => $up_qty]);
                $result['status'] = "success";
            }catch (\Exception $e) {
                $teste = $e->getMessage();
                $result['status'] = "error =".$teste;
            }
            //echo $result['status']."<br>";
        }

        //dd();

        $data_transaction_tamp['data_all'] = DB::select('SELECT * FROM `transaction_detail_temp` WHERE trn_id = (SELECT id FROM `transaction_temp` WHERE usr_id = '.Auth::id().' AND status = 2)');
        $data_transaction_tamp['data_money'] = DB::select('SELECT SUM(vat) AS sum_vat,SUM(total) AS sum_total FROM `transaction_detail_temp` WHERE trn_id = (SELECT id FROM `transaction_temp` WHERE usr_id = '.Auth::id().' AND status = 2)');
        $data_transaction_tamp['data_trn_doc_no'] = DB::select('SELECT trn_doc_no FROM `transaction_temp` WHERE usr_id = '.Auth::id().' AND status = 2');

        // echo "<pre>";
        // //echo $id_trn_doc_no[0]->trn_doc_no.'<br>';
        // print_r($data_transaction_tamp['data_money']);
        // // print_r(json_decode($data_transaction_tamp['data_all'][0]->toppings));
        // // var_dump(json_decode($data_transaction_tamp['data_all'][1]->toppings));
        // echo "</pre>";
        // dd();
        return view('pages.sale_pages.charge',compact('data_transaction_tamp'));
    }

    //Payment
    public function charge_completed(Request $req){
        //dd($req->all());
        try {
            DB::table('transaction_temp')->where('id',$req->trn_id)->where('trn_doc_no',$req->trn_doc_no)->where('status',2)->update(['status' => 3]);
            $result['status'] = "success";
        }catch (\Exception $e) {
            $teste = $e->getMessage();
            $result['status'] = "error =".$teste;
        }
        return response()->json($result);
    }

    //pro_ticket_option_save
    public function pro_ticket_option_save(Request $req){
        $submit_quantity = intval($req->pro_quantity);
        $select_pro_quantity = DB::select('SELECT pro_quantity,pro_price,size_price,toppings FROM `transaction_detail_temp` WHERE trn_id = '.$req->trn_id.' AND pro_id = '.$req->pro_id.'');
        if($select_pro_quantity[0]->toppings != "-"){
            $data_toppings = json_decode($select_pro_quantity[0]->toppings);
            for($i = 0; $i < count($data_toppings);$i++){
                $b[] = intval($data_toppings[$i]->price);
            }
            $topping_price = array_sum($b);
        }else{
            $data_toppings = "-";
            $topping_price = 0;
        }
        $result['pro_quantity_new'] = $select_pro_quantity[0]->pro_quantity + $submit_quantity;
        $cal_vat = ($result['pro_quantity_new'] * $select_pro_quantity[0]->pro_price) + $topping_price + $select_pro_quantity[0]->size_price;
        $result['pro_vat_new'] = round(($cal_vat*7 )/100);
        $result['pro_total_new'] = ($result['pro_quantity_new'] * $select_pro_quantity[0]->pro_price) + $topping_price;
        $result['pro_id'] = $req->pro_id;
        try {
            DB::table('transaction_detail_temp')->where('trn_id',$req->trn_id)->where('pro_id',$req->pro_id)
            ->update([
                'pro_quantity' => $result['pro_quantity_new'],
                //'vat' => $result['pro_vat_new'],
                'total' => $result['pro_total_new']
            ]);
            $result['status'] = "success";
        }catch (\Exception $e) {
            $teste = $e->getMessage();
            $result['status'] = "error =".$teste;
        }
        return response()->json($result);
    }

    //Print Receipt
    public function print_receipt(Request $req){
        //dd($req->all());
        $data['receipt'] = DB::select('SELECT * FROM `receipt` WHERE id = (SELECT MAX(id) FROM `receipt`)');
        $data['data_all'] = DB::select('SELECT * FROM `transaction_detail_temp` WHERE trn_id = '.$req->trn_id.'');
        $data['data_money'] = DB::select('SELECT SUM(vat) AS sum_vat,SUM(total) AS sum_total FROM `transaction_detail_temp` WHERE trn_id = '.$req->trn_id.'');

        $path = public_path('images/'.$data['receipt'][0]->shop_logo);
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data_logo = file_get_contents($path);
        $data['logo'] = 'data:image/' . $type . ';base64,' . base64_encode($data_logo);
        //return view('pages.sale_pages.print_receipt_org_80mm',compact('data'));
        // return view('pages.sale_pages.print_receipt_org_58mm',compact('data'));
        return view('pages.sale_pages.print_receipt',compact('data'));
        dd();
        $loadview_blade = view('pages.sale_pages.print_receipt_org_58mm',compact('data'))->render();
        require_once('../vendor/autoload.php');

        // $mpdf = new \Mpdf\Mpdf();

        $defaultConfig = (new \Mpdf\Config\ConfigVariables())->getDefaults();
        $fontDirs = $defaultConfig['fontDir'];

        $defaultFontConfig = (new \Mpdf\Config\FontVariables())->getDefaults();
        $fontData = $defaultFontConfig['fontdata'];

        $mpdf = new \Mpdf\Mpdf([
            'fontDir' => array_merge($fontDirs, [
                __DIR__ . '/tmp',
            ]),
            'fontdata' => $fontData + [
                'garuda' => [
                    'N' => 'Garuda.ttf',
                    'B' =>  'Garuda-Bold.ttf',
                ]
            ],
            'default_font' => 'garuda',
            'default_font_size' => 12,
            'margin_top' => 0,
            'margin_left' => 0,
            'margin_right' => 0,
            'margin_bottom' => 0,
            'padding' => 0,
            'mirrorMargins' => true,
        ]);
        $mpdf->SetDisplayMode('fullwidth');
        $mpdf->WriteHTML($loadview_blade);
        $mpdf->Output('Myreceipt.pdf');

        return view('pages.sale_pages.print_test02');
    }
}
