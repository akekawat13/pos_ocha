<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\http\Requests;
use Response;
use DB;
use Image;
use File;
use Redirect;
use Storage;
use Auth;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mainCategory = DB::select('SELECT * FROM `category` WHERE 1');
        return view('pages.products.addProducts',compact('mainCategory'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $req)
    {
        // dd($req->all());
        if($req->hasFile('productImg')){
            $path = '/productImages/';
            $destinationPath = public_path($path);
            $image = $req->file('productImg');
            $allowed_extensions = Array('jpg','png','jpeg');    
            if(in_array($image->getClientOriginalExtension(),$allowed_extensions)){
                $imageTime = uniqid();
                $imagename = $imageTime.'.'.$image->getClientOriginalExtension();
                $photo = Image::make($image->getRealPath())->orientate();
                if (!file_exists($destinationPath)) {
                    mkdir($destinationPath, 0777, true);
                }
                $width = $photo->width();
                if($width > 250){
                    $width = 250;
                }
                $photo->resize($width, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath.'/'.$imagename);
            }
        }else{
            $imagename = 'no';
        }
        
        try {
            DB::table('products')->insert( 
                [
                    'productImg' => $imagename, 
                    "name_th" => $req->productNameTh,
                    "name_en" => $req->productNameEn,
                    "size" => $req->productSize,
                    "fda_number" => $req->fdaNumber,
                    "barcode" => $req->barcode,
                    "cost_price1" => $req->wholesalePrice1,
                    "cost_price2" => $req->wholesalePrice2,
                    "cost_price3" => $req->wholesalePrice3,
                    "cost_price4" => $req->wholesalePrice4,
                    "cost_price5" => $req->wholesalePrice5,
                    "price1" => $req->retailPrice1,
                    "price2" => $req->retailPrice2,
                    "price3" => $req->retailPrice3,
                    "price4" => $req->retailPrice4,
                    "price5" => $req->retailPrice5,
                    'category_id'=> $req->mainCategory,
                    'category_sub_id' => $req->category_sub,
                ]
            );
            $status = "success";
            $result_insert = 'บันทึกข้อมูลเรียบร้อย';
        }catch (\Exception $e) {
            $teste = $e->getMessage();
            $result_insert = 'กรุณากรอกข้อมูลให้ครบถ้วน';
            $status = "error =".$teste;
        }
        //echo $status;
        // if ($status == "success") {
        //     return view('pages.products.productsLists',compact('status','result_insert'));
        // }
        // if ($status == "error") {
        //     return view('pages.products.productsLists',compact('status','result_insert'));
        // }
        return redirect()->route('productsLists');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $productsLists = DB::select('SELECT * FROM `products` WHERE 1');
        return view('pages.products.productsLists',compact('productsLists'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $req)
    {
        $productDetail = DB::select('SELECT * FROM `products` WHERE id='.$req->id);
        return response()->json($productDetail);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $req)
    {
        $editProduct = DB::select('SELECT * FROM `products` WHERE id='.$req->id);
        return response()->json($editProduct);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $req)
    {
        // dd($req->all());
        if (empty($req->editProductImg)) {
            try {
                DB::table('products')
                ->where('id',$req->id)
                ->update([
                        // 'productImg' => $imagename,
                        "name_th" => $req->productNameTh,
                        "name_en" => $req->productNameEn,
                        "size" => $req->productSize,
                        "fda_number" => $req->fdaNumber,
                        "barcode" => $req->barcode,
                        "cost_price1" => $req->wholesalePrice1,
                        "cost_price2" => $req->wholesalePrice2,
                        "cost_price3" => $req->wholesalePrice3,
                        "cost_price4" => $req->wholesalePrice4,
                        "cost_price5" => $req->wholesalePrice5,
                        "price1" => $req->retailPrice1,
                        "price2" => $req->retailPrice2,
                        "price3" => $req->retailPrice3,
                        "price4" => $req->retailPrice4,
                        "price5" => $req->retailPrice5
                    ]
                );
                $status = "success";
                $result_insert = 'บันทึกข้อมูลเรียบร้อย';
            }catch (\Exception $e) {
                $teste = $e->getMessage();
                $result_insert = 'กรุณากรอกข้อมูลให้ครบถ้วน';
                $status = "error =".$teste;
            }
            // echo $status;
            // dd();
            return redirect('productsLists')->with('status', 'แก้ไขข้อมูลเรียบร้อย');
        }
        if (!empty($req->editProductImg)) {
            $image = \DB::table('products')->where('id', $req->id)->first();
            $file= $image->productImg;
            $filename = "{{asset('/productImages/'}}".$file;
            \File::delete($filename);

            if($req->hasFile('editProductImg')){
                $path = '/productImages/';
                $destinationPath = public_path($path);
                $image = $req->file('editProductImg');
                $allowed_extensions = Array('jpg','png','jpeg');    
                if(in_array($image->getClientOriginalExtension(),$allowed_extensions)){
                    $imageTime = uniqid();
                    $imagename = $imageTime.'.'.$image->getClientOriginalExtension();
                    
                    $photo = Image::make($image->getRealPath())->orientate();
                    if (!file_exists($destinationPath)) {
                        mkdir($destinationPath, 0777, true);
                    }
                    $width = $photo->width();
                    if($width > 250){
                        $width = 250;
                    }
                    $photo->resize($width, null, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($destinationPath.'/'.$imagename);
                }
            }
            try {
                DB::table('products')
                ->where('id',$req->id)
                ->update([
                        'productImg' => $imagename, 
                        "name_th" => $req->productNameTh,
                        "name_en" => $req->productNameEn,
                        "size" => $req->productSize,
                        "fda_number" => $req->fdaNumber,
                        "barcode" => $req->barcode,
                        "cost_price1" => $req->wholesalePrice1,
                        "cost_price2" => $req->wholesalePrice2,
                        "cost_price3" => $req->wholesalePrice3,
                        "cost_price4" => $req->wholesalePrice4,
                        "cost_price5" => $req->wholesalePrice5,
                        "price1" => $req->retailPrice1,
                        "price2" => $req->retailPrice2,
                        "price3" => $req->retailPrice3,
                        "price4" => $req->retailPrice4,
                        "price5" => $req->retailPrice5
                    ]
                );
                $status = "success";
                $result_insert = 'บันทึกข้อมูลเรียบร้อย';
            }catch (\Exception $e) {
                $teste = $e->getMessage();
                $result_insert = 'กรุณากรอกข้อมูลให้ครบถ้วน';
                $status = "error =".$teste;
            }
            return redirect('productsLists')->with('status', 'แก้ไขข้อมูลเรียบร้อย !');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $req)
    {
        $deleteProduct = DB::delete('DELETE FROM `products` WHERE id ='.$req->id);
        return response()->json($deleteProduct);
    }

    public function searchCategorySub(Request $req){
        $data_sub_category = DB::select('SELECT * FROM `category_sub` WHERE category_id ='.$req->id);
        return response()->json($data_sub_category);
    }

    public function importProducts(Request $req)
    {
        // dd($req->import_products);
        if (!empty($req->import_products)) {
            Storage::disk('local')->put('fileDataProductsImport/Productsfile.csv',file_get_contents($req->import_products->getRealPath()));
            //ดึงไฟล์มาแสดง
            $filepath = storage_path("app/fileDataProductsImport/Productsfile.csv");
            $file = fopen($filepath,"r");
            // echo $file;
            // dd();
            $prdindata_arr = array();
            $i = 0;
            // While = เข้าไปที่ไฟล์ csv ทีละบรรทัด เริ่มจากคอลัมภ์ทางด้านซ้ายไปขวา
            while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {         
               $num = count($filedata);
               //$num = จำนวนคอลัมภ์

               // Skip first row (Remove below comment if you want to skip the first row)
               if($i == 0){
               $i++;
               continue; 
               }                   
                //For คือเข้าไปทีละคอลัมภ์ในบรรทัดนั้น ๆ 
                for ($c=0; $c < $num; $c++) {
                    $prdindata_arr[$i][] = $filedata[$c];
                }
               $i++;
            }
            fclose($file);
            // echo "header('Content-Type: text/html; charset=utf-8')";
            // echo "<pre>";
            // print_r($prdindata_arr);
            // echo "</pre>";
            // dd();
            foreach ($prdindata_arr as $key => $value) {
                // var_dump($value[0]);
                // echo $value[7];
                // dd();
                if ($value[0] == "") {
                    $barcode = 0;
                }else{
                    $barcode = $value[0];
                }
                if ($value[7] == "") {
                    $category_sub_id = 0;
                }else{
                    $category_sub_id = $value[7];
                }
                if ($value[9] == "") {
                    $fda_number = null;
                }else{
                    $fda_number = $value[9];
                }
                if ($value[12] == "") {
                    $cost_price2 = null;
                }else{
                    $cost_price2 = $value[12];
                }
                if ($value[13] == "") {
                    $cost_price3 = null;
                }else{
                    $cost_price3 = $value[13];
                }
                if ($value[14] == "") {
                    $cost_price4 = null;
                }else{
                    $cost_price4 = $value[14];
                }
                if ($value[15] == "") {
                    $cost_price5 = null;
                }else{
                    $cost_price5 = $value[15];
                }
                if ($value[17] == "") {
                    $price2 = null;
                }else{
                    $price2 = $value[17];
                }
                if ($value[18] == "") {
                    $price3 = null;
                }else{
                    $price3 = $value[18];
                }
                if ($value[19] == "") {
                    $price4 = null;
                }else{
                    $price4 = $value[19];
                }
                if ($value[20] == "") {
                    $price5 = null;
                }else{
                    $price5 = $value[20];
                }     

                DB::table('products')->insert( 
                    [
                        "barcode" => $barcode,
                        "product_code" => $value[1],
                        "productImg" => $value[2],
                        "name_th" => $value[3],
                        "name_en" => $value[4],
                        "size" => $value[5],
                        "category_id" => $value[6],
                        "category_sub_id" => $category_sub_id,
                        "description" => $value[8],
                        "fda_number" => $fda_number,
                        "quantity" => $value[10],
                        "cost_price1" => $value[11],
                        "cost_price2" => $cost_price2,
                        "cost_price3" => $cost_price3,
                        "cost_price4" => $cost_price4,
                        "cost_price5" => $cost_price5,
                        "price1" => $value[16],
                        "price2" => $price2,
                        "price3" => $price3,
                        "price4" => $price4,
                        "price5" => $price5,
                        "tax" => $value[21],
                    ]
                );
                // echo "<pre>";
                // print_r($value);
                // echo "</pre>";
            } 
            // echo "<pre>";
            // print_r($prdindata_arr[1]);
            // echo "</pre>";
            $dataFile['success'] = 'อัฟโหลดไฟล์เรียบร้อย';
            $dataFile['dataProducts'] = $prdindata_arr;
            return view('pages.products.addProducts',$dataFile);
        }
        if (empty($req->import_products)) {
            $dataFile['error'] = 'อัฟโหลดไม่ได้ กรุณาเลือกไฟล์ที่ต้องการก่อน';
            return view('pages.products.addProducts',$dataFile);
        }
    }
}
