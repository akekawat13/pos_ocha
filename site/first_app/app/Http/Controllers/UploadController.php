<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Response;
class UploadController extends Controller
{
    function index(){
    	return view('ajax_upload');
    }

    function action(Request $req)
    {
		$validation = Validator::make($req->all(), [
			'select_file' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048'
		]);
		if($validation->passes()){
			$image = $req->file('select_file');
			$imageTime = uniqid();
            $new_name = $imageTime.'.'.$image->getClientOriginalExtension();
			$image->move(public_path('images'), $new_name);
            $filename = "{{asset('/images/'}}".$new_name;
			return response()->json([
				'message'   => 'Image Upload Successfully',
				'uploaded_image' => '<img src='.$filename.' width="20%" />',
				'class_name'  => 'alert-success',
				'logo_name'  => $new_name,
			]);
		}else{
			return response()->json([
				'message'   => $validation->errors()->all(),
				'uploaded_image' => 'Null',
				'class_name'  => 'alert-danger'
			]);
		}
    }
}
