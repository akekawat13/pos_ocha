//Flot Pie Chart
$(function() {

    var data = [{
        label: "ถั่วอบน้ำผึง",
        data: 21,
        color: "#7f8c8d",
    }, {
        label: "ถั่วอบงา",
        data: 3,
        color: "#d35400",
    }, {
        label: "ถั่วทอด",
        data: 15,
        color: "#2980b9",
    }, {
        label: "ถั่วอบธรรมดา",
        data: 52,
        color: "#27ae60",
    }, {
        label: "ถั่วอบน้ำมัน",
        data: 52,
        color: "#8e44ad",
    }];

    var plotObj = $.plot($("#flot-pie-chart-upsale"), data, {
        series: {
            pie: {
                show: true
            }
        },
        grid: {
            hoverable: true
        },
        tooltip: true,
        tooltipOpts: {
            content: "%p.0%, %s", // show percentages, rounding to 2 decimal places
            shifts: {
                x: 20,
                y: 0
            },
            defaultTheme: false
        }
    });

});

//Flot Pie Chart
$(function() {

    var data = [{
        label: "ถั่วอบน้ำผึง",
        data: 25,
        color: "#7f8c8d",
    }, {
        label: "ถั่วอบงา",
        data: 20,
        color: "#d35400",
    }, {
        label: "ถั่วทอด",
        data: 55,
        color: "#2980b9",
    }, {
        label: "ถั่วอบธรรมดา",
        data: 10,
        color: "#27ae60",
    }, {
        label: "ถั่วอบน้ำมัน",
        data: 52,
        color: "#8e44ad",
    }];

    var plotObj = $.plot($("#flot-pie-chart-downsale"), data, {
        series: {
            pie: {
                show: true
            }
        },
        grid: {
            hoverable: true
        },
        tooltip: true,
        tooltipOpts: {
            content: "%p.0%, %s", // show percentages, rounding to 2 decimal places
            shifts: {
                x: 20,
                y: 0
            },
            defaultTheme: false
        }
    });

});

$("#flot-pie-chart-upsale-2").sparkline([10,15,20,25,30], {
    type: 'pie',
    height: '200px',
    sliceColors: ['#7f8c8d', '#d35400', '#2980b9','#27ae60','#8e44ad']
});
$("#flot-pie-chart-downsale-2").sparkline([30,25,20,15,10], {
    type: 'pie',
    height: '200px',
    sliceColors: ['#16a085', '#2980b9', '#2c3e50','#e74c3c','#95a5a6']
});