<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\HomeController;
use App\Http\Controllers\BranchController;
use App\Http\Controllers\ProductsController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\StockController;
use App\Http\Controllers\StocksTransferController;
use App\Http\Controllers\PartnerSystemController;
use App\Http\Controllers\WholeSaleController;
use App\Http\Controllers\WarehousingController;
use App\Http\Controllers\ReceiptController;
use App\Http\Controllers\UploadController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('auth.login');
// });

Auth::routes();
require __DIR__.'/sale_pages.php';

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
//admin pages
Route::get('admin/home',[HomeController::class, 'adminHome'])->name('admin.home')->middleware('is_admin');

//branch
Route::get('/branch', [BranchController::class, 'index'])->name('branch');
Route::post('/addBranch', [BranchController::class, 'create']);
Route::post('/updateBranch', [BranchController::class, 'update'])->name('updateBranch');
Route::post('/deleteBranch', [BranchController::class, 'destroy'])->name('deleteBranch');
//Category
Route::get('/category', [CategoryController::class,'index']);
Route::post('/addCategory', [CategoryController::class,'create_main_category'])->name('addCategory');
Route::post('/addCategorySub', [CategoryController::class,'create_sub_category'])->name('addCategorySub');
Route::post('/deleteMainCategory', [CategoryController::class,'destroy_main_category'])->name('deleteMainCategory');
//Products
Route::get('/addProducts', [ProductsController::class, 'index']);
Route::post('/create_product', [ProductsController::class, 'create']);
Route::get('/productsLists', [ProductsController::class, 'store'])->name('productsLists');
Route::post('/viewProductDetail', [ProductsController::class, 'show'])->name('viewProductDetail');
Route::post('/editProduct', [ProductsController::class, 'edit'])->name('editProduct');
Route::post('/updateProduct', [ProductsController::class, 'update'])->name('updateProduct');
Route::post('/deleteProduct', [ProductsController::class, 'destroy'])->name('deleteProduct');
Route::post('/importProducts', [ProductsController::class, 'importProducts']);
Route::post('/searchCategorySub', [ProductsController::class, 'searchCategorySub'])->name('searchCategorySub');
Route::get('/productsListsBranch', [ProductsController::class, 'productsListsBranch']);
//Stock
Route::get('/viewMainStocks', [StockController::class ,'index']);
Route::post('/updateMainstocks', [StockController::class ,'update'])->name('updateMainstocks');
Route::post('/addNewMainStocksQty', [StockController::class ,'addNewImportMainStocksQty']);
Route::post('/addProductsStock', [StockController::class,'addProductsStock'])->name('addProductsStock');
Route::post('/deleteProductsStock', [StockController::class,'deleteProductsStock'])->name('deleteProductsStock');
//Stock Transfer
Route::post('/addStocksTransfer', [StocksTransferController::class ,'create']);
Route::post('/addTransferBranch', [StocksTransferController::class ,'addTransferBranch']);
Route::get('/stockTransfer', [StocksTransferController::class ,'stockTransfer']);
Route::get('/listStockTransfer', [StocksTransferController::class ,'store']);
Route::get('/viewStockTransfer/{id}', [StocksTransferController::class ,'show']);
Route::get('/transferPaperReport/{id}', [StocksTransferController::class ,'transferPaperReport']);
Route::get('/pdfTransferPaper/{id}', [StocksTransferController::class ,'pdfTransferPaper']);

//Partner system
Route::get('/partnerSystem', [PartnerSystemController::class ,'index'])->name('partnerSystem');
Route::POST('/addPartnerSystem', [PartnerSystemController::class ,'create'])->name('addPartnerSystem');
Route::POST('/updatePartnerSystem', [PartnerSystemController::class ,'update'])->name('updatePartnerSystem');
Route::POST('/deletePartnerSystem', [PartnerSystemController::class ,'destroy'])->name('deletePartnerSystem');

//wholesale
Route::get('/wholeSale', [wholeSaleController::class ,'index'])->name('wholeSale');

//Warehousing
Route::get('/Warehousing', [WarehousingController::class ,'index'])->name('Warehousing');

//receiptSetting
Route::get('/receiptSetting', [ReceiptController::class ,'index']);
Route::get('/print', [ReceiptController::class ,'print'])->name('print');
Route::post('/branchToreceipt', [ReceiptController::class ,'branchToreceipt']);
Route::post('/addReceipt', [ReceiptController::class ,'create'])->name('addReceipt');
Route::post('/add_img',[ReceiptController::class ,'add_img']);

Route::get('/ajax_upload', [UploadController::class ,'index']);
Route::post('/ajax_upload/action', [UploadController::class ,'action'])->name('ajaxupload.action');
