<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SalePageController;


//sale pages
Route::get('sale_page',[SalePageController::class, 'index'])->name('sale.page');
Route::post('/sort_category_submit', [SalePageController::class ,'sort_category_submit'])->name('sort_category_submit');
Route::post('/add_transaction_temp', [SalePageController::class ,'add_transaction_temp'])->name('add_transaction_temp');
Route::post('/clear_ticket_al', [SalePageController::class ,'clear_ticket_al'])->name('clear_ticket_al');
//save ticket
Route::post('/save_ticket_onuser', [SalePageController::class ,'save_ticket_onuser'])->name('save_ticket_onuser');
Route::post('/save_ticket_onuser_open', [SalePageController::class ,'save_ticket_onuser_open'])->name('save_ticket_onuser_open');
//open sale
Route::post('/open_sale', [SalePageController::class ,'open_sale'])->name('open_sale');
//check suspend
Route::post('/check_suspend', [SalePageController::class ,'check_suspend'])->name('check_suspend');
//search pro
Route::post('/search_products', [SalePageController::class ,'search_products'])->name('search_products');
//pro_ticket_option_save
Route::post('/pro_ticket_option_save', [SalePageController::class ,'pro_ticket_option_save'])->name('pro_ticket_option_save');
//submit charge
Route::get('/submit_charge', [SalePageController::class ,'submit_charge'])->name('submit_charge');
//Payment charge_completed
Route::post('/charge_completed', [SalePageController::class ,'charge_completed'])->name('charge_completed');
//print receript
Route::post('/print_receipt', [SalePageController::class ,'print_receipt'])->name('print_receipt');