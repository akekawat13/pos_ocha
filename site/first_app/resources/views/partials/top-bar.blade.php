<nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0;background-color: #1ab394 !important;">
        <div class="navbar-header">
            {{-- <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a> --}}
            {{-- <form role="search" class="navbar-form-custom" action="search_results.html" style="color: #ffffff !important;">
                <div class="form-group">
                    <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">
                </div>
            </form> --}}
        </div>

        <ul class="nav navbar-top-links navbar-right" style="color: #ffffff !important;">
            <li style="padding: 20px;">
                <span class="m-r-sm welcome-message">Welcome to Peanut Home Admin</span>
            </li>
            <li>
                <a href="{{ route('logout') }}"
                onclick="event.preventDefault();
                document.getElementById('logout-form').submit();" style="color: #ffffff !important;">
                    <i class="fa fa-sign-out"></i> Log out
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </li>
            {{-- <li>
                <a class="right-sidebar-toggle" style="color: #ffffff !important;">
                    <i class="fa fa-tasks"></i>
                </a>
            </li> --}}
        </ul>

</nav>
