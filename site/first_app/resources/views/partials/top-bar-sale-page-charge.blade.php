<nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0;background-color: #020763 !important;width: 100%;">
    <div class="navbar-header">
        {{-- <a class="minimalize-styl-2 btn btn-primary" href="{{ route('sale.page') }}"><i class="fa fa-arrow-left"></i> </a> --}}
    </div>

    <ul class="nav navbar-top-links navbar-right" style="color: #ffffff !important;">
        {{-- <li>
            <a class="right-sidebar-toggle" style="color: #ffffff !important;">
                <i class="fa fa-search"></i>
            </a>
        </li> --}}
        <li>
            <a href="{{ route('logout') }}"
            onclick="event.preventDefault();
            document.getElementById('logout-form').submit();" style="color: #ffffff !important;">
                <i class="fa fa-sign-out"></i> Log out
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </li>
    </ul>
</nav>
