@php
    $posAdmin = array(1,10);
@endphp

<ul class="nav metismenu" id="side-menu">
    <li class="nav-header">
        <div class="dropdown profile-element">
            <img alt="image" class="rounded-circle img-fluid" src="{{asset("img/admin-user/user-login.png")}}" width="20%">
            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                <span class="block m-t-xs font-bold">{{ Auth::user()->name }}</span>
                <span class="text-muted text-xs block">Admin Director <b class="caret"></b></span>
            </a>
            <ul class="dropdown-menu animated fadeInRight m-t-xs">
                <li><a class="dropdown-item" href="{{ route('logout') }}"
                onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">Logout</a></li>
            </ul>
        </div>
        <div class="logo-element">
            <img src="{{asset("img/logo/logo-peanutHome-3.png")}}" width="40%">
        </div>
    </li>

    <li class="sub-menu">
        <a href="{{URL::to('admin/home')}}"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboards</span></a>
    </li>
    <li class="sub-menu">
        <a href="{{URL::to('/branch')}}"><i class="fa fa-th-large"></i> <span class="nav-label">จัดการสาขา</span></a>
    </li>
    @if(in_array(Auth::user()->is_admin, $posAdmin))
        <li class="sub-menu">
            <a href="{{URL::to('/category')}}"><i class="fa fa-object-group"></i> <span class="nav-label">กลุ่มสินค้า</span></a>
        </li>
    @endif
    <li class="sub-menu">
        <a href="#"><i class="fa fa-shopping-cart"></i> <span class="nav-label">สินค้า</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse">
            @if(in_array(Auth::user()->is_admin, $posAdmin))
                <li class="{{ Request::is('addProducts*') ? 'active' : '' }}">
                    <a href="{{URL::to('/addProducts') }}"><i class="fa fa-plus"></i> <span class="nav-label">เพิ่มสินค้า</span></a>
                </li>
                @php
                    $count_P = DB::select('SELECT COUNT(*) as num FROM products');
                @endphp
                <li class="">
                    <a href="{{URL::to('/productsLists') }}"><i class="fa fa-eye"></i> <span class="nav-label">รายการสินค้า</span><span class="label label-danger float-right">{{ $count_P[0]->num }}</span></a>
                </li>
                @else
                    @php
                        $count_PB = DB::select('SELECT COUNT(*) as num FROM stocks_branch WHERE brach_id = (SELECT pos_user_branch.branch_id FROM pos_user_branch JOIN users ON pos_user_branch.user_id = users.id WHERE users.id = '.Auth::id().')');
                    @endphp
                    <li class="">
                        <a href="{{URL::to('/productsListsBranch') }}"><i class="fa fa-eye"></i> <span class="nav-label">รายการสินค้า</span><span class="label label-danger float-right">{{ $count_PB[0]->num }}</span></a>
                    </li>
            @endif
        </ul>
    </li>
    <li class="sub-menu">
        <a href="#"><i class="fa fa-archive"></i> <span class="nav-label">สต๊อกสินค้า</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse">
            @php
                $count_stock = DB::select('SELECT COUNT(*) as num FROM stocks');
            @endphp
            @if(in_array(Auth::user()->is_admin, $posAdmin))
                <li class=""><a href="{{URL::to('/viewMainStocks') }}">สต๊อกใหญ่ <span class="label label-secondary float-right">{{ $count_stock[0]->num }}</span></a></li>
                @else
                <li>
                    @php
                        $stock_branch = DB::select('SELECT COUNT(*) as num FROM stocks_branch');
                    @endphp
                    <a href="{{URL::to('/listStockBranch')}}">สต๊อกสาขา <span class="label label-secondary float-right">{{ $stock_branch[0]->num }}</span></a>
                </li>
            @endif
            
            <li><a href="{{URL::to('/listStockTransfer')}}">รายการโอนสินค้า</a></li>
        </ul>
    </li>
    @php
        $count_PS = DB::select('SELECT COUNT(*) as num FROM partnersystem');
    @endphp
    <li class="sub-menu">
        <a href="{{URL::to('/partnerSystem')}}"><i class="fa fa-users" aria-hidden="true"></i> <span class="nav-label">ระบบคู่ค้า </span><span class="label label-warning float-right">{{ $count_PS[0]->num }}</span></a>
    </li>
    <li class="sub-menu">
        <a href="{{URL::to('/wholeSale')}}"><i class="fa fa-cart-plus" aria-hidden="true"></i> <span class="nav-label">ขายส่ง</span><span class="label label-info float-right">5</span></a>
    </li>
    <li class="sub-menu">
        <a href="{{URL::to('/Warehousing')}}"><i class="fa fa-handshake-o" aria-hidden="true"></i> <span class="nav-label">รับฝากสินค้า</span><span class="label label-success float-right">5</span></a>
    </li>
    <li class="sub-menu">
        <a href="{{URL::to('/receiptSetting')}}"><i class="fa fa-file-text" aria-hidden="true"></i> <span class="nav-label">ตั้งค่าใบเสร็จ</span></a>
    </li>
</ul>

@section('script-menu')
    <script type="text/javascript">
        $(document).ready(function(){

            $(function() {
                setNavigation();
            });

            function setNavigation() {
                var path = window.location.href;
                // path = path[0] == '/' ? path : path; //it will remove the dash in the URL

                $("ul.metismenu a").each(function() {
                    var href = $(this).attr('href');

                    if (path.substring(0, href.length) === href) {
                        $(this).closest('li').addClass('active');
                    }
                });

                $("ul.metismenu .nav-second-level a").each(function() {
                    var href = $(this).attr('href');
                    if (path.substring(0, href.length) === href) {
                        $(this).parent().parent().closest('li').addClass('active');
                        $(this).closest('ul').addClass('in');
                    }
                });
            }
        
        });
    </script>
@endsection