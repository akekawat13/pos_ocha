@php
    $che_save_ticket = DB::select('SELECT id FROM `transaction_temp` WHERE usr_id = '.Auth::id().' AND status = 0');
    $c_id = count($che_save_ticket);
@endphp
<div class="container">
    <div class="row">
        <div class="col-lg-5">
            <h3 class="font-weight-bold">Ticket</h3>
        </div>
        <div class="col-lg-4 text-center" style="cursor: pointer;" id="save_ticket_onopen">
            <h3 id="icon_suspend"></h3>
        </div>
        {{-- <div class="col-lg-3 text-center" style="cursor: pointer;">
            <h3><i class="fa fa-users"></i></h3>
        </div> --}}
        <div class="col-lg-3 text-center">
            <ul class="nav navbar-top-links">
                <li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#" style="padding: 15px 10px !important;">
                        <i class="fa fa-ellipsis-v text-dark" style="font-size: 21px;margin-left: 14px;"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-alerts" style="width: 202px;">
                        <li>
                            <a class="dropdown-item clear_ticket_al" href="#"><i class="fa fa-trash"></i>&nbsp;&nbsp; Clear ticket all</a>
                        </li>
                        <li class="dropdown-divider"></li>
                        <li>
                            <a class="dropdown-item print_bill" href="#"><i class="fa fa-print"></i>&nbsp;&nbsp; Print bill</a>
                        </li>
                        <li class="dropdown-divider"></li>
                        <li>
                            <a class="dropdown-item" href="#" style="cursor: no-drop;color: #ccc;"><i class="fa fa-edit"></i>&nbsp;&nbsp; Edit ticket</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <div class="col-lg-12" style="border-bottom: 4px solid #cfcccc;top: 3px;"></div>
        <!-- user id -->
        <input type="hidden" name="user_id" value="{{Auth::id()}}">
        @if(!empty($data['transction_data_trn_doc_no'][0]->trn_doc_no))
            <input type="hidden" name="trn_doc_no" value="{{$data['transction_data_trn_doc_no'][0]->trn_doc_no}}">
            <input type="hidden" name="trn_id" value="{{$data['transction_data_all'][0]->trn_id}}">
        @endif
    </div>

</div>

<div class="container" style="font-size: 14px;">
    @if(!empty($data['transction_data_all']))
        @foreach($data['transction_data_all'] as $key => $value)
            <div class="row mt-2 on_data_for">
                <div class="col-lg-8" style="cursor: pointer;" onclick="pro_ticket_option({{$value->trn_id}},{{$value->pro_id}},'{{$value->pro_name}}')">
                    <input type="hidden" name="pro_id" value="{{$value->pro_id}}">
                    {{$value->pro_name}} <span class="text-muted">X <span class="font-weight-bold pro_quantity_{{$value->pro_id}}">{{$value->pro_quantity}}</span></span><br>
                </div>
                <div class="col-lg-4 text-right">
                    <span class="font-weight-bold">{{$value->pro_price}}</span><br>
                </div>
                <div class="w-100"></div>

                @if($value->toppings != "-")
                    @php
                        $arr_toppings = json_decode($value->toppings);
                    @endphp
                    @foreach($arr_toppings as $key2 => $value2)
                        <div class="col-lg-8"><span class="ml-3">- {{$value2->name}}</span></div>
                        <div class="col-lg-4 text-right">{{$value2->price}}.00</div>
                    @endforeach
                @endif
            </div>
        @endforeach

        <div class="row mt-3 on_data_for">
            <div class="col-lg-12"><hr></div>
            <div class="col-lg-6 font-weight-bold">
                Total
            </div>
            <div class="col-lg-6 text-right font-weight-bold print_total">
                {{$data['transction_data_money'][0]->sum_total}}
            </div>
        </div>
    @endif
    <div class="row mt-3" id="option_to_ticket"></div>
    <div class="row mt-3" id="tax_total"></div>
</div>

<br><br><br><br>
<div style="bottom: 0 !important;position: fixed;width: 26%;">
    <div class="container">
        <div class="row">
            <div
                class="col-lg-6 text-center border border-light"
                id="save_ticket"
                style="background-color: #020763 !important;color: #fff !important;padding: 15px 0 15px 0;cursor: pointer;">
                <span id="save_ticket_keyword">SAVE</span>
            </div>
            <div
                class="col-lg-6 text-center border border-light"
                style="background-color: gray !important;color: #fff !important;padding: 15px 0 15px 0;cursor: pointer;display: none;">
                <span id="">OPEN ON TICKET <span class="badge badge-danger save_ticket_keyword_open">{{$c_id}}</span></span>
            </div>

            <div
                class="col-lg-6 text-center border border-light"
                id="charge_ticket"
                onclick="location.href='{{route('submit_charge')}}'"
                style="background-color: #020763 !important;color: #fff !important;padding: 15px 0 15px 0;cursor: pointer;">
                CHARGE
            </div>
        </div>
    </div>
</div>
