<nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0;background-color: #020763 !important;width: 100%;">
    <div class="navbar-header">
        {{-- <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a> --}}

        {{-- <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#" onclick="event.preventDefault();window.location.reload();"><i class="fa fa-refresh"></i> </a> --}}

        <div class="navbar-form-custom" style="color: #ffffff !important;">
            <div class="form-group">
                {{-- <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search" > --}}
                <input type="text" class="form-input" name="search" placeholder="Search.." style="height: 36px;margin-top: 13px;">
            </div>
        </div>
        <form role="search" class="navbar-form-custom">
            <div class="btn-group" style="top: 13px;left: 50px;">
                <button data-toggle="dropdown" class="btn btn-secondary dropdown-toggle" aria-haspopup="true" aria-expanded="false">All items</button>
                <div class="dropdown-menu dropdown-menu-right">
                    @foreach($data['category'] as $key => $value)
                        <div class="dropdown-divider"></div>
                        <div class="all_items_{{$value->id}}">
                            {{--  <br>  --}}
                                <a class="dropdown-item sort_category" href="#" data-id="{{$value->id}}">{{$value->name}}</a>
                            {{--  <br>  --}}
                        </div>
                    @endforeach
                    <div class="dropdown-divider"></div>
                </div>
            </div>
        </form>
    </div>

    <ul class="nav navbar-top-links navbar-right" style="color: #ffffff !important;">
        <li style="padding: 20px;">
            <i class="fa fa-user"></i> <span class="m-r-sm welcome-message font-weight-bold">{{Auth::user()->name}}</span>
        </li>
        <li>
            <a href="{{ route('logout') }}"
            onclick="event.preventDefault();
            document.getElementById('logout-form').submit();" style="color: #ffffff !important;">
                <i class="fa fa-sign-out"></i> Log out
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </li>
    </ul>
</nav>
