<div class="container">
    <div class="row">
        <div class="col-lg-6">
            <h3 class="font-weight-bold">Ticket</h3>
        </div>
        <div class="col-lg-6 text-right" style="cursor: pointer;">
            <h3><i class="fa fa-users"></i></h3>
        </div>
        <div class="col-lg-12" style="border-bottom: 4px solid #cfcccc;top: 9px;"></div>
        <!-- user id -->
        <input type="hidden" name="user_id" value="{{Auth::id()}}">
    </div>

</div>

<div class="container mt-3" style="font-size: 14px;">
    @foreach($data_transaction_tamp['data_all'] as $key => $value)
        <div class="row">
            <div class="col-lg-8">
                {{$value->pro_name}} <span class="text-muted">X {{$value->pro_quantity}}</span><br>
                {{--  <small><span class="ml-3">{{$value->size_name}}</span></small><br>  --}}
            </div>
            <div class="col-lg-4 text-right">
                <span class="font-weight-bold">{{$value->pro_price}}</span><br>
                {{--  <span>{{$value->size_price}}</span>  --}}
            </div>
            <div class="w-100"></div>

            @if($value->toppings != "-")
                @php
                    $arr_toppings = json_decode($value->toppings);
                @endphp
                @foreach($arr_toppings as $key2 => $value2)
                    <div class="col-lg-8"><span class="ml-3">- {{$value2->name}}</span></div>
                    <div class="col-lg-4 text-right">{{$value2->price}}.00</div>
                @endforeach
            @endif
        </div>
    @endforeach

    <div class="row mt-3">
        <div class="col-lg-12"><hr></div>
        {{--  <div class="col-lg-6">
            Vat
        </div>
        <div class="col-lg-6 text-right">
            {{$data_transaction_tamp['data_money'][0]->sum_vat}}
        </div>
        <div class="col-lg-12"><hr></div>  --}}
        <div class="col-lg-6 font-weight-bold">
            Total
        </div>
        <div class="col-lg-6 text-right font-weight-bold">
            {{$data_transaction_tamp['data_money'][0]->sum_total}}
        </div>
    </div>
</div>
<br>
