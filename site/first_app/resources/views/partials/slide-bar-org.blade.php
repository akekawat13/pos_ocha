@section('customstyle')
    <!-- <style>
        .actived {
            border-left: 4px solid #19aa8d;
            background: #293846;
        }
    </style> -->
@endsection

<ul class="nav metismenu" id="side-menu">
    <li class="nav-header">
        <div class="dropdown profile-element">
            <img alt="image" class="rounded-circle img-fluid" src="{{asset("img/admin-user/user-login.png")}}" width="20%">
            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                <span class="block m-t-xs font-bold">{{ Auth::user()->name }}</span>
                <span class="text-muted text-xs block">Admin Director <b class="caret"></b></span>
            </a>
            <ul class="dropdown-menu animated fadeInRight m-t-xs">
                <li><a class="dropdown-item" href="{{ route('logout') }}"
                onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">Logout</a></li>
            </ul>
        </div>
        <div class="logo-element">
            <img src="{{asset("img/logo/logo-peanutHome-3.png")}}" width="40%">
        </div>
    </li>

    <li class="{{ (\Request::route()->getName() == 'home') ? 'active' : '' }} sub-menu">
        <a href="{{URL::to('/home')}}"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboards</span></a>
    </li>
    <li class="{{ (\Request::route()->getName() == 'category') ? 'active' : '' }} sub-menu">
        <a href="{{URL::to('/category')}}"><i class="fa fa-object-group"></i> <span class="nav-label">กลุ่มสินค้า</span></a>
    </li>
    <li class="{{ (\Request::route()->getName() == 'products') ? 'active' : '' }} sub-menu">
        <a href="#"><i class="fa fa-shopping-cart"></i> <span class="nav-label">สินค้า</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse">
            <li class="{{ request()->is('addProducts*','importProducts*') ? 'active' : '' }}">
                <a href="{{URL::to('/addProducts') }}"><i class="fa fa-plus"></i> <span class="nav-label">เพิ่มสินค้า</span></a>
            </li>
            @php
                $count_P = DB::select('SELECT COUNT(*) as num FROM products');
            @endphp
            <li class="{{ request()->is('productsLists*') ? 'active' : '' }}">
                <a href="{{URL::to('/productsLists') }}"><i class="fa fa-eye"></i> <span class="nav-label">รายการสินค้า</span><span class="label label-danger float-right">{{ $count_P[0]->num }}</span></a>
            </li>
        </ul>
    </li>
    <li class="{{ (\Request::route()->getName() == 'viewMainStocks') ? 'active' : '' }} sub-menu">
        <a href="#"><i class="fa fa-archive"></i> <span class="nav-label">สต๊อกสินค้า</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level collapse">
            <li class="active"><a href="{{URL::to('/viewMainStocks') }}">สต๊อกใหญ่</a></li>
            <li><a href="stock_2.html">stock v.2</a></li>
            <li><a href="stock_3.html">stock v.3</a></li>
            <li><a href="stock_4_1.html">stock v.4</a></li>
            <li><a href="stock_5.html">stock v.5 </a></li>
        </ul>
    </li>
    @php
        $count_PS = DB::select('SELECT COUNT(*) as num FROM partnersystem');
    @endphp
    <li class="{{ (\Request::route()->getName() == 'partnerSystem') ? 'active' : '' }} sub-menu">
        <a href="{{URL::to('/partnerSystem')}}"><i class="fa fa-users" aria-hidden="true"></i> <span class="nav-label">ระบบคู่ค้า </span><span class="label label-warning float-right">{{ $count_PS[0]->num }}</span></a>
    </li>
    <li class="{{ (\Request::route()->getName() == 'wholeSale') ? 'active' : '' }} sub-menu">
        <a href="{{URL::to('/wholeSale')}}"><i class="fa fa-cart-plus" aria-hidden="true"></i> <span class="nav-label">ขายส่ง</span><span class="label label-info float-right">5</span></a>
    </li>
    <li class="{{ (\Request::route()->getName() == 'Warehousing') ? 'active' : '' }} sub-menu">
        <a href="{{URL::to('/Warehousing')}}"><i class="fa fa-handshake-o" aria-hidden="true"></i> <span class="nav-label">รับฝากสินค้า</span><span class="label label-success float-right">5</span></a>
    </li>
</ul>

@section('script-menu')
    <script type="text/javascript">
        $(document).ready(function(){
            // $('ul#side-menu li').click(function(){
            //     $('li').removeClass("active");
            //     $(this).addClass("active");
            // });

            $(function() {
                setNavigation();
            });

            function setNavigation() {
                var path = window.location.pathname;

                $("ul.metismenu a").each(function() {
                    var href = $(this).attr('href');
                   
                    if (path.substring(0, href.length) === href) {
                        $(this).closest('li').addClass('active');
                    }
                });

                $("ul.metismenu .nav-second-level a").each(function() {
                    var href = $(this).attr('href');
                    if (path.substring(0, href.length) === href) {
                        $(this).parent().parent().closest('li').addClass('active');
                        $(this).closest('ul').addClass('in');
                    }
                });
            }
        
        });
    </script>
@endsection