<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="{{asset('img/logo/logo-peanutHome.png')}}" type="image/ico" />

    <title>Peanut Home</title>

    <link href="{{asset('css_insa/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('font-awesome/css/font-awesome.css')}}" rel="stylesheet">

    <link href="{{asset('css_insa/animate.css')}}" rel="stylesheet">
    <link href="{{asset('css_insa/style.css')}}" rel="stylesheet">

    <!-- Sweet Alert -->
    <link href="{{asset('css_insa/plugins/sweetalert/sweetalert.css')}}" rel="stylesheet">
    
    <!-- Data Table -->
    <link href="{{asset('css_insa/plugins/dataTables/datatables.min.css')}}" rel="stylesheet">
    <!-- FooTable -->
    <link href="{{asset('css_insa/plugins/footable/footable.core.css')}}" rel="stylesheet">

    <!-- Font CDN -->
    <link href="https://fonts.googleapis.com/css?family=Sarabun&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Kanit&display=swap" rel="stylesheet">

    <link href="{{asset('css_insa/plugins/iCheck/custom.css')}}" rel="stylesheet">
    <link href="{{asset('css_insa/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css')}}" rel="stylesheet">

    <!-- DarePicker -->
    <link href="{{asset('css_insa/plugins/datapicker/datepicker3.css')}}" rel="stylesheet">

    <style>
        .wrapper {
            font-family: 'Sarabun', sans-serif;
        }
        .f_sarabun {
            font-family: 'Sarabun', sans-serif;
        }
        .swal2-popup {
            font-family: "Sarabun", sans-serif;
        }
        /*.navbar-default{
            font-family: 'Kanit', sans-serif;
            font-weight: 200;
        }*/
    </style>
    
    <!-- Custom Style -->
    @yield('customstyle')
    
</head>

<body>
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="slimScrollDiv" style="position: fixed; overflow: hidden; width: 220px; height: 100%;">
                <div class="sidebar-collapse" style="position: relative;overflow: hidden; width: auto; height: 100%;">
                    <!-- Slide Bar -->
                    @include('partials.slide-bar')
                </div>
            </div>
        </nav>

        <div id="page-wrapper" class="gray-bg dashbard-1">
            <div class="row border-bottom">
                <!-- Top Bar -->
                @include('partials.top-bar')
            </div>
        
            <!-- Content -->
            @yield('content')
        
            <div class="footer">
                <div class="float-right">
                    10GB of <strong>250GB</strong> Free.
                </div>
                <div>
                    <strong>Copyright</strong> <a href="http://friends.solutions" target="_blank">Friends.Solutions</a> &copy; 2020
                </div>
            </div>
        
        </div>

    <!-- Mainly scripts -->
    <script src="{{asset('js_insa/jquery-3.1.1.min.js')}}"></script>
    <script src="{{asset('js_insa/popper.min.js')}}"></script>
    <script src="{{asset('js_insa/bootstrap.js')}}"></script>
    <script src="{{asset('js_insa/plugins/metisMenu/jquery.metisMenu.js')}}"></script>
    <script src="{{asset('js_insa/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>

    <!-- Flot -->
    <script src="{{asset('js_insa/plugins/flot/jquery.flot.js')}}"></script>
    <script src="{{asset('js_insa/plugins/flot/jquery.flot.tooltip.min.js')}}"></script>
    <script src="{{asset('js_insa/plugins/flot/jquery.flot.resize.js')}}"></script>
    <script src="{{asset('js_insa/plugins/flot/jquery.flot.pie.js')}}"></script>
    <script src="{{asset('js_insa/plugins/flot/jquery.flot.time.js')}}"></script>

    <!-- Peity -->
    <script src="{{asset('js_insa/plugins/peity/jquery.peity.min.js')}}"></script>
    <script src="{{asset('js_insa/demo/peity-demo.js')}}"></script>

    <!-- Custom and plugin javascript -->
    <script src="{{asset('js_insa/inspinia.js')}}"></script>
    <script src="{{asset('js_insa/plugins/pace/pace.min.js')}}"></script>

    <!-- jQuery UI -->
    <script src="{{asset('js_insa/plugins/jquery-ui/jquery-ui.min.js')}}"></script>

    <!-- GITTER -->
    <script src="{{asset('js_insa/plugins/gritter/jquery.gritter.min.js')}}"></script>

    <!-- Sparkline -->
    <script src="{{asset('js_insa/plugins/sparkline/jquery.sparkline.min.js')}}"></script>

    <!-- Sparkline demo data  -->
    <script src="{{asset('js_insa/demo/sparkline-demo.js')}}"></script>

    <!-- ChartJS-->
    <script src="{{asset('js_insa/plugins/chartJs/Chart.min.js')}}"></script>

    <!-- Toastr -->
    <script src="{{asset('js_insa/plugins/toastr/toastr.min.js')}}"></script>

    <!-- Sweet alert -->
    <script src="{{asset('js_insa/plugins/sweetalert/sweetalert.min.js')}}"></script>

    @yield('script')
    @yield('script-menu')
    @yield('scr-datatable')
    @yield('scriptAddProducts')
    @yield('script_addProductOne')
    @yield('scriptStock')
    @yield('sc_cat')
    @yield('script_category')
    @yield('script_category_sub')
    @yield('script_receiprt')
    @yield('script_branch')
    @yield('script_upload')
    @yield('scriptStock-branch')
    @yield('scr-editStockstransfer')
    @yield('scriptStockBranch')
    @yield('scriptStockTransfer')
    @yield('src_productBranch')
</body>
</html>
