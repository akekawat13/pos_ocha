<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="{{asset('img/logo/logo-ocha.png')}}" type="image/ico" />

    <title>Ocha Sale Page</title>

    <link href="{{asset('css_insa/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('font-awesome/css/font-awesome.css')}}" rel="stylesheet">

    <link href="{{asset('css_insa/animate.css')}}" rel="stylesheet">
    <link href="{{asset('css_insa/style.css')}}" rel="stylesheet">

    <!-- Sweet Alert -->
    <link href="{{asset('css_insa/plugins/sweetalert/sweetalert.css')}}" rel="stylesheet">

    <!-- Data Table -->
    <link href="{{asset('css_insa/plugins/dataTables/datatables.min.css')}}" rel="stylesheet">
    <!-- FooTable -->
    <link href="{{asset('css_insa/plugins/footable/footable.core.css')}}" rel="stylesheet">

    <!-- Font CDN -->
    <link href="https://fonts.googleapis.com/css?family=Sarabun&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Kanit:wght@200&display=swap" rel="stylesheet">


    <link href="{{asset('css_insa/plugins/iCheck/custom.css')}}" rel="stylesheet">
    <link href="{{asset('css_insa/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css')}}" rel="stylesheet">

    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

    <link href="{{asset('css_insa/custom.css')}}" rel="stylesheet">

    <style>

    </style>

</head>

<body style="background-color: #fff !important;font-family: 'Kanit', sans-serif;">

    <!-- Sidebar -->
    <div class="w3-sidebar w3-bar-block w3-card" style="width:26%;right:0;z-index: 2000;">
        @include('partials.slide-bar-sale-page')
    </div>
    <!-- Page Content -->
    <div style="margin-right:25%">
        <div class="w3-container w3-teal" style="background-color: #020763 !important;position: fixed !important;z-index: 1000;width: 75%;">
            @include('partials.top-bar-sale-page')
        </div>

        <div class="w3-container">
            <!-- Content -->
            @yield('content')

            <div class="footer" style="position: fixed !important; bottom: 0 !important;">
                <div class="float-right">
                    10GB of <strong>250GB</strong> Free.
                </div>
                <div>
                    <strong>Copyright</strong> <a href="http://friends.solutions" target="_blank">Friends.Solutions</a> &copy; 2020
                </div>
            </div>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="{{asset('js_insa/jquery-3.1.1.min.js')}}"></script>
    <script src="{{asset('js_insa/popper.min.js')}}"></script>
    <script src="{{asset('js_insa/bootstrap.js')}}"></script>
    <script src="{{asset('js_insa/plugins/metisMenu/jquery.metisMenu.js')}}"></script>
    <script src="{{asset('js_insa/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>

    <!-- Flot -->
    <script src="{{asset('js_insa/plugins/flot/jquery.flot.js')}}"></script>
    <script src="{{asset('js_insa/plugins/flot/jquery.flot.tooltip.min.js')}}"></script>
    <script src="{{asset('js_insa/plugins/flot/jquery.flot.resize.js')}}"></script>
    <script src="{{asset('js_insa/plugins/flot/jquery.flot.pie.js')}}"></script>
    <script src="{{asset('js_insa/plugins/flot/jquery.flot.time.js')}}"></script>

    <!-- Peity -->
    <script src="{{asset('js_insa/plugins/peity/jquery.peity.min.js')}}"></script>
    <script src="{{asset('js_insa/demo/peity-demo.js')}}"></script>

    <!-- Custom and plugin javascript -->
    <script src="{{asset('js_insa/inspinia.js')}}"></script>
    <script src="{{asset('js_insa/plugins/pace/pace.min.js')}}"></script>

    <!-- jQuery UI -->
    <script src="{{asset('js_insa/plugins/jquery-ui/jquery-ui.min.js')}}"></script>

    <!-- GITTER -->
    <script src="{{asset('js_insa/plugins/gritter/jquery.gritter.min.js')}}"></script>

    <!-- Sparkline -->
    <script src="{{asset('js_insa/plugins/sparkline/jquery.sparkline.min.js')}}"></script>

    <!-- Sparkline demo data  -->
    <script src="{{asset('js_insa/demo/sparkline-demo.js')}}"></script>

    <!-- ChartJS-->
    <script src="{{asset('js_insa/plugins/chartJs/Chart.min.js')}}"></script>

    <!-- Toastr -->
    <script src="{{asset('js_insa/plugins/toastr/toastr.min.js')}}"></script>

    <!-- Sweet alert -->
    <script src="{{asset('js_insa/plugins/sweetalert/sweetalert.min.js')}}"></script>

    <!-- TouchSpin -->
    <script src="{{asset('js_insa/plugins/touchspin/jquery.bootstrap-touchspin.min.js')}}"></script>

    @yield('script_sale_page')

</body>
</html>
