@extends('layouts.app_login')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-8">
            <div class="text-center">
                <img src="{{asset('img/logo/logo-ocha.png')}}" width="20%">
            </div>
            <div class="font-weight-bold text-center text-secondary">
                Wellcome To Ocha <br>Login in.To see it in action.
                @if ($message = Session::get('error'))
                    <div class="row">
                        <div class="col-lg-4"></div>
                        <div class="col-lg-4">
                            <div class="alert alert-danger alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $message }}</strong>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
            <form method="POST" action="{{ route('login') }}">
            @csrf
                <div class="row mt-2 mb-2">
                    <div class="col-lg-4"></div>
                    <div class="col-lg-4">
                        <div>
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Email">
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="mt-3">
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Password">
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="mt-3 text-center">
                            <button type="submit" class="btn btn-success col-12" style="background-color: #050bb9b7;">
                            {{ __('Login') }}
                            </button>

                            {{-- @if (Route::has('password.request'))
                                <a class="btn btn-link" href="{{ route('password.request') }}" style="font-size: 12px;color: #050ab9;">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                            @endif --}}
                        </div>
                        {{-- <div class="mt-1 text-center">
                             <a class="btn btn-success col-12 btn btn-outline-secondary" href="{{ route('register') }}">{{ __('Create an account') }}</a>
                        </div> --}}
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
