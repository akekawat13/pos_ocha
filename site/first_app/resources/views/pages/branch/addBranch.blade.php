<form action="{{url('/addBranch')}}" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="row">
        <div class="col-lg-1"></div>
        <div class="col-lg-4 text-center">
            <p><input type="file" accept="image/*" name="shopLogo" id="shopLogo" onchange="loadFile(event)" style="display: none;"></p>
            <p><label for="shopLogo" style="cursor: pointer;border: 1px dashed #16a085;padding: 20px;border-radius: 5px;"><i class="fa fa-upload fa-2x"></i> อัฟโหลดโลโก้</label></p>
            <p><img id="output" width="200" /></p>
        </div>
        <div class="col-lg-6">
            <div class="form-group row"><label class="col-lg-4 col-form-label">ชื่อ ร้านค้า</label>
                <div class="col-lg-8"><input type="text" class="form-control" name="branchName" required>
                </div>
            </div>
            <div class="form-group row"><label class="col-lg-4 col-form-label">ที่อยู่</label>
                <div class="col-lg-8"><textarea name="branchAddress" class="form-control" type="text" required></textarea></div>
            </div>
            <div class="form-group row"><label class="col-lg-4 col-form-label">เลขผู้เสียภาษี</label>
                <div class="col-lg-8"><input type="text" class="form-control"name="branchTax" placeholder="" required></div>
            </div>
            <div class="form-group row"><label class="col-lg-4 col-form-label">เบอร์โทรศัพท์</label>
                <div class="col-lg-8"><input type="text" class="form-control"name="branchPhoneNumber" placeholder="" required></div>
            </div>
        </div>
        <div class="hr-line-dashed"></div>
        <div class="col-lg-12 text-center">
            <button type="submit" class="btn btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i> บันทึก</button>
        </div>
    </div>
</form>