@extends('layouts.appPeanutHome')
@php
    $posAdmin = array(1,10);
@endphp
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>สาขา</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Home</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>สาขา</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeIn">
    <div class="row mb-5">
        <div class="col-lg-12">
            <div class="tabs-container">
                <ul class="nav nav-tabs" role="tablist">
                    @if(in_array(Auth::user()->is_admin, $posAdmin))
                        <li><a class="nav-link " data-toggle="tab" href="#tab-1"><i class="fa fa-plus" aria-hidden="true"></i> เพิ่มสาขา</a></li>
                    @endif
                    <li><a class="nav-link active" data-toggle="tab" href="#tab-2"><i class="fa fa-eye" aria-hidden="true"></i> 
                        @if(in_array(Auth::user()->is_admin, $posAdmin)) สาขาทั้งหมด @endif สาขา
                    </a></li>
                    {{--  <li><a class="nav-link " data-toggle="tab" href="#tab-1"><i class="fa fa-plus" aria-hidden="true"></i> เพิ่มสาขา</a></li>  --}}
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" id="tab-1" class="tab-pane ">
                        <div class="panel-body">
                            @include('pages.branch.addBranch')
                        </div>
                    </div>
                    <div role="tabpanel" id="tab-2" class="tab-pane active">
                        <div class="panel-body">
                            @include('pages.branch.viewBranch')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script_branch')
	<script type="text/javascript">
        var loadFile = function(event) {
            var image = document.getElementById('output');
            image.src = URL.createObjectURL(event.target.files[0]);
        };
        //function Edit 
        $(document).on('click','.edit-modal',function() {
            $('#footer_action_button').text("บันทึก");
            $('#footer_action_button').addClass('fa fa-check');
            $('#footer_action_button').removeClass('glyphicon-trash');
            $('.actionBtn').addClass('btn-success');
            $('.actionBtn').removeClass('btn-danger');
            $('.actionBtn').addClass('edit');
            $('.modal-title').text('แก้ไขข้อมูล');
            $('.deletecontent').hide();
            $('#fid').val($(this).data('id'));
            $('#sN').val($(this).data('shopname'));
            $('#aD').val($(this).data('address'));
            $('#st').val($(this).data('shoptax'));
            $('#sp').val($(this).data('shopphone'));
            $('#myModal').modal('show');
        });

        $('.modal-footer').on('click','.edit',function() {
          $.ajax({
            type : "POST",
            url : "{{route('updateBranch')}}",
            data : {
              '_token' : $('input[name=_token]').val(),
              'id' : $('#fid').val(),
              'shopName' : $('#sN').val(),
              'address' : $('#aD').val(),
              'shoptax' : $('#st').val(),
              'shopphone' : $('#sp').val()
            },
            success:function(data){
                console.log(data);
                swal({
                    title: "แก้ไขข้อมูลเรียบร้อย !",
                    type: "success",
                    showConfirmButton: false,
                    timer: 1500
                });
                location.reload();
            }
          });
        });
        //Form Delete Function
        $(document).on('click','.delete-modal',function() {
            $('#footer_action_button').text("ลบ");
            $('#footer_action_button').removeClass('glyphicon-check');
            $('#footer_action_button').addClass('fa fa-trash');
            $('.actionBtn').removeClass('btn-success');
            $('.actionBtn').addClass('btn-danger');
            $('.actionBtn').addClass('delete');
            $('.modal-title').text('ลบ สาขา');
            $('.deletecontent').show();
            $('.form-horizontal').hide();
            $('.id').text($(this).data('id'));
            $('.title').text($(this).data('shopname'));
            $('#myModal').modal('show');
        });

        $('.modal-footer').on('click','.delete',function() {
          $.ajax({
            type : "POST",
            url : "{{route('deleteBranch')}}",
            data : {
              '_token' : $('input[name=_token]').val(),
              'id' : $('.id').text()
            },
            success:function(data){
              $('.post' + $('.id').text()).remove();
              location.reload();
            }
          });
        });
    </script>
@endsection