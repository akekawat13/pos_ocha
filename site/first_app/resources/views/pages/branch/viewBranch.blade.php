@php
// echo "<pre>";
    // print_r($allbranch);
// echo "</pre>";
@endphp
<div class="table table-responsive">
  <table class="table table-bordered" id="table">
    <thead>
      <tr>
        <th class="text-center">No.</th>
        <th class="text-center">ชื่อร้าน</th>
        <th class="text-center">ที่อยู่</th>
        <th class="text-center">เลขผู้เสียภาษี</th>
        <th class="text-center">เบอร์โทร</th>
        <th></th>
      </tr>
    </thead>
    {{ csrf_field() }}
    @php $no=1; @endphp
    @foreach ($allbranch as $key => $value)
    <tr class="post{{ $value->id }}">
      <td class="text-center">{{ $no++ }}</td>
      <td class="text-center">{{ $value->shop_name }}</td>
      <td class="text-center">{{ $value->shop_address }}</td>
      <td class="text-center">{{ $value->shop_tax_id }}</td>
      <td class="text-center">{{ $value->shop_phone }}</td>
      <td class="text-center">
        <a href="#" class="edit-modal btn btn-warning btn-sm" data-id="{{ $value->id }}" data-shopname="{{ $value->shop_name }}" data-address="{{ $value->shop_address }}" data-shoptax="{{ $value->shop_tax_id }}" data-shopphone="{{ $value->shop_phone }}">
          <i class="fa fa-edit"></i> แก้ไขข้อมูล
        </a>
        @if(in_array(Auth::user()->is_admin, $posAdmin))
        <a href="#" class="delete-modal btn btn-danger btn-sm" data-id="{{ $value->id }}" data-shopname="{{ $value->shop_name }}">
          <i class="fa fa-trash"></i> ลบ
        </a>
        @endif
      </td>
    </tr>
    @endforeach
  </table>
</div><!-- ../class table -->

{{-- Form Modal --}}
@include('pages.branch.formModalBranch')