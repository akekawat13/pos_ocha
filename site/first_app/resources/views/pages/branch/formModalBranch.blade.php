{{-- form Edit and Delete post --}}
    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form class="form-horizontal" role="modal">
              {{-- ID --}}
              <input type="text" class="form-control" id="fid" hidden>

              <div class="form-group row">
                <label for="" class="control-label col-sm-3">ชื่อร้าน</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="sN">
                </div>
              </div>
              <div class="form-group row">
                <label for="" class="control-label col-sm-3">ที่อยู่</label>
                <div class="col-sm-9">
                  <textarea type="text" class="form-control" id="aD"></textarea>
                </div>
              </div>
              <div class="form-group row">
                <label for="" class="control-label col-sm-3">เลขผู้เสียภาษี</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="st">
                </div>
              </div>
              <div class="form-group row">
                <label for="" class="control-label col-sm-3">เบอร์ติดต่อ</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="sp">
                </div>
              </div>
            </form>
            
            {{-- Form Delete --}}
            <div class="deletecontent">
              ต้องการลบสาขา <span class="title"></span> ?
              <input type="hidden" class="id">
            </div>

          </div>
          <div class="modal-footer">
            <button class="btn actionBtn" type="button" data-dismiss="modal">
              <span id="footer_action_button" class="glyphicon"></span>
            </button>
            <button class="btn btn-warning" type="button" data-dismiss="modal">
              <span class="glyphicon glyphicon"></span>ยกเลิก
            </button>
          </div>
        </div>
      </div>
    </div>