@extends('layouts.appPeanutHome')

@section('script_receiprt')
	<!-- iCheck -->
    <script src="{{asset('js_insa/plugins/iCheck/icheck.min.js')}}"></script>

	<!-- upload images -->
	{{-- <script src="{{ asset('js2/jquery.uploadfile.js') }}"></script>
	<script src="{{ asset('js2/add_new_img.js') }}"></script>
	<script src="{{ asset('js2/jquery.form.js') }}"></script> --}}

	<script type="text/javascript">
		$('#print').click(function(){
			window.print();
		});
		$('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });
		function keyUpAjax(){
			$(document).ready(function(){
				var Nshop = $("input[name=shopName]").val();
				var address = $("input[name=shopAddress]").val();
				var tax_id = $("input[name=tax_id]").val();
				var last_text = $("textarea[name=last_text]").val();
				var Nphone = $("input[name=shopPhone]").val();
				//console.log(Nshop);
				$('#shopNameTo').html(Nshop);
				$('#addressTo').html(address);
				$('#taxTo').html("เลขประจำตัวผู้เสียภาษี "+tax_id);
				$('#last_textTo').html(last_text);
				$('#phoneTo').html("โทร. "+Nphone);
			});
		}
		$("select[name=nameBranch]").change(function() {
	        var value = "";
	        $( "select option:selected" ).each(function() {
	            var nameBranch = $("select[name=nameBranch]").val();
	            //console.log("ไอดี สาขา = " +nameBranch);
	            $.ajax({
	                type : "POST",
	                url : '/branchToreceipt',
	                data : {
	                  '_token' : $('input[name=_token]').val(),
	                  'id' : nameBranch,
	                },
	                success:function(data){
	                   //console.log(data[0].shop_logo);
	                   $("#shop_logo").html('<img src="shopLogo/'+data[0].shop_logo+'" width="25%">');
	                   $("#shopNameTo").html(data[0].shop_name);
	                   $("#addressTo").html(data[0].shop_address);
	                   $("#taxTo").html("เลขประจำตัวผู้เสียภาษี "+data[0].shop_tax_id);
	                   $("#phoneTo").html("โทร. " +data[0].shop_phone);
	                }
	            });
	        });
	    });

		// show Image
        var loadFile = function(event) {
            var image = document.getElementById('output');
            image.src = URL.createObjectURL(event.target.files[0]);
        };

        $('#upload_form').on('submit', function(event){
            event.preventDefault();
            $.ajax({
                url:"{{ route('ajaxupload.action') }}",
                method:"POST",
                data:new FormData(this),
                dataType:'JSON',
                contentType: false,
                cache: false,
                processData: false,
                success:function(data){
                    // $('#message').css('display', 'block');
                    // $('#message').html(data.message);
                    // $('#message').addClass(data.class_name);
                    $('#show_images').html('<img src="images/'+data.logo_name+'" width="20%" />');
                    $('input[name=logo_name]').val(data.logo_name);
                    console.log(data.message+","+data.logo_name);
                }
            })
        });
        //save receipt
        $('#addReceipt').click(function(){
        	var logo_name = $('input[name=logo_name]').val();
        	var shopName = $('input[name=shopName]').val();
        	var shopAddress = $('input[name=shopAddress]').val();
        	var tax_id = $('input[name=tax_id]').val();
        	var last_text_bill = $('textarea[name=last_text]').val();
        	var shopPhone = $('input[name=shopPhone]').val();
        	//console.log(logo_name);
        	$.ajax({
                type : "POST",
                url : "{{route('addReceipt')}}",
                data : {
                  	'_token' : $('input[name=_token]').val(),
                  	'logo_name' : logo_name,
                  	'shopName' : shopName,
                  	'shopAddress' : shopAddress,
                  	'tax_id' : tax_id,
                  	'last_text_bill' : last_text_bill,
                  	'shopPhone' : shopPhone,
                },
                success:function(data){
                   	console.log(data);
                }
            });
        });
	</script>
@endsection

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>ตั้งค่าใบเสร็จ</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Home</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>ตั้งค่าใบเสร็จ</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight" >
    <div class="row">
        <div class="col-lg-6">
            <div class="ibox ">
                <div class="ibox-content">
                    <form method="post" id="upload_form" enctype="multipart/form-data">
                	{{csrf_field()}}
                    	@php
                    		$allbranch = DB::select('SELECT * FROM `branch` WHERE 1');
                    	@endphp
                    	{{-- <div class="form-group row">
                    		<label class="col-sm-3 col-form-label">เลือกร้านค้า</label>
                            <div class="col-sm-9">
                            	<select class="form-control m-b" name="nameBranch">
                            		<option value="0" id="select_branch">-- เลือกร้านค้า --</option>
	                                @for ($i = 0; $i < count($allbranch); $i++)
									    <option value="{{$i+1}}" id="select_branch{{$i}}">{{$allbranch[$i]->shop_name}}</option>
									@endfor
	                            </select>
	                        </div>
	                    </div> --}}

                <div class="form-group">
                    <table class="table">
                        <tr>
                            <td width="40%" align="right"><label>อัฟโหลด logo</label></td>
                            <td width="30"><input type="file" name="select_file" id="select_file" /></td>
                            <td width="30%" align="left"><input type="submit" name="upload" id="upload" class="btn btn-primary" value="Upload"></td>
                        </tr>
                        {{-- <tr>
                            <td width="40%" align="right"></td>
                            <td width="30"><span class="text-muted">jpg, png, gif</span></td>
                            <td width="30%" align="left"></td>
                        </tr> --}}
                    </table>
                </div>

                    	{{-- <div class="form-group  row"><label class="col-sm-3 col-form-label">โลโก้</label>
                            <div class="col-sm-9">
                            	<p><input type="file" name="shop_logo" id="shop_logo" onchange="loadFile(event)" style="display: none;"></p>
			                    <label for="shop_logo" style="cursor: pointer;border: 1px dashed #16a085;padding: 20px;border-radius: 5px;"><i class="fa fa-upload fa-2x"></i> อัฟโหลดโลโก้ร้านค้า</label>
								<div class="text-danger">
									(ระบุขนาดให้ชัดเจน)
								</div>
                            </div>
                        </div> --}}
                        <div class="form-group  row"><label class="col-sm-3 col-form-label">ชื่อร้าน</label>
                            <div class="col-sm-9"><input onkeyup="keyUpAjax();" type="text" class="form-control" name="shopName"></div>
                        </div>
                        <div class="form-group  row"><label class="col-sm-3 col-form-label">ที่อยู่</label>
                            <div class="col-sm-9"><input onkeyup="keyUpAjax();" type="text" class="form-control" name="shopAddress"></div>
                        </div>
                        <div class="form-group  row"><label class="col-sm-3 col-form-label">เลขผู้เสียภาษี</label>
                            <div class="col-sm-9"><input onkeyup="keyUpAjax();" type="text" class="form-control" name="tax_id"></div>
                        </div>
                        <div class="form-group  row"><label class="col-sm-3 col-form-label">ข้อความท้ายบิล</label>
                            <div class="col-sm-9"><textarea onkeyup="keyUpAjax();" name="last_text" class="form-control"></textarea></div>
                        </div>
                        <div class="form-group  row"><label class="col-sm-3 col-form-label">เบอร์โทร</label>
                            <div class="col-sm-9"><input onkeyup="keyUpAjax();" type="text" class="form-control" name="shopPhone"></div>
                        </div>
                        <div class="form-group row"><label class="col-sm-3 col-form-label">ตั้งค่า vat</label>
                            <div class="col-sm-9">
                            	<label class="checkbox-inline i-checks">
                            		<input type="checkbox" value="vat" id="inlineCheckbox1" disabled> มี vat
                            	</label>
                            	<label class="i-checks ml-3">
                            		<input type="checkbox" value="noVat" id="inlineCheckbox2" disabled> ไม่มี vat
                            	</label>
                         	</div>
                        </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>ตัวอย่างใบเสร็จ</h5>
	                    <span>
	                    	<a href="{{url('/print')}}" class="btn btn-sm btn-info">พิมพ์ตัวอย่าง</a>
	                    </span>
	                    <span>
	                    	{{-- <button  type="submit" class="btn btn-sm btn-success">บันทึกใบเสร็จ</button> --}}
	                    	<button  id="addReceipt" type="button" class="btn btn-sm btn-success">บันทึกใบเสร็จ</button>
	                    </span>
                    </form><!-- ../Form -->
                </div>
                <div class="ibox-content">
	                <div class="row">
	                	<div class="col-lg-2"></div>
	                    <div class="col-lg-8 border border-red">
	                    	<div id="shop_logo" class="text-center mt-3 mb-3">
	                    		<p><img id="output" width="10%" /></p>
	                    		<span id="show_images"></span>
	                    		<input type="hidden" name="logo_name">
	                    	</div>
	                    	<div id="shopNameTo" class="text-center mt-3 mb-3" style="font-size: 14px;"></div>
	                    	<div id="addressTo" class="text-center mt-3 mb-3"></div>
	                    	<div id="taxTo" class="text-center mt-3 mb-3"></div>
	                    	<hr class="hr-line-dashed">
								<div class="row">
									<div class="col-lg-4">
										No.
									</div>
									<div class="col-lg-4">
										รายการ
									</div>
									<div class="col-lg-4 text-right">
										ราคา
									</div>
								</div>
	                    	<hr class="hr-line-dashed">
	                    		<div class="row">
	                    			<div class="col-lg-4"></div>
	                    			<div class="col-lg-4 text-right">
	                    				<div>รวม</div>
	                    				<div>vat</div>
	                    				<div>รวม vat</div>
	                    			</div>
	                    			<div class="col-lg-4 text-right">
	                    				<div><span>

	                    					</span><span>บาท</span>
	                    				</div>
	                    				<div><span>

		                    				</span><span>บาท</span>
		                    			</div>
	                    				<div><span class="border-bottom">

	                    					</span><span>บาท</span>
	                    				</div>
	                    			</div>
	                    		</div>
	                    	<hr class="hr-line-dashed">
	                    	<div id="last_textTo" class="text-center mt-3 mb-3"></div>
	                    	<div id="phoneTo" class="text-center mt-3 mb-3"></div>
	                    </div>
	                    <div class="col-lg-12 mt-2">
	                    	<div><span class="text-danger"><sup>**</sup>ขนาดควากว้าง 58 มิลลิเมตร</span></div>
                            <div><span class="text-danger"><sup>*</sup>ขนาดควากว้าง 80 มิลลิเมตร</span></div>
	                    </div>
	                </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
