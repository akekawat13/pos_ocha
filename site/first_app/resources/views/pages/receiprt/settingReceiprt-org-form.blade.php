@extends('layouts.appPeanutHome')

@section('script_receiprt')
	<!-- iCheck -->
    <script src="js/plugins/iCheck/icheck.min.js"></script>

	<script type="text/javascript">
		$('#print').click(function(){
			window.print();
		});
		$('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });
		function keyUpAjax(){
			$(document).ready(function(){
				var Nshop = $("input[name=shopName]").val();
				var address = $("input[name=shopAddress]").val();
				var tax_id = $("input[name=tax_id]").val();
				var last_text = $("textarea[name=last_text]").val();
				var Nphone = $("input[name=shopPhone]").val();
				//console.log(Nshop);
				$('#shopNameTo').html(Nshop);
				$('#addressTo').html(address);
				$('#taxTo').html("เลขประจำตัวผู้เสียภาษี "+tax_id);
				$('#last_textTo').html(last_text);
				$('#phoneTo').html("โทร. "+Nphone);
			});
		}
		$("select[name=nameBranch]").change(function() {
	        var value = "";
	        $( "select option:selected" ).each(function() {
	            var nameBranch = $("select[name=nameBranch]").val();
	            //console.log("ไอดี สาขา = " +nameBranch);
	            $.ajax({
	                type : "POST",
	                url : '/branchToreceipt',
	                data : {
	                  '_token' : $('input[name=_token]').val(),
	                  'id' : nameBranch,
	                },
	                success:function(data){
	                   //console.log(data[0].shop_logo);
	                   $("#shop_logo").html('<img src="shopLogo/'+data[0].shop_logo+'" width="25%">');
	                   $("#shopNameTo").html(data[0].shop_name);
	                   $("#addressTo").html(data[0].shop_address);
	                   $("#taxTo").html("เลขประจำตัวผู้เสียภาษี "+data[0].shop_tax_id);
	                   $("#phoneTo").html("โทร. " +data[0].shop_phone);
	                }
	            });  
	        });
	    }); 

		// show Image
        var loadFile = function(event) {
            var image = document.getElementById('output');
            image.src = URL.createObjectURL(event.target.files[0]);
        };

        // $("#addReceipt").click(function(){
        // 	var logo = $('#imageInput')[0].file[0].type;
        // 	var formData = new FormData($("#myform")[0]);
        // 	console.log(logo);
        // 	$.ajax({
        //         type : "post",
        //         url : '/addReceipt',
        //         dataType: 'text', // what to expect back from the server
	       //      cache: false,
	       //      contentType: false,
	       //      processData: false,
        //         data : {
        //           '_token' : $('input[name=_token]').val(),
        //           'logo' : logo,
        //         },
        //         success:function(data){
        //           	console.log(data);
        //         }
        //     });  
        // });
	</script>
@endsection

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>ตั้งค่าใบเสร็จ</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Home</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>ตั้งค่าใบเสร็จ</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight" >
    <div class="row">
        <div class="col-lg-6">
            <div class="ibox ">
                <div class="ibox-content">
                    <form method="post" action="{{url('/addReceipt')}}" enctype="multipart/form-data" id="myform">
                    	@csrf
                    	@php
                    		$allbranch = DB::select('SELECT * FROM `branch` WHERE 1');
                    	@endphp
                    	{{-- <div class="form-group row">
                    		<label class="col-sm-3 col-form-label">เลือกร้านค้า</label>
                            <div class="col-sm-9">
                            	<select class="form-control m-b" name="nameBranch">
                            		<option value="0" id="select_branch">-- เลือกร้านค้า --</option>
	                                @for ($i = 0; $i < count($allbranch); $i++)
									    <option value="{{$i+1}}" id="select_branch{{$i}}">{{$allbranch[$i]->shop_name}}</option>
									@endfor
	                            </select>
	                        </div>
	                    </div> --}}
                    	<div class="form-group  row"><label class="col-sm-3 col-form-label">โลโก้</label>
                            <div class="col-sm-9">
                            	<p><input type="file" accept="image/*" name="shop_logo" id="shop_logo" onchange="loadFile(event)" style="display: none;"></p>
			                    <label for="shop_logo" style="cursor: pointer;border: 1px dashed #16a085;padding: 20px;border-radius: 5px;"><i class="fa fa-upload fa-2x"></i> อัฟโหลดโลโก้ร้านค้า</label>
								<div class="text-danger">
									(ระบุขนาดให้ชัดเจน)
								</div>
                            </div>
                        </div>
                        <div class="form-group  row"><label class="col-sm-3 col-form-label">ชื่อร้าน</label>
                            <div class="col-sm-9"><input onkeyup="keyUpAjax();" type="text" class="form-control" name="shopName"></div>
                        </div>
                        <div class="form-group  row"><label class="col-sm-3 col-form-label">ที่อยู่</label>
                            <div class="col-sm-9"><input onkeyup="keyUpAjax();" type="text" class="form-control" name="shopAddress"></div>
                        </div>
                        <div class="form-group  row"><label class="col-sm-3 col-form-label">เลขผู้เสียภาษี</label>
                            <div class="col-sm-9"><input onkeyup="keyUpAjax();" type="text" class="form-control" name="tax_id"></div>
                        </div>
                        <div class="form-group  row"><label class="col-sm-3 col-form-label">ข้อความท้ายบิล</label>
                            <div class="col-sm-9"><textarea onkeyup="keyUpAjax();" name="last_text" class="form-control"></textarea></div>
                        </div>
                        <div class="form-group  row"><label class="col-sm-3 col-form-label">เบอร์โทร</label>
                            <div class="col-sm-9"><input onkeyup="keyUpAjax();" type="text" class="form-control" name="shopPhone"></div>
                        </div>
                        <div class="form-group row"><label class="col-sm-3 col-form-label">ตั้งค่า vat</label>
                            <div class="col-sm-9">
                            	<label class="checkbox-inline i-checks">
                            		<input type="checkbox" value="vat" id="inlineCheckbox1"> มี vat 
                            	</label>
                            	<label class="i-checks ml-3"> 
                            		<input type="checkbox" value="noVat" id="inlineCheckbox2"> ไม่มี vat 
                            	</label>
                         	</div>
                        </div>
                </div>
            </div>  
        </div>
        <div class="col-lg-6">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>ตัวอย่างใบเสร็จ</h5>
	                    <span>
	                    	<a href="{{url('/print')}}" class="btn btn-sm btn-info">พิมพ์ตัวอย่าง</a>
	                    </span>
	                    <span>
	                    	<button  type="submit" class="btn btn-sm btn-success">บันทึกใบเสร็จ</button>
	                    	{{-- <button  id="addReceipt" type="button" class="btn btn-sm btn-success">บันทึกใบเสร็จ</button> --}}
	                    </span>
                    </form><!-- ../Form -->
                </div>
                <div class="ibox-content">
	                <div class="row">
	                	<div class="col-lg-2"></div>
	                    <div class="col-lg-8 border border-red">
	                    	<div id="shop_logo" class="text-center mt-3 mb-3">
	                    		<p><img id="output" width="25%" /></p>
	                    	</div>
	                    	<div id="shopNameTo" class="text-center mt-3 mb-3" style="font-size: 14px;"></div>
	                    	<div id="addressTo" class="text-center mt-3 mb-3"></div>
	                    	<div id="taxTo" class="text-center mt-3 mb-3"></div>
	                    	<hr class="hr-line-dashed">
								<div class="row">
									<div class="col-lg-4">
										No.
									</div>
									<div class="col-lg-4">
										รายการ
									</div>
									<div class="col-lg-4 text-right">
										ราคา
									</div>
								</div>
	                    	<hr class="hr-line-dashed">
	                    		<div class="row">
	                    			<div class="col-lg-4"></div>
	                    			<div class="col-lg-4 text-right">
	                    				<div>รวม</div>
	                    				<div>vat</div>
	                    				<div>รวม vat</div>
	                    			</div>
	                    			<div class="col-lg-4 text-right">
	                    				<div><span>
	                    					
	                    					</span><span>บาท</span>
	                    				</div>
	                    				<div><span>
	                    					
		                    				</span><span>บาท</span>
		                    			</div>
	                    				<div><span class="border-bottom">
	                    					
	                    					</span><span>บาท</span>
	                    				</div>
	                    			</div>
	                    		</div>
	                    	<hr class="hr-line-dashed">
	                    	<div id="last_textTo" class="text-center mt-3 mb-3"></div>
	                    	<div id="phoneTo" class="text-center mt-3 mb-3"></div>
	                    </div>
	                    <div class="col-lg-12 mt-2">
	                    	<span class="text-danger"><sup>*</sup>ขนาดควากว้าง 80 มิลลิเมตร</span>
	                    </div>
	                </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
