<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>

	<link rel="stylesheet" type="text/css" href="{{asset('css_insa/print.css')}}" media="print">
</head>
<body style="margin: 0 !important;font-size: 12px;">
	<button id="print" type="button" class="btn btn-sm btn-info"><i class="fa fa-print"></i> พิมพ์ตัวอย่าง</button>
    <div style="text-align: center;padding: 15px;width: 200px;border: 1px solid #000;">
    	@php
    		foreach ($receiptData as $key => $value) {
    			//echo $value->shop_logo;
    		}
    	@endphp
    	<div>
    		<img src="images/{{$value->shop_logo}}" alt="" width="30%">
    	</div>
    	<div id="shopNameTo">
    		{{$value->shop_name}}
    	</div>
    	<div id="addressTo">
    		{{$value->shop_address}}
    	</div>
    	<div id="taxTo">
    		เลขผู้เสียภาษี <span>{{$value->tax_id}}</span>
    	</div>
    	<hr>
			<div class="row">
				<span style="width: 60px;float: left;">
					No.
				</span>
				<span style="width: 60px;">
					รายการ
				</span>
				<span style="width: 60px;float: right;">
					ราคา
				</span>
			</div>
    	<hr>
	    	<div class="row">
	    		<div>
					<span style="width: 90px;">
						รวม
					</span>
					<span style="width: 90px;float: right;">
						บาท
					</span>
				</div>
				<div>
					<span style="width: 90px;">
						vat
					</span>
					<span style="width: 90px;float: right;">
						7 %
					</span>
				</div>
				<div>
					<span style="width: 90px;">
						รวม vat
					</span>
					<span style="width: 90px;float: right;">
						บาท
					</span>
				</div>
			</div>
    	<hr>
    	<div id="last_textTo">
    		{{$value->last_text}}
    	</div>
    	<div id="phoneTo">
    		โทร. {{$value->shop_phone}}
    	</div>
    </div>

    <!-- Mainly scripts -->
    <script src="{{asset('js_insa/jquery-3.1.1.min.js')}}"></script>
    <script type="text/javascript">
    	//window.print();
		$('#print').click(function(){
			window.print();
		});
	</script>
</body>
</html>
