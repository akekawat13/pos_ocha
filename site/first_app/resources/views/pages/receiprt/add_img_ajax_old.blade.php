@extends('layouts.app_custom')

@section('content')
<?php session_start(); ?>
<div class="container">
    <div class="row"> 
        
        <!-- <button onclick="goBack()"><< Back</button> -->
        <br><br>
        <div><strong>อัพโหลดรูปสำหรับหัวข้อ {{ $topic_name }}</strong></div>
        <br> trasaction = {{ $transac_id }}
        <input type="hidden" id="transac_id" name="transac_id" value="{{ $transac_id }}">
        <?php 
            //echo $_SESSION['section_img_dir'];
            $img_patch = 'images/';
            $img_list = scandir($img_patch);
            //print_r($img_list);
        ?>
        <div id="img_result" class="col-md-12">
        <?php 

            for ($i=0; $i < count($img_list); $i++) { 
                if ($img_list[$i] != '.' && $img_list[$i] != '..'){
                    echo '<div id="div_img'.$i.'" class="col-xs-6 col-sm-6 col-md-4 col-lg-3 pull-left"style="margin-bottom:10px;"> <img src="'.asset($img_patch.'/'.$img_list[$i]).'" class="img-responsive" style="float: left; margin-bottom:5px;">';
                    echo '<span id="img_del_'.$i.'" onclick="godelete(\''.$img_patch.','.$img_list[$i].',div_img'.$i.'\')" class="btn btn-danger">ลบ</span>&nbsp;';
                    /*echo '<span id="rotate_r_'.$i.'" onclick="rotate_r(\''.$img_patch.','.$img_list[$i].',div_img'.$i.'\')" class="btn btn-primary">rt_r</span>&nbsp;';*/
                        
                    echo '<span><a class="btn btn-primary" href="'.url('rotate_left/'.$transac_id.'/'.$img_list[$i]).'/'.$year.'/'.$quarter.'/'.$dlrid.'/'.$dlrname.'/'.$dlrsize.'/'.$section_type.'/'.$ctent_type.'/'.$topic_name.'"><i class="fa fa-undo" aria-hidden="true"></i></a></span>&nbsp;';
                        
                    echo '<span><a class="btn btn-primary" href="'.url('rotate_right/'.$transac_id.'/'.$img_list[$i]).'/'.$year.'/'.$quarter.'/'.$dlrid.'/'.$dlrname.'/'.$dlrsize.'/'.$section_type.'/'.$ctent_type.'/'.$topic_name.'"><i class="fa fa-repeat" aria-hidden="true"></i></a></span>';
                    echo '</div>';
                }
            }
        ?>
        
        </div>        
        <br><br>
        <div id="delete_result"></div>
        <div class="row col-xs-12 col-md-12" style="border-top: 1px solid #ccc; margin-top: 30px; padding-top: 30px;"></div>
        <div class="row col-xs-12 col-sm-12 col-md-12">
            <div id="eventsmessage"></div>
            <div id="addnew" class="pull-left">Add new</div>        
        </div>
        <div class="row col-xs-12 col-sm-12 col-md-12" style="margin-bottom: 50px;">
            <span class="pull-left text-left" style="margin-top: 20px;">
               {{--  <div id="add_product" class="ajax-file-upload-green pull-left" style="margin-top:5px;">อัพโหลด</div> --}}
            </span>
        </div>

    </div>
</div>  
@endsection

@section('javascript')

<script src="{{ asset('js2/jquery-1.11.0.js') }}"></script>
<script src="{{ asset('js2/jquery.uploadfile.js') }}"></script>
<script src="{{ asset('js2/add_new_img.js') }}"></script>
<script src="{{ asset('js2/jquery.form.js') }}"></script>

<script type="text/javascript">
        
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function rotate_r(a){
        //alert(a);
        //$('#img2').hide();
        //img_result
        var b = a.split(",");
        //confirm("Press a button!");
        
        //alert('yes');
        var data_str = "string="+a;
        $.ajax({
            type: "POST",
            url: "delete_img.php",
            data: data_str,
            success: function(data){
                $('#delete_result').html(data);
                //$('#result_area').html(data);
                //consloe.log(data);                    
            }
        });
        //$('#div_img14').hide();
        $('#'+b[2]).hide();
        
    }

    function godelete(a){
        //alert(a);
        //$('#img2').hide();
        //img_result
        var b = a.split(",");
        //confirm("Press a button!");

        if(confirm("คุณต้องการจะลบรูปนี้จริงหรือไม่")){
            //alert('yes');
            var data_str = "string="+a;
            $.ajax({
                type: "POST",
                url: "delete_img.php",
                data: data_str,
                success: function(data){
                    $('#delete_result').html(data);
                    //$('#result_area').html(data);
                    //consloe.log(data);                    
                }
            });
            //$('#div_img14').hide();
            $('#'+b[2]).hide();
        } 
    }

    $(document).ready(function(){
        

        //alert("test start");
        /*var _token = $('#_token').val();        
       $('#ctent_type').change(function(){        
            var ctent_type = $('#ctent_type').val();
            //alert(ctent_type);
            //alert(_token);
            //console.log();
            $.post('dlr_to_report',
                {_token:_token, ctent_type:"test"}, function(data){
                    console.log(data);
                    $('#result_area').html('');
            });

            var data_str = "firstname=Apichai&ctent_type="+ctent_type;
            $.ajax({
                type: "POST",
                url: "dlr_to_report",
                data: data_str,
                success: function(data){
                    $('#dealer').html(data);
                    //$('#result_area').html(data);
                    //consloe.log(data);
                }
            });
       });*/ 
    });

</script>

@endsection