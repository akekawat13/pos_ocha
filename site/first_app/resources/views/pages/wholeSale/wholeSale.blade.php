@extends('layouts.appPeanutHome')

@section('scr-datatable')
    <script src="{{asset('js_insa/plugins/dataTables/datatables.min.js')}}"></script>
    <script src="{{asset('js_insa/plugins/dataTables/dataTables.bootstrap4.min.js')}}"></script>

    <!-- Flot demo data -->
    <script src="{{asset('js_insa/demo/flot-demo.js')}}"></script>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]

            });

        });

    </script>
@endsection

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>ขายส่ง</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Home</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>ขายส่ง</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight" >
    <div class="row">
        <div class="col-lg-6">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>รายงานสินค้าขายดี</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-4 m-b-xs"><select class="form-control-sm form-control input-s-sm inline">
                            <option value="0">เลือกเดือน</option>
                        </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="flot-chart">
                                <div class="flot-chart-pie-content" id="flot-pie-chart-upsale"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>รายงานสินค้าขายไม่ดี</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-4 m-b-xs"><select class="form-control-sm form-control input-s-sm inline">
                            <option value="0">เลือกเดือน</option>
                        </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="flot-chart">
                                <div class="flot-chart-pie-content" id="flot-pie-chart-downsale"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight" style="margin-top: -60px !important;">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>ขายส่งสินค้า</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-4 m-b-xs"><select class="form-control-sm form-control input-s-sm inline">
                            <option value="0">เลือกร้านค้า</option>
                        </select>
                        </div>
                        <div class="col-sm-4"></div>
                        <div class="col-sm-4 m-b-xs text-right">
                            <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                <label class="btn btn-sm btn-success">
                                    <input type="radio" name="options" id="option1" autocomplete="off" checked> Day
                                </label>
                                <label class="btn btn-sm btn-warning active">
                                    <input type="radio" name="options" id="option2" autocomplete="off"> Week
                                </label>
                                <label class="btn btn-sm btn-info">
                                    <input type="radio" name="options" id="option3" autocomplete="off"> Month
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                            <tr>
                                <th>ลำดับ</th>
                                <th>รายการสินค้า</th>
                                <th>ราคาส่ง</th>
                                <th>ส่งรอบที่ 1</th>
                                <th>ส่งรอบที่ 2</th>
                                <th>รวม</th>
                                <th>ขายได้</th>
                                <th>คงเหลือ</th>
                                <th>ยอดขายเป็นชิ้น</th>
                            </tr>
                        </thead>
                        <tbody>
                            @for ($i = 1; $i <= 5; $i++)
                                <tr>
                                    <td class="text-center">{{$i}}</td>
                                    <td>ถั่ว</td>
                                    <td class="text-center">50.00</td>
                                    <td class="text-center">12 ขวด</td>
                                    <td class="text-center">1 กล่อง</td>
                                    <td class="text-center">24</td>
                                    <td class="text-center">10</td>
                                    <td class="text-center">14</td>
                                    <td class="text-center">50</td>
                                </tr>
                            @endfor
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="2" class="text-center">รวม</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tfoot>
                        </table>
                    </div>

                </div>
            </div>

        </div>
    </div>
</div>

@endsection
