<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title></title>

	<link rel="stylesheet" type="text/css" href="css/print.css" media="print">

	{{-- <link href="https://fonts.googleapis.com/css2?family=Sarabun:wght@400&display=swap" rel="stylesheet"> --}}

	<style>
		/* Create three unequal columns that floats next to each other */
		.column_1 {
		  	float: left;
		  	padding: 2px;
		}
		.column_2 {
		  	float: right;
		  	padding: 2px;
		}

		/* Left and right column */
		.column.side {
		  	width: 25%;
		}

		/* Middle column */
		.column.middle {
		  	width: 50%;
		}
		/* Clear floats after the columns */
		.row:after {
		  	content: "";
		  	display: table;
		  	clear: both;
		}
		@font-face {
		  font-family: 'Garuda';
		  src: url("{{asset('fonts/garuda/Garuda.ttf')}}")format('truetype');
		  font-weight: normal;
		  font-style: normal;
		}
		@font-face {
		  font-family: 'THSarabunNew';
		  src: url("{{asset('fonts/THSarabunNew/THSarabunNew.ttf')}}")format('truetype');
		  font-weight: normal;
		  font-style: normal;
		}
		body {
			font-family: 'Garuda', sans-serif;
		}
		@page {
	      	size: A4;
	      	padding: 15px;
	    }
	    @media print {
	      	html, body {
	        	width: 210mm;
	        	height: 297mm;
	        	/*font-size : 16px;*/
	      	}
	    }
	</style>
</head>
<body style="padding: 0 !important;margin: 0 !important;font-size: 12px;color: #000000;">
	{{-- <button id="print" type="button" class="btn btn-sm btn-info"><i class="fa fa-print"></i> พิมพ์ตัวอย่าง</button>
	<button onclick="ExportPdf()" type="button" class="btn btn-outline-warning ml-3">
		<i class="far fa-file-pdf"></i> PDF Download
	</button> --}}

    <div style="text-align: center;padding: 15px;width: 200px;" id="myPDF">
    	<div>
    		<img src="{{ $data['logo'] }}" alt="logo" width="30%" />
    	</div>
    	<div id="shopNameTo">
    		{{$data['receipt'][0]->shop_name}}
    	</div>
    	<div id="addressTo">
    		{{$data['receipt'][0]->shop_address}}
    	</div>
    	<div id="taxTo">
    		เลขผู้เสียภาษี <span>{{$data['receipt'][0]->tax_id}}</span>
    	</div>
    	<hr>
    </div>

	<div style="padding: 15px;width: 200px;">
		@foreach($data['data_all'] as $key => $value)

			<div class="row">
				<div class="column_1 middle">
					{{$value->pro_name}} <span>X {{$value->pro_quantity}}</span>
	            	<div>
	            		<small>{{$value->size_name}}</small>
	            	</div>
				</div>

				<div class="column_2 side">
					<div>
						<span class="font-weight-bold" style="margin-left: 7px;">{{$value->pro_price}}</span><br>
						<span style="margin-left: 13px;">{{$value->size_price}}</span>
					</div>
				</div>
			</div>

            @if($value->toppings != "-")
                @php
                    $arr_toppings = json_decode($value->toppings);
                @endphp
                @foreach($arr_toppings as $key2 => $value2)
                	<div class="row">
						<div class="column_1 middle"><small>{{$value2->name}}</small></div>
						<div class="column_2 side">{{$value2->price}}.00</div>
					</div>
                @endforeach
            @endif

	    @endforeach
	</div>

	<div style="text-align: center;padding: 15px;width: 200px;">
    	<hr>
    		<div class="row">
				<div class="column_1 middle">รวม</div>
				<div class="column_2 side">
					@php
						echo $data['data_money'][0]->sum_total - $data['data_money'][0]->sum_vat.".00";
					@endphp
				</div>
			</div>
			<div class="row">
				<div class="column_1 middle">vat</div>
				<div class="column_2 side">7 %</div>
			</div>
			<div class="row">
				<div class="column_1 middle">รวม vat</div>
				<div class="column_2 side" style="font-weight: bold;">{{$data['data_money'][0]->sum_total}}</div>
			</div>
    	<hr>

    	<div id="last_textTo">
    		{{$data['receipt'][0]->last_text}}
    	</div>
    	<div id="phoneTo">
    		โทร. {{$data['receipt'][0]->shop_phone}}
    	</div>
    </div>

    <button id="print" type="button" class="btn btn-sm btn-info"><i class="fa fa-print"></i> พิมพ์ตัวอย่าง</button>
	<!-- <button onclick="ExportPdf()" type="button" class="btn btn-outline-warning ml-3">
		<i class="far fa-file-pdf"></i> PDF Download
	</button> -->
	<a href="Myreceipt.pdf" class="btn btn-success btn-sm">PDF Download</a>

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    {{-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script> --}}

    <!-- Mainly scripts -->
    <script src="js/jquery-3.1.1.min.js"></script>
    <script type="text/javascript">
    	//window.print();
		$('#print').click(function(){
			// alert();
	
			window.print();
		});
		// function ExportPdf(){ 

		// 	kendo.drawing
		// 	    .drawDOM("#myPDF", 
		// 	    { 
		// 	        paperSize: "A4",
		// 	        margin: { left: "0.2cm", right: "0.2cm", top: "0.1cm", bottom: "0.1cm" },
		// 	        scale: 0.65,
		// 	        height: 500
		// 	    })
		// 	        .then(function(group){
		// 	        kendo.drawing.pdf.saveAs(group, "Exported.pdf")
		// 	    });
		// }
	</script>
</body>
</html>