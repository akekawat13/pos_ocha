@php

//require_once __DIR__ . '/vendor/autoload.php';
// @include('vendor.autoload');

// $defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
// $fontDirs = $defaultConfig['fontDir'];

// $defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
// $fontData = $defaultFontConfig['fontdata'];

// $mpdf = new \Mpdf\Mpdf([
//     'fontDir' => array_merge($fontDirs, [
//         __DIR__ . '/tmp',
//     ]),
//     'fontdata' => $fontData + [
//         // 'sarabun' => [
//         //     'R' => 'THSarabun.ttf',
//         //     'I' => 'THSarabun Italic.ttf',
//         //     'B' =>  'THSarabun Bold.ttf',
//         // ],
//         'garuda' => [
//             'N' => 'Garuda.ttf',
//             'B' =>  'Garuda-Bold.ttf',
//         ]
//     ],
//     'default_font' => 'garuda',
//     'default_font_size' => 12,
//     'margin_top' => 0,
//     'margin_left' => 0,
//     'margin_right' => 0,
//     'margin_bottom' => 0,
//     'padding' => 0,
//     'mirrorMargins' => true,
// ]);

// ob_start();
@endphp

<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Language" content="th" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title></title>

	{{-- <link rel="stylesheet" type="text/css" href="{{asset('css/print.css')}}" media="print"> --}}
	{{-- <link href="{{asset('css/print_receipt.css')}}" rel="stylesheet"> --}}
    {{-- <link rel="stylesheet" media="mediatype and|not|only (expressions)" href="{{asset('css/print_receipt.css')}}"> --}}

	<style>

        body {
            /*font-family: 'sarabun';*/
            margin: 0;
            font-size: 12pt;
        }

	</style>
</head>
<body style="">
	{{-- <button id="print" type="button" class="btn btn-sm btn-info"><i class="fa fa-print"></i> พิมพ์ตัวอย่าง</button>
	<button onclick="ExportPdf()" type="button" class="btn btn-outline-warning ml-3">
		<i class="far fa-file-pdf"></i> PDF Download
	</button> --}}

    <div style="text-align: center;padding: 10px 0px 10px 0px;width: 250px;" id="myPDF">
    	<div>
    		<img src="images/{{$data['receipt'][0]->shop_logo}}" alt="" width="30%">
    	</div>
    	<div id="shopNameTo">
    		{{$data['receipt'][0]->shop_name}}
    	</div>
    	<div id="addressTo" style="padding: 0px 25px 0px 25px;">
    		{{$data['receipt'][0]->shop_address}}
    	</div>
    	<div id="taxTo" style="padding: 0px 20px 0px 17px;">
    		เลขผู้เสียภาษี <span>{{$data['receipt'][0]->tax_id}}</span>
    	</div>
    	<hr>
    		@foreach($data['data_all'] as $key => $value)
    			<div class="table" style="padding: 5px;width: 250px;">
    				<table>
			            <tr>
			                <td style="width: 150px;text-align: left;" class="">
			                	{{$value->pro_name}} <span>X {{$value->pro_quantity}}</span>
				            	<div>
				            		<small>{{$value->size_name}}</small>
				            	</div>
			                </td>
			                <td style="width: 100px;text-align: right;padding: 0px 20px 0px 0px;">
			                	<div>
									{{$value->pro_price}}
								</div>
								<div>
									{{$value->size_price}}
								</div>
			                </td>
			            </tr>
			            @if($value->toppings != "-")
			                @php
			                    $arr_toppings = json_decode($value->toppings);
			                @endphp
			                @foreach($arr_toppings as $key2 => $value2)
			                	<tr>
					                <td style="width: 150px;text-align: left;" class="">{{$value2->name}}</td>
					                <td style="width: 100px;text-align: right;padding: 0px 20px 0px 0px;">{{$value2->price}}.00</td>
					            </tr>
			                @endforeach
			            @endif
			        </table>
    			</div>
    		@endforeach
    	<hr>
    		<div class="table" style="padding: 5px;width: 250px;">
				<table>
		            <tr>
		                <td style="width: 150px;text-align: left;" class="">รวม</td>
		                <td style="width: 100px;text-align: right;padding: 0px 20px 0px 0px;">
		                	@php
								echo $data['data_money'][0]->sum_total - $data['data_money'][0]->sum_vat.".00";
							@endphp
		                </td>
		            </tr>
		            <tr>
		                <td style="width: 150px;text-align: left;" class="">vat</td>
		                <td style="width: 100px;text-align: right;padding: 0px 20px 0px 0px;">7 %</td>
		            </tr>
		            <tr>
		                <td style="width: 150px;text-align: left;" class="">รวม vat</td>
		                <td style="width: 100px;text-align: right;padding: 0px 20px 0px 0px;font-weight: bold;">{{$data['data_money'][0]->sum_total}}</td>
		            </tr>
		        </table>
			</div>
    	<hr>
    	<div id="last_textTo">
    		{{$data['receipt'][0]->last_text}}
    	</div>
    	<div id="phoneTo">
    		โทร. {{$data['receipt'][0]->shop_phone}}
    	</div>
    </div>



    @php
        // $html = ob_get_contents();
        // $mpdf->SetDisplayMode('fullwidth');
        // $mpdf->WriteHTML($html);
        // $mpdf->Output('Myreceipt.pdf');
        // ob_end_flush();
    @endphp

   {{--  <embed
    	type="application/pdf"
    	src="{{asset('Myreceipt.pdf')}}"
    	id="myPDF"
    	width="100%"
    	height="100%"
    /> --}}

    {{-- <button id="print" type="button" class="btn btn-sm btn-info"><i class="fa fa-print"></i> พิมพ์ตัวอย่าง</button> --}}
    <!-- <button onclick="ExportPdf()" type="button" class="btn btn-outline-warning ml-3">
        <i class="far fa-file-pdf"></i> PDF Download
    </button> -->
    {{-- <a href="Myreceipt.pdf" class="btn btn-success btn-sm">PDF Download</a> --}}

    <!-- Mainly scripts -->
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="https://kendo.cdn.telerik.com/2017.2.621/js/jquery.min.js"></script>{{-- dependency for Kendo UI API --}}
 	<script src="https://kendo.cdn.telerik.com/2017.2.621/js/jszip.min.js"></script>
 	<script src="https://kendo.cdn.telerik.com/2017.2.621/js/kendo.all.min.js"></script>
    <script type="text/javascript">
    	//window.print();
		$('#print').click(function(){
			window.print();
		});

		function ExportPdf(myPDF){
			var doc = document.getElementById('myPDF')
			if(typeof doc.print == 'undefined'){
				setTimeout(function(){printDocument('myPDF');},1000);
			}else{
				doc.print();
			}
		}
	</script>
</body>
</html>
