@extends('layouts.app_salepage_charge')

@section('content')
<div class="container ml-2">
	<br>
    <div class="row">
    	<div class="col-lg-6 text-right" style="border-right: 2px solid #0BCAF3;">
    		<div class="font-weight-bold f_50">{{$data_transaction_tamp['data_money'][0]->sum_total}}</div>
    		<div class="text-muted font-weight-bold">Total paid</div>
    	</div>
    	<div class="col-lg-6 text-left">
    		<div class="font-weight-bold f_50 change_to text-danger">00.00</div>
    		<div class="text-muted font-weight-bold">Change</div>
    	</div>
    </div>
    <br>
    {{-- <form action="{{url('payment')}}" method="post" accept-charset="utf-8">
		@csrf --}}
    <div class="row">
    	<div class="col-lg-2"></div>
    	<div class="col-lg-8">
    		<div class="row align-items-end">
		    	<div class="col-lg-12">
		    		<div class="text-success font-weight-bold ml-3">Cash received</div>
		    	</div>
		    	<div class="w-100"></div>
		    	<div class="col-lg-1"><i class="fa fa-money ml-3 fa-2x"></i></div>
		    	<div class="col-lg-8 font-weight-bold" style="font-size: 18px;">
		    		<div class="form__group field">
					  	<input type="number" class="form__field" placeholder="{{$data_transaction_tamp['data_money'][0]->sum_total}}" name="total_new" value=""/>
					</div>
					<div style="border-bottom: 4px solid #fff;"></div>
		    	</div>
		    	<div class="col-lg-3 text-center">
		    		<input type="hidden" name="total_old" value="{{$data_transaction_tamp['data_money'][0]->sum_total}}">
		    		{{-- <button type="submit" class="btn btn-w-m btn-primary btn-lg bh_60">CHARGE</button> --}}
		    		<button 
		    			id="submit_payment" 
		    			type="button" 
		    			class="btn btn-w-m btn-primary btn-lg bh_60" 
		    			>CHARGE</button>
		    	</div>
		    	<div class="w-100"></div>
		    	<div class="col-12 col-lg-12 text-center mt-2" id="chk_charge" style="display: none;">
		    		<div class="alert alert-danger alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        จำนวนเงินไม่ถูกต้อง กรุณากรอกใหม่.
                    </div>
		    	</div>
		    </div>
			<br>
			@php
				$total_current = intval($data_transaction_tamp['data_money'][0]->sum_total);
				//var_dump($total_current);
				if($total_current < 50){
					$pay_total = "50.00";
				}elseif($total_current > 50 && $total_current < 100){
					$pay_total = "100.00";
				}elseif($total_current > 100 && $total_current < 500){
					$pay_total = "500.00";
				}elseif($total_current > 500 && $total_current < 1000){
					$pay_total = "1000.00";
				}elseif($total_current >= 1000 && $total_current < 1500){
					$pay_total = "1500.00";
				}elseif($total_current >= 1500 && $total_current < 2000){
					$pay_total = "2000.00";
				}

				$str_explo = explode('.',$data_transaction_tamp['data_money'][0]->sum_total);
				//print_r($str_explo);
				$str_sp = substr($str_explo[0],-1);
				$split = intval($str_sp);
				//echo $split;
				if($split < 5){
					$num_1 = 5-$split;
					$num_2 = 5;
					//echo $num_1.'<br>'.$num_2;
				}elseif($split >= 5 && $split < 10){
					$num_1 = 10-$split;
					$num_2 = 5; 
				}elseif($split == 10){
					$num_1 = 5;
					$num_2 = 10; 
				}
			@endphp
		    <div class="row" id="btt_quick_amount_cash">
		    	<div class="col-lg-3 text-center">
		    		<input type="hidden" name="quick_amount_cash_1" value="{{$data_transaction_tamp['data_money'][0]->sum_total}}">
		    		<button type="button" class="btn btn-w-m btn-default bh_60 btt1">{{$data_transaction_tamp['data_money'][0]->sum_total}}</button>
		    	</div>
		    	<div class="col-lg-3 text-center">
		    		<button type="button" class="btn btn-w-m btn-default bh_60 btt2">
		    			@php
		    				$pay_total2 = $total_current+$num_1.".00";
		    				echo $pay_total2;
		    			@endphp
		    			<input type="hidden" name="quick_amount_cash_2" value="{{$pay_total2}}">
		    		</button>
		    	</div>
		    	<div class="col-lg-3 text-center">
		    		<button type="button" class="btn btn-w-m btn-default bh_60 btt3">
		    			@php
		    				$pay_total3 = $pay_total2+$num_2.".00";
		    				echo $pay_total3;
		    			@endphp
		    			<input type="hidden" name="quick_amount_cash_3" value="{{$pay_total3}}">
		    		</button>
		    	</div>
		    	<div class="col-lg-3 text-center">
		    		<input type="hidden" name="quick_amount_cash_4" value="{{$pay_total}}">
		    		<button type="button" class="btn btn-w-m btn-default bh_60 btt4">{{$pay_total}}</button>
		    	</div>
		    </div>
			<br>

		    <div class="row" id="card_cash">
		    	<div class="col-lg-12 text-center">
		    		<button type="button" class="btn btn-w-m btn-default bh_60 col-12" disabled="disabled"><i class="fa fa-cc-mastercard"></i> CARD</button>
		    	</div>
		    </div>
			<br>
		    <div class="row" id="check_cash">
		    	<div class="col-lg-12 text-center">
		    		<button type="button" class="btn btn-w-m btn-default bh_60 col-12" disabled="disabled"><i class="fa fa-credit-card"></i> CHECK</button>
		    	</div>
		    </div>

		    <!--- commple charge display none start -->
			<div id="comple_charge" style="display: none;">
				{{-- <div class="row align-items-end" id="send_receipt_to_email">
			    	<div class="col-lg-1"><i class="fa fa-envelope ml-3 fa-2x"></i></div>
			    	<div class="col-lg-8 font-weight-bold" style="font-size: 18px;">
			    		<div class="form__group field">
						  	<input type="email" class="form__field" placeholder="Enter email" name="send_receipt_email" value="" />
						</div>
						<div style="border-bottom: 4px solid #fff;"></div>
			    	</div>
			    	<div class="col-lg-3 text-center">
			    		<button type="button" class="btn btn-w-m btn-info btn-lg bh_60" id="send_receipt" disabled="disabled">SEND RECEIPT</button>
			    	</div>
			    </div> --}}
			    <div class="row mt-3" id="print_receipt">
			    	<div class="col-lg-12 text-center">
			    		<form action="{{url('print_receipt')}}" method="post" accept-charset="utf-8">
			    			@csrf
			    			<input type="hidden" name="usr_id" value="{{Auth::id()}}">
		    				<input type="hidden" name="trn_id" value="{{$data_transaction_tamp['data_all'][0]->trn_id}}">
		    				<input type="hidden" name="trn_doc_no" value="{{$data_transaction_tamp['data_trn_doc_no'][0]->trn_doc_no}}">
			    			<button type="submit" class="btn btn-w-m btn-default bh_60 col-12"><i class="fa fa-print"></i> PRINT RECEIPT</button>
			    		</form>
			    	</div>
			    </div>
			    <div class="row" id="new_sale" style="margin-top: 30px;">
			    	<div class="col-lg-12 text-center">
			    		<button type="button" class="btn btn-w-m btn-primary bh_60 col-12" onclick="location.href='{{route('sale.page')}}'"><i class="fa fa-check"></i> NEW SALE</button>
			    	</div>
			    </div>
			</div>
		    <!--- commple charge display none end -->

    	</div>
    </div>
	{{-- </form> --}}

</div><!-- Container -->
@endsection

@section('script_sale_page_charge')
	<script>
        $(document).ready(function(){ 
        	//console.log("CHARGE");
        	$('#submit_payment').click(function(){
        		var tt_old = $('input[name=total_old]').val();
        		var tt_new = $('input[name=total_new]').val();
        		//console.log(tt_new);
        		if (tt_new == "") {
        			$('.change_to').html("00.00").addClass('text-danger');
        			$('#chk_charge').css('display','block');
        		}else if (parseInt(tt_new) < parseInt(tt_old)) {
        			$('.change_to').html("00.00").addClass('text-danger');
        			$('#chk_charge').css('display','block');
        		}else if(parseInt(tt_new) >= parseInt(tt_old)) {
        			$('#chk_charge').css('display','none');
        			var charge_money = parseInt(tt_new) - parseInt(tt_old);
        			$('.change_to').html(charge_money+".00").removeClass('text-danger').css('color','#28a745');
        			$('#btt_quick_amount_cash').css('display','none');
        			$('#card_cash').css('display','none');
        			$('#check_cash').css('display','none');
        			$(this).attr('disabled','disabled');
        			$('#comple_charge').css('display','block');
        			//back top bar
        			$('.minimalize-styl-2').css('display','none');
        			$.ajax({
                        type : "POST",
                        url : "{{route('charge_completed')}}",
                        data : {
                            '_token' : $('input[name=_token]').val(),
                            'usr_id' : $('input[name=usr_id]').val(),
                            'trn_id' : $('input[name=trn_id]').val(),
                            'trn_doc_no' : $('input[name=trn_doc_no]').val(),
                        },
                        success:function(data){
                            console.log("charge_completed = "+data.status);
                            // swal("Deleted!", "Your imaginary file has been deleted.", "success");
                        }
                    });
        			//console.log(charge_money);
        		}
        		
        	});

        	$('.btt1').click(function(){
        		var btt_1 = $('input[name=quick_amount_cash_1]').val();
        		$('input[name=total_new]').val(btt_1);
        	});
        	$('.btt2').click(function(){
        		var btt_2 = $('input[name=quick_amount_cash_2]').val();
        		$('input[name=total_new]').val(btt_2);
        	});
        	$('.btt3').click(function(){
        		var btt_3 = $('input[name=quick_amount_cash_3]').val();
        		$('input[name=total_new]').val(btt_3);
        	});
        	$('.btt4').click(function(){
        		var btt_4 = $('input[name=quick_amount_cash_4]').val();
        		//console.log(typeof(btt_4));
        		$('input[name=total_new]').val(btt_4);
        	});

        	//key up send email
        	$('input[name=send_receipt_email]').keyup(function(){
        		$('#send_receipt').removeAttr('disabled');
        	});
        	//Send to email
        	$('#send_receipt').click(function(){
        		var customer_email = $('input[name=send_receipt_email]').val();
        		console.log("mail to ="+customer_email);
        	});
        });



    </script>
@endsection