@extends('layouts.app_salepage_charge')

@section('content')
<div class="container ml-2">
	<br>
    <div class="row">
    	<div class="col-lg-6 text-right" style="border-right: 2px solid #0BCAF3;">
    		<div class="font-weight-bold f_45">{{$data_transaction_tamp['total_old']}}</div>
    		<div class="text-muted">Total paid</div>
    	</div>
    	<div class="col-lg-6 text-left">
    		<div class="font-weight-bold f_45 text-danger">{{$data_transaction_tamp['total_change']}}.00</div>
    		<div class="text-muted">Change</div>
    	</div>
    </div>
    <br>

    <div class="row">
    	<div class="col-lg-2"></div>
    	<div class="col-lg-8">
    		<br>
    		<div class="row align-items-end">
		    	<div class="col-lg-1"><i class="fa fa-envelope ml-3 fa-2x"></i></div>
		    	<div class="col-lg-8 font-weight-bold" style="font-size: 18px;">
		    		<div class="form__group field">
					  	<input type="email" class="form__field" placeholder="Enter email" name="email" value="" />
					</div>
					<div style="border-bottom: 4px solid #fff;"></div>
		    	</div>
		    	<div class="col-lg-3 text-center">
		    		<button type="button" class="btn btn-w-m btn-info btn-lg bh_60">SEND RECEIPT</button>
		    	</div>
		    </div>
			
		    <div class="row" style="margin-top: 200px;">
		    	<div class="col-lg-12 text-center">
		    		<button type="button" class="btn btn-w-m btn-primary bh_60 col-12" onclick="location.href='/sale_page'"><i class="fa fa-check"></i> NEW SALE</button>
		    	</div>
		    </div>
    	</div>
    </div>

</div><!-- Container -->
@endsection

@section('script_sale_page_charge_payment')
	<script>
        $(document).ready(function(){ 
        	$('.minimalize-styl-2').css('display','none');
        });
    </script>
@endsection