@extends('layouts.app_salepage')

@section('content')
<div class="container" style="padding-top: 60px;">
    <div class="row mt-2 mb-2 all_pro"></div> <!-- ROW -->

    {{-- <div class="row mt-2 mb-2 DataTo" id="all_pro_select_category"></div> --}}

    <!-- Modal Products Option -->
    @include('pages.sale_pages.pro_option')
    @include('pages.sale_pages.suspend')
    @include('pages.sale_pages.pro_ticket_option')

</div><!-- Container -->
@endsection

@section('script_sale_page')
    <script type="text/javascript">

        $(document).ready(function(){
            //alert($('input[name=_token]').val());
            $('#save_ticket').css('pointer-events','none');
            $('#charge_ticket').css('pointer-events','none');
            var check_ojb = $('.print_total').text();
            if(check_ojb == ""){
                $('#save_ticket').css('pointer-events','none');
                $('#charge_ticket').css('pointer-events','none');
            }else{
                $('#save_ticket').css('pointer-events','auto');
                $('#charge_ticket').css('pointer-events','auto');
            }
            // //chk pro quantity
            // var pro_qty = $('.product-price-bg-'+data[i].category_id+'').text();
            // console.log(pro_qty);
            // if(parseInt(pro_qty) == 0){
            //     // $('#pro_option').css('pointer-events','none');
            // }

            //alert('test');
            // $('#pro_ticket_option').modal({
            //     keyboard: false
            // });

            $(".touchspin1").TouchSpin({
                buttondown_class: 'btn btn-danger',
                buttonup_class: 'btn btn-primary'
            });

            //onload show products
            print_products();
            check_suspend();

            //search products on category
            $('.sort_category').click(function(){
                var category_id = $(this).data('id');
                $('.all_pro').html("");
                //console.log("category ID ="+category_id);
                $.ajax({
                    type : "POST",
                    url:"{{ route('sort_category_submit') }}",
                    //url : '/sort_category_submit',
                    data : {
                      '_token' : $('input[name=_token]').val(),
                      'category_id' : category_id,
                    },
                    success:function(data){
                        //console.log("จำนวน ที่หาเจอ ="+data.length+"="+data[0].name_category);
                        $(".all_pro").html("");
                        for (var i = 0 ; i < data.length; i++) {
                            if(data[i].productImg != ""){
                                var pro_img = "productImages/"+data[i].productImg;
                                //console.log(pro_img);
                                $(".all_pro").append('<div class="col-xl-3">'+
                                    '<div class="ibox" data-toggle="modal" data-target="#pro_option" style="cursor: pointer;" data-id="'+data[i].id+'" data-name="'+data[i].name_th+'" data-price="'+data[i].price1+'.00" onclick="pro_option_detail('+data[i].id+',\''+data[i].name_th+'\','+data[i].price1+')">'+
                                        '<div class="ibox-content product-box">'+
                                            '<div class="product-imitation"><img src="'+pro_img+'" class="img-fluid"></div>'+
                                            '<div class="product-desc"><span class="product-price">'+data[i].price1+'.00</span>'+
                                            '<span class="product-price-bg-'+data[i].category_id+' pro_qty_'+data[i].id+'" data-pro_qty="'+data[i].quantity+'">'+data[i].name_category+'</span>'+
                                            '<small class="text-muted">'+data[i].name_en+'</small><a href="#" class="product-name" style="font-size: 15px;"> '+data[i].name_th+'</a><input type="hidden" name="pro_id"value="'+data[i].id+'"></div>'+
                                        '</div>'+
                                    '</div><br></div>');
                            }else{
                                $(".all_pro").append('<div class="col-xl-3">'+
                                    '<div class="ibox" data-toggle="modal" data-target="#pro_option" style="cursor: pointer;" data-id="'+data[i].id+'" data-name="'+data[i].name_th+'" data-price="'+data[i].price1+'.00" onclick="pro_option_detail('+data[i].id+',\''+data[i].name_th+'\','+data[i].price1+')">'+
                                        '<div class="ibox-content product-box">'+
                                            '<div class="product-imitation"><img src="productImages/no_image.png" class="img-fluid"></div>'+
                                            '<div class="product-desc"><span class="product-price">'+data[i].price1+'.00</span>'+
                                            '<span class="product-price-bg-'+data[i].category_id+' pro_qty_'+data[i].id+'" data-pro_qty="'+data[i].quantity+'">'+data[i].name_category+'</span>'+
                                            '<small class="text-muted">'+data[i].name_en+'</small><a href="#" class="product-name" style="font-size: 15px;"> '+data[i].name_th+'</a><input type="hidden" name="pro_id"value="'+data[i].id+'"></div>'+
                                        '</div>'+
                                    '</div><br></div>');
                            }
                        }
                    }
                });
            });
            //search products on input
            $('input[name=search]').keyup(function(){
                // event.preventDefault();
                $(".all_pro").html("");
                //$('.DataTo').remove();
                console.log($(this).val());
                $.ajax({
                    type : "POST",
                    url : "{{route('search_products')}}",
                    data : {
                      '_token' : $('input[name=_token]').val(),
                      'name_pro' : $(this).val(),
                    },
                    success:function(data){
                        //console.log(data);
                        var img_pro = '<img src="{{asset("productImages/5e4f897929b27.jpg")}}" class="img-fluid">';
                        console.log("จำนวน ที่ค้นหาเจอ ="+data.length+"="+data[0].name_category);
                        $(".all_pro").html("");
                        for (var i = 0 ; i < data.length; i++) {
                            if(data[i].productImg != ""){
                                var pro_img = "productImages/"+data[i].productImg;
                                //console.log(pro_img);
                                $(".all_pro").append('<div class="col-xl-3">'+
                                    '<div class="ibox" data-toggle="modal" data-target="#pro_option" style="cursor: pointer;" data-id="'+data[i].id+'" data-name="'+data[i].name_th+'" data-price="'+data[i].price1+'.00" onclick="pro_option_detail('+data[i].id+',\''+data[i].name_th+'\','+data[i].price1+')">'+
                                        '<div class="ibox-content product-box">'+
                                            '<div class="product-imitation"><img src="'+pro_img+'" class="img-fluid"></div>'+
                                            '<div class="product-desc"><span class="product-price">'+data[i].price1+'.00</span>'+
                                            '<span class="product-price-bg-'+data[i].category_id+' pro_qty_'+data[i].id+'" data-pro_qty="'+data[i].quantity+'">'+data[i].name_category+'</span>'+
                                            '<small class="text-muted">'+data[i].name_en+'</small><a href="#" class="product-name" style="font-size: 15px;"> '+data[i].name_th+'</a><input type="hidden" name="pro_id"value="'+data[i].id+'"></div>'+
                                        '</div>'+
                                    '</div><br></div>');
                            }else{
                                $(".all_pro").append('<div class="col-xl-3">'+
                                    '<div class="ibox" data-toggle="modal" data-target="#pro_option" style="cursor: pointer;" data-id="'+data[i].id+'" data-name="'+data[i].name_th+'" data-price="'+data[i].price1+'.00" onclick="pro_option_detail('+data[i].id+',\''+data[i].name_th+'\','+data[i].price1+')">'+
                                        '<div class="ibox-content product-box">'+
                                            '<div class="product-imitation"><img src="productImages/no_image.png" class="img-fluid"></div>'+
                                            '<div class="product-desc"><span class="product-price">'+data[i].price1+'.00</span>'+
                                            '<span class="product-price-bg-'+data[i].category_id+' pro_qty_'+data[i].id+'" data-pro_qty="'+data[i].quantity+'">'+data[i].name_category+'</span>'+
                                            '<small class="text-muted">'+data[i].name_en+'</small><a href="#" class="product-name" style="font-size: 15px;"> '+data[i].name_th+'</a><input type="hidden" name="pro_id"value="'+data[i].id+'"></div>'+
                                        '</div>'+
                                    '</div><br></div>');
                            }
                        }
                    }
                });
            });

            //Save Changs product ticket
            $('#save_changes').click(function(){
                $('#save_ticket').css('pointer-events','auto');
                $('#charge_ticket').css('pointer-events','auto');
                var trn_id = $('input[name=trn_id]').val();
                var pro_id_changes = $('input[name=id_pro]').val();
                var pro_name_changes = $('#name_pro').html();
                var pro_price_changes = $('#price_pro').html();
                //var pro_option_size_name = $('#option_size_name').html();
                //var pro_option_size_price = $('#option_size_price').html();
                var data_topping = [];
                $(".topping_name_c").each(function() {
                    item = {}
                    item ["name"] = $(this).html();
                    item ["price"] = parseInt($(this).attr('topping_price'));
                    data_topping.push(item);
                });
                //console.log(typeof(data_topping));
                if (data_topping == '') {
                    var topping_price = 0;
                }else {
                    var topping_price = data_topping;
                }
                $.ajax({
                    type : "POST",
                    url : "{{route('add_transaction_temp')}}",
                    data : {
                      '_token' : $('input[name=_token]').val(),
                      'pro_id' : pro_id_changes,
                      'pro_name' : pro_name_changes,
                      'pro_price' : parseInt(pro_price_changes),
                      //'size_name' : pro_option_size_name,
                      //'size_price' : parseInt(pro_option_size_price),
                      'data_toppings' : data_topping,
                    },
                    success:function(data){
                        console.log(" insert data = "+data.status_1+","+data.status_2);
                        $('input[name=trn_id]').val(data.pro_id_trn_id[0].trn_id);
                        if (topping_price == 0) {
                            $('#option_to_ticket').append('<div class="col-lg-8" style="cursor: pointer;"'+
                                'onclick="pro_ticket_option('+data.pro_id_trn_id[0].trn_id+','+pro_id_changes+',\''+pro_name_changes+'\')">'+pro_name_changes+'<span>::X <span class="font-weight-bold pro_quantity_'+pro_id_changes+'">1</span></span></div><div class="col-lg-4 text-right"><span class="font-weight-bold">'+pro_price_changes+'</span></div><div class="w-100"></div>');
                        }else {
                            var topping_n = '';
                            var topping_p = '';
                            for (var i = 0; i < topping_price.length; i++) {
                                topping_n += '<br><small><span class="ml-3">- '+topping_price[i].name+'</span></small>';
                                topping_p += '<div>'+topping_price[i].price+'.00 </div>';
                            }
                            $('#option_to_ticket')
                                .append('<div class="col-lg-8" style="cursor: pointer;"'+
                                'onclick="pro_ticket_option('+data.pro_id_trn_id[0].trn_id+','+pro_id_changes+',\''+pro_name_changes+'\')">'+pro_name_changes+'<span>::X <span class="font-weight-bold pro_quantity_'+pro_id_changes+'">1</span></span>'+topping_n+'</div>'+
                                '<div class="col-lg-4 text-right">'+
                                    '<div class="font-weight-bold">'+pro_price_changes+'</div>'+topping_p+
                                '</div><div class="w-100"></div>');
                        }
                        //console.log(data);
                        $('#tax_total').html('<div class="col-lg-12"><hr></div><div class="col-lg-6 font-weight-bold">Total</div><div class="col-lg-6 text-right font-weight-bold print_total">'+data.pro_charge[0].sum_total+
                            '</div><input type="hidden" name="trn_doc_no" value="'+data.trn_doc_no+'">'+
                            '<input type="hidden" name="trn_id" value="'+data.pro_id_trn_id[0].trn_id+'">'+
                            '<input type="hidden" name="pro_id" value="'+data.pro_id_trn_id[0].pro_id+'">');
                        // $('.pro_qty').html("-1");
                        $('#pro_option').modal('hide');
                    }
                });
            });

            // Ticket Option Add Product Quantity
            $('.bootstrap-touchspin-up').click(function(){
                var add_qty = $('input[name=pro_quantity]').val();
                var pro_id = $('input[name=pro_id_ticket_option]').val();
                var pro_qty = parseInt($('.pro_qty_'+pro_id+'').text());
                if(add_qty >  pro_qty){
                    alert('จำนวนสินค้าไม่พอ');
                    $('#pro_ticket_option_save').attr('disabled','disabled');
                }else if(add_qty <= pro_qty) {
                    var value = pro_qty-add_qty;
                    console.log("สินค้าที่เหลือ ="+value);
                    $('#pro_ticket_option_save').removeAttr('disabled');
                }
            });
            $('.bootstrap-touchspin-down').click(function(){
                var add_qty = $('input[name=pro_quantity]').val();
                var pro_id = $('input[name=pro_id_ticket_option]').val();
                var pro_qty = parseInt($('.pro_qty_'+pro_id+'').text());
                if(add_qty >  pro_qty){
                    alert('จำนวนสินค้าไม่พอ');
                    $('#pro_ticket_option_save').attr('disabled','disabled');
                }else if(add_qty <= pro_qty) {
                    var value = pro_qty-add_qty;
                    console.log("สินค้าที่เหลือ ="+value);
                    $('#pro_ticket_option_save').removeAttr('disabled');
                }
            });
            $('#pro_ticket_option_save').click(function(){
                var pro_id = $('input[name=pro_id_ticket_option]').val();
                var pro_quantity = $('input[name=pro_quantity]').val();
                var trn_id = $('input[name=trn_id_ticket_option]').val();
                console.log("เพิ่มจำนวน ="+pro_quantity+", trn_id ="+trn_id+", pro_id ="+pro_id);
                //หาจำนวนที่เหลือของสินค้า
                var value_qty = (parseInt($('.pro_qty_'+pro_id+'').text())+1) - (parseInt(pro_quantity)+1);
                $.ajax({
                    type : "POST",
                    url : "{{route('pro_ticket_option_save')}}",
                    data : {
                        '_token' : $('input[name=_token]').val(),
                        'trn_id' : trn_id,
                        'pro_id' : pro_id,
                        'pro_quantity' : pro_quantity,
                    },
                    success:function(data){
                        // console.log(data.length);
                        console.log(data.pro_total_new);
                        $('.print_vat').html(data.pro_vat_new+".00");
                        $('.print_total').html(data.pro_total_new+".00");
                        $('.pro_quantity_'+data.pro_id).html(data.pro_quantity_new);
                        $('#pro_ticket_option').modal('hide');
                        $('.pro_qty_'+pro_id+'').html(value_qty);
                    }
                });
            });

            //Clear ticket
            // $(function(){
            //     $('.clear_ticket_al').trigger( "click" );
            // });
            $('.clear_ticket_al').click(function () {
                $('#save_ticket').css('pointer-events','none');
                $('#charge_ticket').css('pointer-events','none');
                var user_id = $('input[name=user_id]').val();
                var trn_id = $('input[name=trn_id]').val();
                var pro_id = $('input[name=pro_id]').val();
                console.log("TRN ID Clear ="+trn_id);
                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                }, function () {
                    //console.log(user_id);
                    $.ajax({
                        type : "POST",
                        url : "{{route('clear_ticket_al')}}",
                        data : {
                            '_token' : $('input[name=_token]').val(),
                            'user_id' : user_id,
                            'trn_id' : trn_id,
                            'pro_id' : pro_id,
                        },
                        success:function(data){
                            console.log(data+"delete = "+data.status);
                            swal("Deleted!", "Your imaginary file has been deleted.", "success");
                            $('#option_to_ticket').html("");
                            $('#tax_total').html("");
                            $('.on_data_for').remove();
                            if(data.id_suspend.length < 1){
                                $('#icon_suspend').html("");
                            }else if(data.id_suspend.length == 1){
                                $('#icon_suspend').html("");
                                $('#icon_suspend').append('<i class="fa fa-file-text-o text-warning"></i>');
                            }else{
                                $('#icon_suspend').html("");
                                $('#icon_suspend').append('<i class="fa fa-files-o text-warning"></i>');
                            }
                        }
                    });
                });
            });

            //Save ticket
            $('#save_ticket').click(function(){
                var user_id = $('input[name=user_id]').val();
                var trn_doc_no = $('input[name=trn_doc_no]').val();
                var trn_id = $('input[name=trn_id]').val();
                $.ajax({
                    type : "POST",
                    url : "{{route('save_ticket_onuser')}}",
                    data : {
                        '_token' : $('input[name=_token]').val(),
                        'user_id' : user_id,
                        'trn_doc_no' : trn_doc_no,
                        'trn_id' : trn_id,
                    },
                    success:function(data){
                        console.log("Save ="+data.status+", พักการขาย"+data.id_suspend.length);
                        $('#option_to_ticket').html("");
                        $('#tax_total').html("");
                        $('.on_data_for').html("");
                        $('#save_ticket').css('pointer-events','none');
                        $('#charge_ticket').css('pointer-events','none');
                        swal("พักการขาย!", "ข้อมูลการขายได้เก็บไว้แล้ว.", "success");
                        if(data.id_suspend.length < 1){
                            $('#icon_suspend').html("");
                        }else if(data.id_suspend.length == 1){
                            $('#icon_suspend').html("");
                            $('#icon_suspend').append('<i class="fa fa-file-text-o text-warning"></i>');
                        }else{
                            $('#icon_suspend').html("");
                            $('#icon_suspend').append('<i class="fa fa-files-o text-warning"></i>');
                        }
                        //$('.save_ticket_keyword_open').html(data.id_suspend.length);
                    }
                });
            });
            //Open Save ticket option
            $('#save_ticket_onopen').click(function(){
                $('#opens_save_ticket').html("");
                // var our = $(this);
                // our.css('display','none');
                // $('#save_ticket').css('display','block');
                var usr_id = $('input[name=user_id]').val();
                //console.log(usr_id)
                $.ajax({
                    type : "POST",
                    url : "{{route('save_ticket_onuser_open')}}",
                    data : {
                        '_token' : $('input[name=_token]').val(),
                        'usr_id' : usr_id,
                    },
                    success:function(data){
                        $('#option_to_ticket').html("");
                        $('#tax_total').html("");
                        $('.on_data_for').remove();
                        //console.log(data);
                        $('#open_suspend').modal('show');
                        var n = 1;
                        for (var i = 0 ; i < data.data_all.length; i++) {
                            var order = n+i;
                            $('#opens_save_ticket')
                            .append(
                                '<div class="col-sm-6 mt-1 mb-1 col-lg-6 border border-light bg-info" style="padding: 10px;"><div class="form-check abc-radio abc-radio-success"><input class="form-check-input" type="radio" name="trn_id" id="radio_'+data.data_all[i].id+'" value="'+data.data_all[i].id+'"><label class="form-check-label" for="radio_'+data.data_all[i].id+'">#ลำดับที่_'+order+'</label></div></div>'+
                                '<div class="col-sm-6 mt-1 mb-1 text-center border border-light bg-info" style="padding: 10px;">'+data.data_all[i].created_at+'</div>'+
                                '<div class="w-100"></div>'
                            );
                        }
                    }
                });
            });
            //Open Sale
            $('#open_sale').click(function(){
                // $('#opens_save_ticket').html("");
                // $('#save_ticket_onopen').html("");
                var trn_id = $( "input:checked" ).val();
                $('input[name=trn_id]').val(trn_id);
                console.log("trn_id ="+trn_id)
                $.ajax({
                    type : "POST",
                    url : "{{route('open_sale')}}",
                    data : {
                        '_token' : $('input[name=_token]').val(),
                        'trn_id' : trn_id,
                    },
                    success:function(data){
                        $('#open_suspend').modal('hide');
                        $('#save_ticket').css('pointer-events','auto');
                        $('#charge_ticket').css('pointer-events','auto');
                        //console.log(data)
                        for (var i = 0 ; i < data.data_all.length; i++) {
                            var str_topping = '';
                            if(data.data_all[i].toppings != '-'){
                                var obj_toppings = JSON.parse(data.data_all[i].toppings);
                                for (var v = 0 ; v < obj_toppings.length; v++) {
                                    str_topping += '<div class="col-lg-8"><span class="ml-3"> -'+obj_toppings[v].name+'</span></div><div class="col-lg-4 text-right">'+obj_toppings[v].price+'.00</div>';
                                }
                            }
                            $('#option_to_ticket').append(
                                '<div class="col-lg-8" style="cursor: pointer;" onclick="pro_ticket_option('+data.data_all[i].trn_id+','+data.data_all[i].pro_id+',\''+data.data_all[i].pro_name+'\')">'+data.data_all[i].pro_name+
                                '<span class="text-muted">X <span class="font-weight-bold pro_quantity_'+data.data_all[i].pro_id+'">'+data.data_all[i].pro_quantity+'</span></span></div><div class="col-lg-4 text-right"><span class="font-weight-bold">'+data.data_all[i].pro_price+'</div><div class="w-100"></div>'+str_topping
                            );
                        }
                        $('#tax_total').html('<div class="col-lg-12"><hr></div><div class="col-lg-6 font-weight-bold">Total</div><div class="col-lg-6 text-right font-weight-bold print_total">'+data.data_money[0].sum_total+'</div>');
                    }
                });
            });
        });//document.ready end

        //search products on category
        function pro_option_detail(pro_id,pro_name,pro_price){
            // $('.make_toppings div div').html("");
            $('#option_size_name').html("");
            $('#option_size_price').html("");
            $('.topping_name_c').remove();
            $('.topping_price_c').remove();
            $('.topping_button_c').remove();
            $('.ott_n').remove();
            $('.ott_p').remove();
            $('.ott_b').remove();
            $('input[name=id_pro]').val(pro_id);
            $('#name_pro').html(pro_name);
            $('#price_pro').html(pro_price+".00");
            //$('#save_changes').attr('disabled','disabled');
            //$('input[name=price_pro]').val(pro_price);
            //console.log("show modal detail ="+pro_id+" "+pro_name+" "+pro_price);
        }

        //Modal pro option
        function pro_option_size(option_size_name,option_size_price){
            $('#option_size_name').html(option_size_name);
            $('#option_size_price').html(option_size_price+".00");
            //$('#save_changes').removeAttr('disabled');
            //console.log("show modal option ="+option_size_name+" "+option_size_price);
        }
        function pro_option_topping(topping_id,option_topping_name,option_topping_price){
            $('.make_toppings').append('<div class="col-lg-6 mt-1 ott_n option_topping_name_'+topping_id+'"><div class="topping_name_c" topping_price="'+option_topping_price+'" style="font-size: 18px;font-weight: bold;">'+option_topping_name+'</div></div><div class="col-lg-5 ott_p mt-1 text-center option_topping_price_'+topping_id+' " style="font-size: 18px;font-weight: bold;"><div class="topping_price_c">'+option_topping_price+'.00 </div></div><div class="col-lg-1 ott_b mt-1 option_topping_price_'+topping_id+' topping_button_c"><button type="button" class="btn btn-sm btn-danger" onclick="delete_topping('+topping_id+')"><i class="fa fa-trash"></button></div>');
            //console.log("show modal option ="+option_topping_name+" "+option_topping_price);
        }
        function delete_topping(topping_id){
            $('.option_topping_name_'+topping_id+'').remove();
            $('.option_topping_price_'+topping_id+'').remove();
        }

        function print_products() {
            var category_id_all = "all";
            $.ajax({
                type : "POST",
                url:"{{ route('sort_category_submit') }}",
                //url : '/sort_category_submit',
                data : {
                  '_token' : $('input[name=_token]').val(),
                  'category_id' : category_id_all,
                },
                success:function(data){
                    console.log("จำนวน ที่หาเจอ ="+data.length+"="+data[0].name_category);
                    $(".all_pro").html("");
                    //console.log(hot_hit_color);
                    for (var i = 0 ; i < data.length; i++) {
                        //console.log(data[i].productImg);
                        if(data[i].productImg != ""){
                            var pro_img = "productImages/"+data[i].productImg;
                            //console.log(pro_img);
                            $(".all_pro").append('<div class="col-xl-3">'+
                                '<div class="ibox" data-toggle="modal" data-target="#pro_option" style="cursor: pointer;" data-id="'+data[i].id+'" data-name="'+data[i].name_th+'" data-price="'+data[i].price1+'.00" onclick="pro_option_detail('+data[i].id+',\''+data[i].name_th+'\','+data[i].price1+')">'+
                                    '<div class="ibox-content product-box">'+
                                        '<div class="product-imitation"><img src="'+pro_img+'" class="img-fluid"></div>'+
                                        '<div class="product-desc"><span class="product-price">'+data[i].price1+'.00</span>'+
                                        '<span class="product-price-bg-'+data[i].category_id+' pro_qty_'+data[i].id+'" data-pro_qty="'+data[i].quantity+'">'+data[i].name_category+'</span>'+
                                        '<small class="text-muted">'+data[i].name_en+'</small><a href="#" class="product-name" style="font-size: 15px;"> '+data[i].name_th+'</a><input type="hidden" name="pro_id"value="'+data[i].id+'"></div>'+
                                    '</div>'+
                                '</div><br></div>');
                        }else{
                            $(".all_pro").append('<div class="col-xl-3">'+
                                '<div class="ibox" data-toggle="modal" data-target="#pro_option" style="cursor: pointer;" data-id="'+data[i].id+'" data-name="'+data[i].name_th+'" data-price="'+data[i].price1+'.00" onclick="pro_option_detail('+data[i].id+',\''+data[i].name_th+'\','+data[i].price1+')">'+
                                    '<div class="ibox-content product-box">'+
                                        '<div class="product-imitation"><img src="productImages/no_image.png" class="img-fluid"></div>'+
                                        '<div class="product-desc"><span class="product-price">'+data[i].price1+'.00</span>'+
                                        '<span class="product-price-bg-'+data[i].category_id+' pro_qty_'+data[i].id+'" data-pro_qty="'+data[i].quantity+'">'+data[i].name_category+'</span>'+
                                        '<small class="text-muted">'+data[i].name_en+'</small><a href="#" class="product-name" style="font-size: 15px;"> '+data[i].name_th+'</a><input type="hidden" name="pro_id"value="'+data[i].id+'"></div>'+
                                    '</div>'+
                                '</div><br></div>');
                        }
                    }
                }
            });
        }

        //Check Save Suspend
        function check_suspend() {
            var user_id = $('input[name=user_id]').val();
            $.ajax({
                type : "POST",
                url : "{{route('check_suspend')}}",
                data : {
                  '_token' : $('input[name=_token]').val(),
                  'usr_id' : user_id,
                },
                success:function(data){
                    console.log("พักการขาย = "+data.id_suspend.length);
                    if(data.id_suspend.length < 1){
                        $('#icon_suspend').html("");
                    }else if(data.id_suspend.length == 1){
                        $('#icon_suspend').html("");
                        $('#icon_suspend').append('<i class="fa fa-file-text-o text-warning"></i>');
                    }else{
                        $('#icon_suspend').html("");
                        $('#icon_suspend').append('<i class="fa fa-files-o text-warning"></i>');
                    }
                }
            });
        }

        //pro_ticket_option
        function pro_ticket_option(trn_id,pro_id,pro_name) {
            $('.pro_ticket_option_name').html(pro_name);
            $('input[name=trn_id_ticket_option]').val(trn_id);
            $('input[name=pro_id_ticket_option]').val(pro_id);
            $('#pro_ticket_option').modal('show');
            console.log(" edit pro on tickety ="+trn_id+","+pro_id+","+pro_name);
        }

    </script>
@endsection
