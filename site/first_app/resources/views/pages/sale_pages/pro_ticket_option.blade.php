<div class="modal inmodal fade" id="pro_ticket_option" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="pro_ticket_option_name"></div>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <p class="font-bold">
                            Quantity
                        </p>
                        <input class="touchspin1" type="text" value="0" name="pro_quantity">
                        <input type="hidden" value="" name="trn_id_ticket_option">
                        <input type="hidden" value="" name="pro_id_ticket_option">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="pro_ticket_option_save"> Save</button>
            </div>
        </div>
    </div>
</div>
