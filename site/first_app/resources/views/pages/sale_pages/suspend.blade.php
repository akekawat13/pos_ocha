<div class="modal inmodal fade" id="open_suspend" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4>Sales hold list</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-6 bg-warning">No.</div>
                    <div class="col-sm-6 bg-warning text-center">Date Time</div>
                    <div class="w-100"></div>
                </div>
                <div class="row" id="opens_save_ticket">
                    {{-- <div class="col-sm-6 mt-1 mb-1 col-lg-6 border border-light bg-info" style="padding: 10px;">
                        <div class="form-check abc-radio abc-radio-success">
                            <input class="form-check-input" type="radio" name="trn_id" id="radio_'+data.data_all[i].id+'" value="'+data.data_all[i].id+'"><label class="form-check-label" for="radio_'+data.data_all[i].id+'">#ลำดับที่_'+order+'</label>
                        </div>
                    </div>
                    <div class="col-sm-6 mt-1 mb-1 text-center border border-light bg-info" style="padding: 10px;">'+data.data_all[i].created_at+'</div>
                    <div class="w-100"></div> --}}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="open_sale"> Open sales</button>
            </div>
        </div>
    </div>
</div>