@php

require_once __DIR__ . '/vendor/autoload.php';

$defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
$fontDirs = $defaultConfig['fontDir'];

$defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
$fontData = $defaultFontConfig['fontdata'];

$mpdf = new \Mpdf\Mpdf([
    'fontDir' => array_merge($fontDirs, [
        __DIR__ . '/tmp',
    ]),
    'fontdata' => $fontData + [
        'sarabun' => [
            'R' => 'THSarabun.ttf',
            'I' => 'THSarabun Italic.ttf',
            'B' =>  'THSarabun Bold.ttf',
        ]
    ],
    'default_font' => 'sarabun',
    'margin_top' => 0,
	'margin_left' => 0,
	'margin_right' => 0,
	'margin_bottom' => 0,
	'padding' => 0,
	'mirrorMargins' => true,
]);

// ob_start();
@endphp
<!DOCTYPE html>
<html>
<head>
	<!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

    <title></title>

	<style>
		/* Create three unequal columns that floats next to each other */
		/*.column_1 {
		  	float: left;
		  	padding: 2px;
		}
		.column_2 {
		  	float: right;
		  	padding: 2px;
		}*/

		/* Left and right column */
		/*.column.side {
		  	width: 25%;
		}*/

		/* Middle column */
		/*.column.middle {
		  	width: 50%;
		}*/
		/* Clear floats after the columns */
		/*.row:after {
		  	content: "";
		  	display: table;
		  	clear: both;
		}*/
	</style>
</head>
<body style="margin: 0 !important;font-size: 12px;">

    <div style="text-align: center;padding: 0px;width: 180px;" id="myPDF">
    	<div>
    		<img src="images/{{$data['receipt'][0]->shop_logo}}" alt="" width="30%">
    	</div>
    	<div id="shopNameTo">
    		{{$data['receipt'][0]->shop_name}}
    	</div>
    	<div id="addressTo">
    		{{$data['receipt'][0]->shop_address}}
    	</div>
    	<div id="taxTo">
    		เลขผู้เสียภาษี <span>{{$data['receipt'][0]->tax_id}}</span>
    	</div>
    	<hr>
    </div>

	<div style="padding: 0px;width: 180px;">

		@foreach($data['data_all'] as $key => $value)
			<table>
				<tr>
					<td style="width: 150px;" class="">
						{{$value->pro_name}} <span>X {{$value->pro_quantity}}</span>
		            	<div>
		            		<small>{{$value->size_name}}</small>
		            	</div>
					</td>
					<td style="width: 50px;">
						<div>
							<span class="font-weight-bold">{{$value->pro_price}}</span><br>
						</div>
						<div>
							<span>{{$value->size_price}}</span>
						</div>
					</td>
				</tr>
				 @if($value->toppings != "-")
	                @php
	                    $arr_toppings = json_decode($value->toppings);
	                @endphp
	                @foreach($arr_toppings as $key2 => $value2)
	                	<tr>
							<td style="width: 150px;" class="">{{$value2->name}}</td>
							<td style="width: 50px;">{{$value2->price}}.00</td>
						</tr>
	                @endforeach
	            @endif
			</table>

		@endforeach

		<hr>

    		<div class="row">
    			<table>
					<tr>
						<td style="width: 150px;margin-left: 10px;" class="">รวม</td>
						<td style="width: 50px;">
							@php
								echo $data['data_money'][0]->sum_total - $data['data_money'][0]->sum_vat.".00";
							@endphp
						</td>
					</tr>
					<tr>
						<td style="width: 150px;margin-left: 10px;" class="">vat</td>
						<td style="width: 50px;">7 %</td>
					</tr>
					<tr>
						<td style="width: 150px;margin-left: 10px;" class="">รวม vat</td>
						<td style="width: 50px;font-weight: bold;">
							{{$data['data_money'][0]->sum_total}}
						</td>
					</tr>
				</table>
			</div>

		<hr>

    	<div class="row">
    		<div class="col-sm-12" style="text-align: center;">
    			<div id="last_textTo">
		    		{{$data['receipt'][0]->last_text}}
		    	</div>
		    	<div id="phoneTo">
		    		โทร. {{$data['receipt'][0]->shop_phone}}
		    	</div>
    		</div>
    	</div>
            
	</div>

    @php
    	$html = ob_get_contents();
    	$mpdf->SetDisplayMode('fullwidth');
    	$mpdf->WriteHTML($html);
		$mpdf->Output("Myreceipt.pdf");
    	ob_end_flush();
    @endphp

    <button id="print" type="button" class="btn btn-sm btn-info"><i class="fa fa-print"></i> พิมพ์ตัวอย่าง</button>
	<!-- <button onclick="ExportPdf()" type="button" class="btn btn-outline-warning ml-3">
		<i class="far fa-file-pdf"></i> PDF Download
	</button> -->
	<a href="Myreceipt.pdf" class="btn btn-success btn-sm">PDF Download</a>

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>

    <!-- Mainly scripts -->
    <!-- <script src="js/jquery-3.1.1.min.js"></script>
    <script src="https://kendo.cdn.telerik.com/2017.2.621/js/jquery.min.js"></script>
 	<script src="https://kendo.cdn.telerik.com/2017.2.621/js/jszip.min.js"></script>
 	<script src="https://kendo.cdn.telerik.com/2017.2.621/js/kendo.all.min.js"></script> -->
    <script type="text/javascript">
    	//window.print();
		$('#print').click(function(){
			window.print();
		});
		// function ExportPdf(){ 

		// 	kendo.drawing
		// 	    .drawDOM("#myPDF", 
		// 	    { 
		// 	        paperSize: "A4",
		// 	        margin: { left: "0.2cm", right: "0.2cm", top: "0.1cm", bottom: "0.1cm" },
		// 	        scale: 0.65,
		// 	        height: 500
		// 	    })
		// 	        .then(function(group){
		// 	        kendo.drawing.pdf.saveAs(group, "Exported.pdf")
		// 	    });
		// }
	</script>
</body>
</html>