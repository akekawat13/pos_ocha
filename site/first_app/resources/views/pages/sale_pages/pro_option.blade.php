<div class="modal inmodal fade" id="pro_option" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                {{-- <h4 class="modal-title" id="name_pro"></h4> --}}
                <input type="hidden" name="id_pro" value="">
                <input type="hidden" name="name_pro" value="">
                <input type="hidden" name="price_pro" value="">
                <div class="row mt-2 make_toppings">
                    <div class="col-lg-6">
                        <div id="name_pro" style="font-size: 18px;font-weight: bold;"></div>
                    </div>
                    <div class="col-lg-5 text-center">
                        <div id="price_pro" style="font-size: 18px;font-weight: bold;"></div>
                    </div>
                    <div class="col-lg-1"></div>
                    <div class="col-lg-12"><hr> </div>
                    <div class="col-lg-6">
                        <div id="option_size_name" style="font-size: 18px;font-weight: bold;"></div>
                    </div>
                    <div class="col-lg-5 text-center">
                        <div id="option_size_price" style="font-size: 18px;font-weight: bold;"></div>
                    </div>
                    <div class="col-lg-1"></div>
                </div>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-6">
                        @foreach($data['option_size'] as $key => $value)
                            <button type="button" class="btn btn-w-m btn-default mt-1 mb-1 col-12" onclick="pro_option_size('{{$value->name}}',{{$value->price}})" style="height: 45px;" disabled>
                                <div class="row">
                                    <div class="col-sm-6">{{$value->name}}</div>
                                    <div class="col-sm-6">{{$value->price}}</div>
                                </div>
                            </button>
                        @endforeach
                    </div>

                    <div class="col-lg-6">
                        @foreach($data['option_topping'] as $key => $value)
                            <button type="button" class="btn btn-w-m btn-default mt-1 mb-1 col-12" onclick="pro_option_topping({{$value->id}},'{{$value->name}}',{{$value->price}})" style="height: 45px;">
                                <div class="row">
                                    <div class="col-sm-6">{{$value->name}}</div>
                                    <div class="col-sm-6">{{$value->price}}</div>
                                </div>
                            </button>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="save_changes"> Save changes</button>
            </div>
        </div>
    </div>
</div>
