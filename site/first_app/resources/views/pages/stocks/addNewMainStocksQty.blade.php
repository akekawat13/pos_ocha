<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-content p-md rounded">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="mb-2"><sub class="text-danger" style="font-size: 16px;">*</sub> ตามไฟล์ที่ Import เข้ามาใหม่ทั้งหมด</div>
                            <div class="mb-2"><sub class="text-danger" style="font-size: 16px;">*</sub> ไฟล์ .CSV เท่านั้น</div>
                            <form action="{{ url('/addNewMainStocksQty') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                @csrf
                                <input type="file" name="import_file" class="btn btn-warning btn-sm" />
                                <button id="upload" type="submit" class="btn btn-warning"><i class="fa fa-cloud-download" aria-hidden="true"></i> Import</button>
                            </form>
                            <div>
                            @if (!empty($error))
                            <br>
                                <div class="alert alert-danger">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                    <p style="font-size: 15px;">{{ $error }}</p>
                                </div>
                            @endif

                            @if (!empty($success))
                            <br>
                                <div class="alert alert-success">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                    <p style="font-size: 15px;">{{ $success }}</p>
                                </div>
                            @endif
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
            
        </div>
    </div>
</div>