@extends('layouts.appPeanutHome')
@php
	$dateThai = date("d/m/".(date("Y")+543));
	$barnch = DB::select('SELECT * FROM `branch` WHERE 1');
	//print_r($barnch);
@endphp
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>โอนสินค้า</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Home</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>โอนสินค้า</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
	<form action="{{url('/addStocksTransfer')}}" method="post">
		{{ csrf_field() }}
		<div class="row">
			<div class="form-group col-lg-4">
			    <label for="exampleFormControlSelect1">สาขา :</label>
			    <select class="form-control" id="exampleFormControlSelect1" name="barnch">
			    	<option>เลือกสาขา</option>
					@foreach ($barnch as $value)     
			      		<option value="{{ $value->id }}">{{ $value->shop_name }}</option>
			      	@endforeach
			    </select>
			</div>
			<div class="form-group col-lg-4">
			    <label for="">วันที่โอน :</label>
		      	<div class="form-group" id="data_1">
	                <div class="input-group date">
	                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
	                    <input type="text" class="form-control" value="{{date('Y/m/d')}}" name="dateTo" style="cursor: no-drop;" disabled>
	                    <input type="hidden" name="dateTo" value="{{date('Y/m/d')}}">
	                </div>
	            </div>
			</div>	
			<div class="form-group col-lg-4">
			    <label for="exampleFormControlSelect1">เลขที่ใบโอนสินค้า :</label>
			    <input type="text" class="form-control" value="{{date('ym')}}{{$number}}" style="cursor: no-drop;" disabled>
			    <input type="hidden" name="transferNumber" value="{{date('ym')}}{{$number}}">
			</div>

			<div class="form-group col-lg-4">
			    <label for="">ชื่อผู้โอนสินค้า :</label>
		      	<input type="text" class="form-control" name="nameTransfer" value="{{Auth::user()->name}}">
		      	<input type="hidden" class="form-control" name="user_id" value="{{Auth::id()}}">
			</div>	

			<div class="form-group col-lg-8">
			    <label for="">หมายเหตุ :</label>
		      	<input type="text" class="form-control" name="noteTransfer">
			</div>	
		</div>

	    <div class="row">
	    	<div class="col-lg-12 text-center">
	    		<button type="submit" class="btn btn-info"><i class="fa fa-sign-out"></i> ตกลง</button>
	    	</div>
	    </div>
	</form>
</div>
@endsection