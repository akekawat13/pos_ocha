@extends('layouts.appPeanutHome')
@php
$mainStock = DB::select('SELECT products.name_th,stocks.unit_1,stocks.unit_2,stocks.unit_3,stocks.products_id FROM stocks JOIN products ON products.id = stocks.products_id WHERE 1');
@endphp
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>สต๊อกสินค้า</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Home</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>สต๊อกสินค้าใหญ่</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeIn">
    <div class="row mb-5">
        <div class="col-lg-12">
            <div class="tabs-container">
                <ul class="nav nav-tabs" role="tablist">
                    <li><a class="nav-link " data-toggle="tab" href="#tab-1"><i class="fa fa-plus" aria-hidden="true"></i> รับเข้าสต๊อกใหม่</a></li>
                    <li><a class="nav-link active" data-toggle="tab" href="#tab-2"><i class="fa fa-retweet" aria-hidden="true"></i> ปรับสต๊อก</a></li>
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" id="tab-1" class="tab-pane ">
                        <div class="panel-body">
                            @include('pages.stocks.addNewMainStocksQty')
                        </div>
                    </div>

                    <div role="tabpanel" id="tab-2" class="tab-pane active">
                        <div class="panel-body">
                            @include('pages.stocks.editMainStocksQty')
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scriptStock')
    <!-- FooTable -->
    <script src="js_insa/plugins/footable/footable.all.min.js"></script>

    <script src="js_insa/plugins/dataTables/datatables.min.js"></script>
    <script src="js_insa/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
    <!-- Data picker -->
    <script src="js_insa/plugins/datapicker/bootstrap-datepicker.js"></script>
    <script type="text/javascript">

        $("#ttap-3").click(function(){
            location.reload();
        });

        $('.footable').footable();
        $('.footable2').footable();
        
        $('.dataTables-example').DataTable({
            pageLength: 25,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'copy'},
                {extend: 'csv'},
                {extend: 'excel', title: 'ExampleFile'},
                {extend: 'pdf', title: 'ExampleFile'},

                {extend: 'print',
                    customize: function (win){
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                        .addClass('compact')
                        .css('font-size', 'inherit');
                    }
                }
            ]

        });
        $('.dataTables-example2').DataTable({
            pageLength: 25,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                // {extend: 'copy'},
                // {extend: 'csv'},
                // {extend: 'excel', title: 'ExampleFile'},
                // {extend: 'pdf', title: 'ExampleFile'},

                // {extend: 'print',
                //     customize: function (win){
                //         $(win.document.body).addClass('white-bg');
                //         $(win.document.body).css('font-size', '10px');

                //         $(win.document.body).find('table')
                //         .addClass('compact')
                //         .css('font-size', 'inherit');
                //     }
                // }
            ]
        });

        function keyUp(pro_id){
            var unit1 = $('input[name=unit_1_'+pro_id+']').val();
            var unit2 = $('input[name=unit_2_'+pro_id+']').val();
            var unit3 = $('input[name=unit_3_'+pro_id+']').val();
            console.log(pro_id,unit1,unit2,unit3);
            $.ajax({
                type : "post",
                url : "{{route('updateMainstocks')}}",
                data : {
                    "_token":$('input[name=_token]').val(),
                    'products_id': pro_id,
                    'unit_1': unit1,
                    'unit_2': unit2,
                    'unit_3': unit3,
                },
                dataType: "json",
                success : function(result){
                    console.log(result);
                }
            });
        }

        // Date Picker
        var mem = $('#data_1 .input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
            format: 'dd/mm/yyyy',
        });

        //check add input
        $('.checkcustom').click(function(){
            var idProduct = $(this).val();
            $('.show_text'+idProduct+'').css('display','block');
            $( ".checkcustom" ).addClass( "customchecked" );
        });

        function keyUpTransferStock(pro_id,u){
            var unit = parseInt($('input[name=transferUnit_'+u+'_'+pro_id+']').val());
            var u_main_stk = parseInt($("#u_"+u+"_"+pro_id+"").text());

            if (unit > u_main_stk) {
                $('#u_'+u+'_'+pro_id).html(u_main_stk);
                alert('กรอกข้อมูลมากเกินไป');
            }else{
                var num1 = u_main_stk-unit;
                $('#u_'+u+'_'+pro_id).html(num1); 
                $('#s_'+u+'_'+pro_id).val(num1);
            }
            //console.log(u);
        }

    </script>
@endsection