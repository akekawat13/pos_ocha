<div class="wrapper wrapper-content animated fadeInRight">
    <form action="{{url('/addMoreMainStocksQty')}}" method="post">
    @csrf
    	<div class="row">
        	<div class="col-lg-6">
        		<div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-addon">ชื่อสินค้า</span>
                    </div>
                    <input type="text" placeholder="" class="form-control" name="pro_name" required>
                </div>
        	</div>
        	<div class="col-lg-2">
        		<div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-addon">Unit 1</span>
                    </div>
                    <input type="text" placeholder="" class="form-control" name="unit_1">
                </div>
        	</div>
        	<div class="col-lg-2">
        		<div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-addon">Unit 2</span>
                    </div>
                    <input type="text" placeholder="" class="form-control" name="unit_2">
                </div>
        	</div>
        	<div class="col-lg-2">
	            <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-addon">Unit 3</span>
                    </div>
                    <input type="text" placeholder="" class="form-control" name="unit_3">
                </div>
        	</div>
    	</div>
    	<div class="row">
    		<div class="col-lg-12 text-right mt-3">
    			<button type="submit" class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i> เพิ่มเข้าสต๊อก</button>
    		</div>
    	</div>
    </form>
</div>