@extends('layouts.appPeanutHome')
@php
    $mainStock = DB::select('SELECT products.name_th,stocks.unit_1,stocks.unit_2,stocks.unit_3,stocks.products_id FROM stocks JOIN products ON products.id = stocks.products_id WHERE 1');
    $dataTransfer = DB::select('SELECT * FROM `stocks_transfer` WHERE id = (SELECT MAX(id) FROM `stocks_transfer`)');
@endphp
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>โอนสินค้า</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Home</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>โอนสินค้า</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <form action="{{url('/addTransferBranch')}}" method="post" name="form1">
    {{ csrf_field() }}
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-content p-md rounded">
                    <div class="row">
                        @foreach ($dataTransfer as $key => $value)@endforeach
                        <input type="hidden" name="stocksTransferId" value="{{$value->id}}">
                        <div class="form-group col-lg-6">
                            <label for="">ชื่อผู้โอนสินค้า :</label>
                            <input type="text" class="form-control" value="{{Auth::user()->name}}" disabled>
                        </div>    
                        <div class="form-group col-lg-6">
                            <label for="">เลขที่ใบโอนสินค้า :</label>
                            <input type="text" class="form-control" value="{{$value->transferNumber}}" disabled>
                        </div>
                        <div class="form-group col-lg-6">
                            <label for="">วันที่โอน :</label>
                            <div class="form-group" id="data_1">
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="text" class="form-control" value="{{date('Y/m/d')}}" name="dateTo" style="cursor: no-drop;" disabled>
                                    <input type="hidden" name="dateTo" value="{{date('Y/m/d')}}">
                                </div>
                            </div>
                        </div>  
                    </div>

                    <table class="table table-striped table-bordered table-hover dataTables-example2" >
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Lists</th>
                                <th>Unit 1(s)</th>
                                <th>Unit 2</th>
                                <th>Unit 3</th>
                                <th>เลือก</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $i=1; @endphp
                            @foreach ($mainStock as $key => $value)
                            <tr>
                                <td class="text-center">{{$i++}}</td>
                                <td class="">{{$value->name_th}}</td>
                                <td class="">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon bg-secondary text-white" id="u_1_{{$value->products_id}}">{{$value->unit_1}}</span>
                                            <input type="hidden" name="summary_1_{{$value->products_id}}" id="s_1_{{$value->products_id}}">
                                            <input type="number" class="form-control show_text{{$value->products_id}}" name="transferUnit_1_{{$value->products_id}}" onblur="keyUpTransferStock({{$value->products_id}} ,'1');" style="display: none;">
                                        </div>
                                    </div>
                                </td>
                                <td class="">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon bg-secondary text-white" id="u_2_{{$value->products_id}}">{{$value->unit_2}}</span>
                                            <input type="hidden" name="summary_2_{{$value->products_id}}" id="s_2_{{$value->products_id}}">
                                            <input type="number" class="form-control show_text{{$value->products_id}}" name="transferUnit_2_{{$value->products_id}}" onblur="keyUpTransferStock({{$value->products_id}} ,'2');" style="display: none;">
                                        </div>
                                    </div>
                                </td>
                                <td class="">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon bg-secondary text-white" id="u_3_{{$value->products_id}}">{{$value->unit_3}}</span>
                                            <input type="hidden" name="summary_3_{{$value->products_id}}" id="s_3_{{$value->products_id}}">
                                            <input type="number" class="form-control show_text{{$value->products_id}}" name="transferUnit_3_{{$value->products_id}}" onblur="keyUpTransferStock({{$value->products_id}} ,'3');" style="display: none;">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input checkcustom" id="checkproductstock{{$value->products_id}}" value="{{$value->products_id}}" name="productsID[]">
                                        <label class="custom-control-label" for="checkproductstock{{$value->products_id}}"></label>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>No.</th>
                                <th>Lists</th>
                                <th>Unit 1(s)</th>
                                <th>Unit 2</th>
                                <th>Unit 3</th>
                                <th>เลือก</th>
                            </tr>
                        </tfoot>
                    </table>
                    <div class="col-lg-12 text-center">
                        <input type="hidden" value="0" name="status_id">
                        <button type="button" class="btn btn-info submit" value="1"><i class="fa fa-save"></i> Save Drafts</button>
                        <button type="button" class="btn btn-primary submit" value="2"><i class="fa fa-exchange"></i> Transfer</button>
                    </div>
                </div> 
            </div>      
        </div>
    </div>
    </form>
</div>
@endsection
@section('scriptStock-branch')
    <!-- FooTable -->
    <script src="js_insa/plugins/footable/footable.all.min.js"></script>

    <script src="js_insa/plugins/dataTables/datatables.min.js"></script>
    <script src="js_insa/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
    <!-- Data picker -->
    <script src="js_insa/plugins/datapicker/bootstrap-datepicker.js"></script>
    <script type="text/javascript">

        $('.footable').footable();
        $('.footable2').footable();
        
        $('.dataTables-example2').DataTable({
            pageLength: 25,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                // {extend: 'copy'},
                // {extend: 'csv'},
                // {extend: 'excel', title: 'ExampleFile'},
                // {extend: 'pdf', title: 'ExampleFile'},

                // {extend: 'print',
                //     customize: function (win){
                //         $(win.document.body).addClass('white-bg');
                //         $(win.document.body).css('font-size', '10px');

                //         $(win.document.body).find('table')
                //         .addClass('compact')
                //         .css('font-size', 'inherit');
                //     }
                // }
            ]
        });

        function keyUp(pro_id){
            var unit1 = $('input[name=unit_1_'+pro_id+']').val();
            var unit2 = $('input[name=unit_2_'+pro_id+']').val();
            var unit3 = $('input[name=unit_3_'+pro_id+']').val();
            console.log(pro_id,unit1,unit2,unit3);
            $.ajax({
                type : "post",
                url : '/updateMainstocks',
                data : {
                    "_token":$('input[name=_token]').val(),
                    'products_id': pro_id,
                    'unit_1': unit1,
                    'unit_2': unit2,
                    'unit_3': unit3,
                },
                dataType: "json",
                success : function(result){
                    console.log(result);
                }
            });
        }

        //check add input
        $('.checkcustom').click(function(){
            var idProduct = $(this).val();
            $('.show_text'+idProduct+'').css('display','block');
            $( ".checkcustom" ).addClass( "customchecked" );
        });

        function keyUpTransferStock(pro_id,u){
            var unit = parseInt($('input[name=transferUnit_'+u+'_'+pro_id+']').val());
            var u_main_stk = parseInt($("#u_"+u+"_"+pro_id+"").text());

            if (unit > u_main_stk) {
                $('#u_'+u+'_'+pro_id).html(u_main_stk);
                alert('กรอกข้อมูลมากเกินไป');
            }else{
                var num1 = u_main_stk-unit;
                $('#u_'+u+'_'+pro_id).html(num1); 
                $('#s_'+u+'_'+pro_id).val(num1);
            }
            //console.log(u);
        }

        $("form").submit(function(e){
            e.preventDefault();
            //alert($('sub1').val());
            this.submit();
        });

        $('.submit').click(function(){
            //console.log($(this).attr('value'));
            $('input[name=status_id]').attr('value',$(this).attr('value'));
            $("form[name=form1]").submit();
        });

    </script>
@endsection