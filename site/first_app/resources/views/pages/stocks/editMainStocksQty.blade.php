
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12 mb-3">
            <a href="{{url('/stockTransfer')}}" class="btn btn-success"> <i class="fa fa-upload" aria-hidden="true"></i> โอนสต๊อก</a>
        </div>
        <div class="col-lg-12">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Lists</th>
                            <th>Unit 1(s)</th>
                            <th>Unit 2</th>
                            <th>Unit 3</th>
                        </tr>
                    </thead>
                    <tbody>
                        {{ csrf_field() }}
                            @php $i=1; @endphp
                            @foreach ($mainStock as $key => $value)
                            <tr>
                                <td class="text-center">{{$i++}}</td>
                                <td class="">{{$value->name_th}}</td>
                                <td class="">
                                    <div class="row">
                                        <div class="col-md-5">
                                            {{-- <input type="hidden" name="id" value="{{$value->products_id}}" onkeyup="keyUp();"> --}}
                                            <input type="number" class="form-control" name="unit_1_{{$value->products_id}}" value="{{$value->unit_1}}" onkeyup="keyUp({{$value->products_id}});">
                                        </div>
                                    </div>
                                </td>
                                <td class="">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <input type="number" class="form-control" name="unit_2_{{$value->products_id}}" value="{{$value->unit_2}}" onkeyup="keyUp({{$value->products_id}});">
                                        </div>
                                    </div>
                                </td>
                                <td class="">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <input type="number" class="form-control" name="unit_3_{{$value->products_id}}" value="{{$value->unit_3}}" onkeyup="keyUp({{$value->products_id}});">
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>No.</th>
                            <th>Lists</th>
                            <th>Unit 1(s)</th>
                            <th>Unit 2</th>
                            <th>Unit 3</th>
                        </tr>
                    </tfoot>
                </table>
            </div>       
        </div>
    </div>
</div>