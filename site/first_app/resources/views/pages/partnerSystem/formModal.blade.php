{{-- form create --}}
    <div id="create" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form class="form-horizontal" role="form">
              <div class="form-group row">
                <label for="shopName" class="control-label col-sm-3">ชื่อร้าน :</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="shopName" name="shopName" required>
                  <p class="error text-center alert alert-danger" hidden></p>
                </div>
              </div>
              <div class="form-group row">
                <label for="address" class="control-label col-sm-3">ที่อยู่ :</label>
                <div class="col-sm-9">
                  <textarea  type="text" class="form-control" id="address" name="address" required></textarea>
                  <p class="error text-center alert alert-danger" hidden></p>
                </div>
              </div>
              <div class="form-group row">
                <label for="contactNumber" class="control-label col-sm-3">เบอร์ติดต่อ :</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="contactNumber" name="contactNumber" required>
                  <p class="error text-center alert alert-danger" hidden></p>
                </div>
              </div>
              <div class="form-group row">
                <label for="contactName" class="control-label col-sm-3">ชื่อผู้ติดต่อ :</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="contactName" name="contactName" required>
                  <p class="error text-center alert alert-danger" hidden></p>
                </div>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button class="btn btn-info" type="submit" id="add">
              <span class="fa fa-save"></span> บันทึก
            </button>
            <button id="colse_create" class="btn btn-danger" type="button" data-dismiss="modal">
              <span class="fa fa-times-circle"></span> ปิด
            </button>
          </div>
        </div>
      </div>
    </div>

    {{-- form Edit and Delete post --}}
    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form class="form-horizontal" role="modal">
              {{-- ID --}}
              <input type="text" class="form-control" id="fid" hidden>

              <div class="form-group row">
                <label for="" class="control-label col-sm-3">ชื่อร้าน</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="sN">
                </div>
              </div>
              <div class="form-group row">
                <label for="" class="control-label col-sm-3">ที่อยู่</label>
                <div class="col-sm-9">
                  <textarea type="text" class="form-control" id="aD"></textarea>
                </div>
              </div>
              <div class="form-group row">
                <label for="" class="control-label col-sm-3">เบอร์ติดต่อ</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="cNum" maxlength="10">
                </div>
              </div>
              <div class="form-group row">
                <label for="" class="control-label col-sm-3">ชื่อผู้ติดต่อ</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="cName">
                </div>
              </div>
            </form>

            {{-- Form Delete --}}
            <div class="deletecontent">
              ต้องการลบร้านค้า <span class="title"></span> ?
              <input type="hidden" class="id">
            </div>

          </div>
          <div class="modal-footer">
            <button class="btn actionBtn" type="button" data-dismiss="modal">
              <span id="footer_action_button" class="glyphicon"></span>
            </button>
            <button class="btn btn-warning" type="button" data-dismiss="modal">
              <span class="glyphicon glyphicon"></span>ปิด
            </button>
          </div>
        </div>
      </div>
    </div>
