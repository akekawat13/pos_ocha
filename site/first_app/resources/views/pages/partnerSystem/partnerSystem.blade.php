@extends('layouts.appPeanutHome')

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>ระบบคู่ค้า</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Home</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>ระบบคู่ค้า</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-content p-md rounded">
                    <div>
                        <a href="#" class="create-modal btn btn-primary btn-sm mb-3">
                          <i class="fa fa-plus"> เพิ่มคู่ค้า</i>
                        </a>
                    </div>
                    <div class="table table-responsive">
                        <table class="table table-bordered" id="table">
                            <tr>
                              <th class="text-center">No.</th>
                              <th class="text-center">ชื่อร้าน</th>
                              <th class="text-center">ที่อยู่</th>
                              <th class="text-center">เบอร์ติดต่อ</th>
                              <th class="text-center">ชื่อผู้ติดต่อ</th>
                              <th></th>
                            </tr>
                            {{ csrf_field() }}
                            <?php $no=1; ?>
                            @foreach ($customer as $key => $value)
                              <tr class="post{{ $value->id }}">
                                <td class="text-center">{{ $no++ }}</td>
                                <td>{{ $value->shopName }}</td>
                                <td>{{ $value->address }}</td>
                                <td class="text-center">{{ $value->contactNumber }}</td>
                                <td class="text-center">{{ $value->contactName }}</td>
                                <td class="text-center">
                                  <a href="#" class="edit-modal btn btn-warning btn-sm" data-id="{{ $value->id }}" data-shopname="{{ $value->shopName }}" data-address="{{ $value->address }}" data-contactnumber="{{ $value->contactNumber }}" data-contactname="{{ $value->contactName }}">
                                    <i class="fa fa-edit"></i> แก้ไขข้อมูล
                                  </a>

                                  <a href="#" class="delete-modal btn btn-danger btn-sm" data-id="{{ $value->id }}" data-shopname="{{ $value->shopName }}">
                                    <i class="fa fa-trash"></i> ลบ
                                  </a>
                                </td>
                              </tr>
                            @endforeach
                        </table>
                    </div><!-- ../class table -->

                </div>
            </div>
        </div>
    </div>
</div>

{{-- Form Modal --}}
  @include('pages.partnerSystem.formModal')

@endsection

@section('script')
    <script type="text/javascript">

        $(document).on('click','.create-modal',function() {
          $('#create').modal({backdrop: 'static', keyboard: false});
          $('#create').modal('show');
          // $('.form-horizontal').modal('show');
          $('.modal-title').text('เพิ่มคู่ค้า');

          $('#colse_create').on('click', function () {
              location.reload();
          });

        });

        $( document ).ready(function() {
          $(".f_focus").focus();
        });

        //function add(save)
        $("#add").click(function() {
          $.ajax({
            type : "post",
            url : "{{route('addPartnerSystem')}}",
            data : {
              '_token' : $('input[name=_token]').val(),
              'shopName' : $('input[name=shopName]').val(),
              'address' : $('textarea[name=address]').val(),
              'contactNumber' : $('input[name=contactNumber]').val(),
              'contactName' : $('input[name=contactName]').val(),
            },
            success : function(data){
                console.log("insert data ="+data.result);
                if ((data.error)) {
                    $('.error').removeClass('hidden');
                    $('error').text(data.error.shopName);
                    $('error').text(data.error.address);
                    $('error').text(data.error.contactNumber);
                    $('error').text(data.error.contactName);
                }else {
                    $('.error').remove();
                    $('#table').append("<tr class='post" + data.id +"'>"+
                    "<td>"+ data.id + "</td>"+
                    "<td>"+ data.shopName + "</td>"+
                    "<td>"+ data.address + "</td>"+
                    "<td class='text-center'>"+ data.contactNumber + "</td>"+
                    "<td class='text-center'>"+ data.contactName + "</td>"+
                    "<td class='text-center'><a href='#' class='edit-modal btn btn-warning btn-sm' data-id='"+ data.id +"'data-shopName='"+ data.shopName +"'data-address='"+ data.address +"'data-contactNumber='"+ data.contactNumber +"'data-contactName='"+ data.contactName +"'><i class='fa fa-edit'></i> แก้ไขข้อมูล</a>" +
                        "<a href='#' class='delete-modal btn btn-danger btn-sm' data-id='"+ data.id +"'data-shopName='"+ data.shopName +"'data-body='"+ data.body +"'><i class='fa fa-trash'></i> ลบ</a>" +
                    "</td>"+
                    "</tr>");
                    swal({
                        title: "บันทึกข้อมูลเรียบร้อย !",
                        type: "success",
                        showConfirmButton: false,
                        timer: 1500
                    });
                    // location.reload();
                }
            },
          });
          $('#shopName').val('');
          $('#address').val('');
          $('#contactNumber').val('');
          $('#contactName').val('');
        });

        //function Edit
        $(document).on('click','.edit-modal',function() {
          $('#footer_action_button').text("บันทึก");
          $('#footer_action_button').addClass('fa fa-check');
          $('#footer_action_button').removeClass('glyphicon-trash');
          $('.actionBtn').addClass('btn-success');
          $('.actionBtn').removeClass('btn-danger');
          $('.actionBtn').addClass('edit');
          $('.modal-title').text('แก้ไขข้อมูล');
          $('.deletecontent').hide();
          $('#fid').val($(this).data('id'));
          $('#sN').val($(this).data('shopname'));
          $('#aD').val($(this).data('address'));
          $('#cNum').val($(this).data('contactnumber'));
          $('#cName').val($(this).data('contactname'));
          $('#myModal').modal('show');
        });

        $('.modal-footer').on('click','.edit',function() {
          $.ajax({
            type : "POST",
            url : "{{route('updatePartnerSystem')}}",
            data : {
              '_token' : $('input[name=_token]').val(),
              'id' : $('#fid').val(),
              'shopName' : $('#sN').val(),
              'address' : $('#aD').val(),
              'contactNumber' : $('#cNum').val(),
              'contactName' : $('#cName').val()
            },
            success:function(data){
                swal({
                    title: "แก้ไขข้อมูลเรียบร้อย !",
                    type: "success",
                    showConfirmButton: false,
                    timer: 1500
                });
                console.log(data.result);
                location.reload();
            }
          });
        });

        //Form Delete Function
        $(document).on('click','.delete-modal',function() {
          $('#footer_action_button').text("ลบ");
          $('#footer_action_button').removeClass('glyphicon-check');
          $('#footer_action_button').addClass('fa fa-trash');
          $('.actionBtn').removeClass('btn-success');
          $('.actionBtn').addClass('btn-danger');
          $('.actionBtn').addClass('delete');
          $('.modal-title').text('ลบคู่ค้า');
          $('.deletecontent').show();
          $('.form-horizontal').hide();
          $('.id').text($(this).data('id'));
          $('.title').text($(this).data('shopname'));
          $('#myModal').modal('show');
        });

        $('.modal-footer').on('click','.delete',function() {
          $.ajax({
            type : "POST",
            url : "{{route('deletePartnerSystem')}}",
            data : {
              '_token' : $('input[name=_token]').val(),
              'id' : $('.id').text()
            },
            success:function(data){
                console.log("delete status ="+data.result);
              $('.post' + $('.id').text()).remove();
              location.reload();
            }
          });
        });
    </script>
@endsection
