@extends('layouts.appPeanutHome')
@php
    $mainCategory = DB::select('SELECT * FROM `category` WHERE 1');
@endphp
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>สินค้า</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Home</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>เพิ่มสินค้า</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeIn">
    <div class="row mb-5">
        <div class="col-lg-12">
            <div class="tabs-container">
                <ul class="nav nav-tabs" role="tablist">
                    <li><a class="nav-link active" data-toggle="tab" href="#tab-1"><i class="fa fa-plus" aria-hidden="true"></i> เพิ่มสินค้าแบบรายการเดียว</a></li>
                    <li><a class="nav-link" data-toggle="tab" href="#tab-2"><i class="fa fa-plus" aria-hidden="true"></i> เพิ่มสินค้าแบบหลายรายการ</a></li>
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" id="tab-1" class="tab-pane active">
                        <div class="panel-body">
                            @include('pages.products.addProductsOneProduct')
                        </div>
                    </div>

                    <div role="tabpanel" id="tab-2" class="tab-pane">
                        <div class="panel-body">
                            @include('pages.products.addProductsAllProducts')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scriptAddProducts')
    <!-- FooTable -->
    <script src="{{asset('js_insa/plugins/footable/footable.all.min.js')}}"></script>

    <script type="text/javascript">
        var loadFile = function(event) {
            var image = document.getElementById('output');
            image.src = URL.createObjectURL(event.target.files[0]);
        };
        
        $('.footable').footable();
        $('.footable2').footable();

    </script>
@endsection
