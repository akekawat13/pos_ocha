<div class="ibox ">
    <div class="ibox-content p-md rounded">
        <form action="{{url('/create_product')}}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-lg-1"></div>
                <div class="col-lg-4 text-center">
                    <p><input type="file" accept="image/*" name="productImg" id="productImg" onchange="loadFile(event)" style="display: none;"></p>
                    <p><label for="productImg" style="cursor: pointer;border: 1px dashed #16a085;padding: 20px;border-radius: 5px;"><i class="fa fa-upload fa-2x"></i> อัฟโหลดรูปสินค้า</label></p>
                    <p><img id="output" width="200" /></p>
                    {{-- <div>
                        <button id="" type="button" class="btn btn-sm btn-info mt-3"><i class="fa fa-random" aria-hidden="true"></i> เปลี่ยนรูปสินค้า</button>
                    </div>  --}}
                </div>
                <div class="col-lg-6">
                    <div class="form-group row"><label class="col-lg-4 col-form-label">ชื่อ สินค้าภาษาไทย</label>
                        <div class="col-lg-8"><input type="text" class="form-control" name="productNameTh" required>
                        </div>
                    </div>
                    <div class="form-group row"><label class="col-lg-4 col-form-label">ชื่อ สินค้าภาษาอังกฤษ</label>
                        <div class="col-lg-8"><input type="text" class="form-control" name="productNameEn"></div>
                    </div>
                    <div class="form-group row"><label class="col-lg-4 col-form-label">ขนาด</label>
                        <div class="col-lg-8"><input type="text" class="form-control"name="productSize" placeholder=""></div>
                    </div>
                    <div class="form-group row"><label class="col-lg-4 col-form-label">เลข อย.</label>
                        <div class="col-lg-8"><input type="number" class="form-control"name="fdaNumber" placeholder="000000"></div>
                        <!-- FDA = food and drug administration (เลข อย) -->
                    </div>
                    <div class="form-group row"><label class="col-lg-4 col-form-label">เลข บาร์โค๊ด</label>
                        <div class="col-lg-8"><input type="number" class="form-control"name="barcode" placeholder="000000"></div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-4 col-form-label">ประเภทสินค้าหลัก</label>
                        <div class="col-lg-4">
                            <select id="categor_id" name="mainCategory" class="form-control">
                                <option value="0" id="select_category">เลือกประเภท</option>
                                @for ($i = 0; $i < count($mainCategory); $i++)
                                    <option value="{{$i+1}}" id="select_category{{$i}}">{{$mainCategory[$i]->name}}</option>
                                @endfor
                            </select> 
                        </div>
                        <div class="col-lg-4">
                            <select id="addCategorySub" name="category_sub" class="form-control"></select> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="row">
                @for($i=1;$i<=5;$i++)
                    @if($i==1)
                    <div class="col">
                        <div class="form-group"><label>ราคาส่ง {{$i}}<span class="text-danger">*</span></label> <input type="number"  class="form-control" placeholder="0.00 บาท" name="wholesalePrice{{$i}}" required></div>
                    </div>
                    @endif
                    @if($i!=1)
                    <div class="col">
                        <div class="form-group"><label>ราคาส่ง {{$i}}</label> <input type="number"  class="form-control" placeholder="0.00 บาท" name="wholesalePrice{{$i}}"></div>
                    </div>
                    @endif
                @endfor
            </div>
            <div class="hr-line-dashed"></div>
            <div class="row">
                @for($i=1;$i<=5;$i++)
                    @if($i==1)
                        <div class="col">
                            <div class="form-group"><label>ราคาปลีก {{$i}}<span class="text-danger">*</span></label> <input type="number"  class="form-control" placeholder="0.00 บาท" name="retailPrice{{$i}}" required></div>
                        </div>
                    @endif
                    @if($i!=1)
                        <div class="col">
                            <div class="form-group"><label>ราคาปลีก {{$i}}</label> <input type="number"  class="form-control" placeholder="0.00 บาท" name="retailPrice{{$i}}"></div>
                        </div>
                    @endif
                @endfor
            </div>
            <div class="hr-line-dashed"></div>
            <div class="col-lg-12 text-center">
                <a href="{{ url('/home') }}" class="btn btn-danger"><i class="fa fa-times" aria-hidden="true"></i> ยกเลิก</a>
                <button type="submit" class="btn btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i> บันทึก</button>
            </div>
        </form>
    </div>
</div>

@section('script_addProductOne')
    
<script type="text/javascript">
    $("select[name=mainCategory]").change(function() {
        var value = "";
        $( "select option:selected" ).each(function() {
            var mainCategory = $("select[name=mainCategory]").val();
            console.log(mainCategory);
            $.ajax({
                type : "POST",
                url : "{{route('searchCategorySub')}}",
                data : {
                  '_token' : $('input[name=_token]').val(),
                  'id' : mainCategory,
                },
                success:function(data){
                    $("select[name=category_sub]").html('');
                    var test = JSON.stringify(data);
                    var datacount = data.length;
                    //console.log(typeof(datacount));     

                    for (var i = 0; i < datacount; i++) {
                        $("select[name=category_sub]").append(
                            '<option value="'+data[i].id+'">'+data[i].name+'</option>'
                        );
                    }
                }
            });  
        });
    });    

</script>
@endsection