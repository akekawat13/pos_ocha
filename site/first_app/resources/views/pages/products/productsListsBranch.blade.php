@extends('layouts.appPeanutHome')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>สินค้า</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Home</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>สินค้า</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-content p-md rounded">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-example">
                        <thead>
                            <tr>
                                <th class="text-center">ลำดับ</th>
                                <th>ชื่อไทย</th>
                                <th class="text-center">ราคาส่ง</th>
                                <th class="text-center">ราคาปลีก</th>
                                <th class="text-center">unit 1</th>
                                <th class="text-center">unit 2</th>
                                <th class="text-center">unit 3</th>
                            </tr>
                        </thead>
                        <tbody>
                            {{ csrf_field() }}
                            @php
                                $i=1;
                            @endphp
                            @foreach ($dataListsProductsBranch as $key => $value)
                            <tr>
                                <td class="text-center">{{$i++}}</td>
                                <td class="">{{$value->name_th}}</td>
                                <td class="text-center">{{$value->cost_price1}} บาท</td>
                                <td class="text-center">{{$value->price1}} บาท</td>
                                <td class="text-center">{{$value->unit_1}}</td>
                                <td class="text-center">{{$value->unit_2}}</td>
                                <td class="text-center">{{$value->unit_3}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('src_productBranch')
	<script src="js/plugins/dataTables/datatables.min.js"></script>
    <script src="js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},
                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');
                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]
            });
        });
    </script>
@endsection