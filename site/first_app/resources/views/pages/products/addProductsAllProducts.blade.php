
<div class="ibox ">
    <div class="ibox-content p-md rounded">
        <div class="col-lg-12">
            <form action="{{ url('importProducts') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                @csrf
                <input type="file" name="import_products" class="btn btn-warning btn-sm" />
                <button id="upload" type="submit" class="btn btn-warning"><i class="fa fa-cloud-download" aria-hidden="true"></i> Import</button>
            </form>
            <div>
                @if (!empty($error))
                <br>
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <p style="font-size: 15px;">{{ $error }}</p>
                    </div>
                @endif

                @if (!empty($success))
                <br>
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <p style="font-size: 15px;">{{ $success }}</p>
                    </div>
                    <div class="col-lg-12">
                        <input type="text" class="form-control form-control-sm m-b-xs" id="filter" placeholder="ค้าหา">
                    </div>
                    <table class="footable table table-stripped" data-page-size="25" data-filter=#filter>
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>barcode</th>
                                <th>product_code</th>
                                <th>ชื่อไทย</th>
                                <th>ชื่ออังกฤษ</th>
                                <th>ขนาด</th>
                                <th>category</th>
                                <th>sub_category</th>
                                <th>description</th>
                                <th>fda_number</th>
                                <th>quantity</th>
                                <th>ราคาส่ง 1</th>
                                <th>ราคาส่ง 2</th>
                                <th>ราคาส่ง 3</th>
                                <th>ราคาส่ง 4</th>
                                <th>ราคาส่ง 5</th>
                                <th>ราคาปลีก 1</th>
                                <th>ราคาปลีก 2</th>
                                <th>ราคาปลีก 3</th>
                                <th>ราคาปลีก 4</th>
                                <th>ราคาปลีก 5</th>
                                <th>Tax</th>
                            </tr>
                        </thead>
                        <tbody id="pro_data">
                            @foreach($dataProducts as $key => $value)
                            <tr>
                                {{-- <td>{{$key}}</td> --}}
                                @foreach($value as $key2 => $value2)
                                <td>{{$value2}}</td>
                                @endforeach
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="17">
                                    <ul class="pagination float-right"></ul>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                @endif
            </div>
        </div>
    </div>
</div>
