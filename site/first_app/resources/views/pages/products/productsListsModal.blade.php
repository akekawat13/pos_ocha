<!-- Modal View Product Detail-->
<div class="modal inmodal f_sarabun" id="viewProduct" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated fadeIn">
            <div class="modal-body">
               <div class="row">
                    <div class="col-md-5">
                        <div class="product-images">
                            <div>
                                <div class="image-imitation">
                                    <p id="Img"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="row">
                            <div class="col-md-6">
                                <h4>ชื่อภาษาไทย</h4>
                                <div class="text-muted">
                                    <p id="nTh"></p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <h4>ชื่อภาษาอังกฤษ</h4>
                                <div class="text-muted">
                                    <p id="nEn"></p>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <h4>ขนาด</h4>
                                <div class="text-muted">
                                    <p id="pZ"></p>
                                </div>
                                <h4>เลข บาร์โค๊ด</h4>
                                <div class="text-muted">
                                    <p id="bC"></p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <h4>เลข อย.</h4>
                                <div class="text-muted">
                                    <p id="FDA"></p>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <div>ราคาส่ง 1 :<span><input type="text" class="form-control form-control-sm" id="wsp1" disabled></span></div>
                                <div>ราคาส่ง 2 :<span><input type="text" class="form-control form-control-sm" id="wsp2" disabled></span></div>
                                <div>ราคาส่ง 3 :<span><input type="text" class="form-control form-control-sm" id="wsp3" disabled></span></div>
                                <div>ราคาส่ง 4 :<span><input type="text" class="form-control form-control-sm" id="wsp4" disabled></span></div>
                                <div>ราคาส่ง 5 :<span><input type="text" class="form-control form-control-sm" id="wsp5" disabled></span></div>
                            </div>
                            <div class="col-md-6">
                                <div>ราคาปลีก 1 :<span><input type="text" class="form-control form-control-sm" id="rtp1" disabled></span></div>
                                <div>ราคาปลีก 2 :<span><input type="text" class="form-control form-control-sm" id="rtp2" disabled></span></div>
                                <div>ราคาปลีก 3 :<span><input type="text" class="form-control form-control-sm" id="rtp3" disabled></span></div>
                                <div>ราคาปลีก 4 :<span><input type="text" class="form-control form-control-sm" id="rtp4" disabled></span></div>
                                <div>ราคาปลีก 5 :<span><input type="text" class="form-control form-control-sm" id="rtp5" disabled></span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                {{-- <a href="#" class="btn btn-warning editProducts" data-toggle="modal2" data-target="#editProduct" data-id="{{$value->id}}">แก้ไข</a> --}}
                <button type="button" class="btn btn-white" data-dismiss="modal">ปิด</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Edit Product -->
<div class="modal inmodal f_sarabun" id="editProduct" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated fadeIn">
            <form action="{{url('/updateProduct')}}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="modal-body">
                   <div class="row">
                    <input type="text" id="dataID" name="id" hidden>
                        <div class="col-md-5">
                            <div class="product-images text-center">
                                <p><input type="file" accept="image/*" name="editProductImg" id="productImg" onchange="loadFile(event)" style="display: none;"></p>
                                <p><label for="productImg" style="cursor: pointer;border: 1px dashed #16a085;padding: 20px;border-radius: 5px;"><i class="fa fa-upload fa-2x"></i> เปลี่ยนรูปสินค้า</label></p>
                                <p><img id="output" width="200" /></p>
                                <p id="editImg"></p>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="row">
                                <div class="col-md-6">
                                    <h4>ชื่อภาษาไทย</h4>
                                    <span><input type="text" class="form-control form-control-sm" id="editnTh" name="productNameTh"></span>
                                </div>
                                <div class="col-md-6">
                                    <h4>ชื่อภาษาอังกฤษ</h4>
                                    <span><input type="text" class="form-control form-control-sm" id="editnEn" name="productNameEn"></span>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <h4>ขนาด</h4>
                                    <span><input type="text" class="form-control form-control-sm" id="editpZ" name="productSize"></span>
                                    <h4>เลข บาร์โค๊ด</h4>
                                    <span><input type="text" class="form-control form-control-sm" id="editbC" name="barcode"></span>
                                </div>
                                <div class="col-md-6">
                                    <h4>เลข อย.</h4>
                                    <span><input type="text" class="form-control form-control-sm" id="editFDA" name="fdaNumber"></span>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <div>ราคาส่ง 1 :<span><input type="text" class="form-control form-control-sm" id="editwsp1" name="wholesalePrice1"></span></div>
                                    <div>ราคาส่ง 2 :<span><input type="text" class="form-control form-control-sm" id="editwsp2" name="wholesalePrice2"></span></div>
                                    <div>ราคาส่ง 3 :<span><input type="text" class="form-control form-control-sm" id="editwsp3" name="wholesalePrice3"></span></div>
                                    <div>ราคาส่ง 4 :<span><input type="text" class="form-control form-control-sm" id="editwsp4" name="wholesalePrice4"></span></div>
                                    <div>ราคาส่ง 5 :<span><input type="text" class="form-control form-control-sm" id="editwsp5" name="wholesalePrice5"></span></div>
                                </div>
                                <div class="col-md-6">
                                    <div>ราคาปลีก 1 :<span><input type="text" class="form-control form-control-sm" id="editrtp1" name="retailPrice1"></span></div>
                                    <div>ราคาปลีก 2 :<span><input type="text" class="form-control form-control-sm" id="editrtp2" name="retailPrice2"></span></div>
                                    <div>ราคาปลีก 3 :<span><input type="text" class="form-control form-control-sm" id="editrtp3" name="retailPrice3"></span></div>
                                    <div>ราคาปลีก 4 :<span><input type="text" class="form-control form-control-sm" id="editrtp4" name="retailPrice4"></span></div>
                                    <div>ราคาปลีก 5 :<span><input type="text" class="form-control form-control-sm" id="editrtp5" name="retailPrice5"></span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">ปิด</button>
                    {{-- <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="location.reload();">ปิด</button> --}}
                    <button type="submit" class="btn btn-success">บันทึก</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Edit Product -->
<div class="modal inmodal f_sarabun" id="delete" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content animated fadeIn">
            <div class="modal-body">
               <div class="row">
                    คุณต้องการลบ
               </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">ปิด</button>
                <button id="deleteProducts" type="button" class="btn btn-danger">ลบ</button>
            </div>
        </div>
    </div>
</div>