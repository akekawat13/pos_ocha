@extends('layouts.appPeanutHome')

@section('scr-datatable')
    <script src="js_insa/plugins/dataTables/datatables.min.js"></script>
    <script src="js_insa/plugins/dataTables/dataTables.bootstrap4.min.js"></script>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},
                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');
                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]
            });
        });

        // uploadimg
        var loadFile = function(event) {
            var image = document.getElementById('output');
            image.src = URL.createObjectURL(event.target.files[0]);
        };

        //$('#viewProduct').modal({backdrop: 'static', keyboard: false});

        // view product detail
        $('.viewProducts').click(function() {
            var id = $(this).data('id');
            // console.log(id);
            var as = "{{asset('/productImages/')}}";
            // alert(typeof(as));
            $.ajax({
                type : "POST",
                url : "{{route('viewProductDetail')}}",
                data : {
                  '_token' : $('input[name=_token]').val(),
                  'id' : id,
                },
                success:function(data){
                  $('#Img').html('<img src="'+as+'/'+data[0].productImg+'" width="200">');
                  $('#nTh').html(data[0].name_th);
                  $('#nEn').html(data[0].name_en);
                  $('#pZ').html(data[0].size);
                  $('#bC').html(data[0].barcode);
                  $('#FDA').html(data[0].fda_number);
                  $('#wsp1').val(data[0].cost_price1);
                  $('#wsp2').val(data[0].cost_price2);
                  $('#wsp3').val(data[0].cost_price3);
                  $('#wsp4').val(data[0].cost_price4);
                  $('#wsp5').val(data[0].cost_price5);
                  $('#rtp1').val(data[0].price1);
                  $('#rtp2').val(data[0].price2);
                  $('#rtp3').val(data[0].price3);
                  $('#rtp4').val(data[0].price4);
                  $('#rtp5').val(data[0].price5);
                }
            });
        });

        // edit product
        $('.editProducts').click(function() {
            var id = $(this).data('id');
            // console.log(id);
            var as = "{{asset('/productImages/')}}";
            $('#viewProduct').modal('hide');
            $('#editProduct').modal({backdrop: 'static', keyboard: false});

            $.ajax({
                type : "POST",
                url : "{{route('editProduct')}}",
                data : {
                  '_token' : $('input[name=_token]').val(),
                  'id' : id,
                },
                success:function(data){
                    //console.log(data[0].id);
                    $('#dataID').val(data[0].id);
                    $('#editImg').html('<img src="'+as+'/'+data[0].productImg+'" width="200">');
                    $('#editnTh').val(data[0].name_th);
                    $('#editnEn').val(data[0].name_en);
                    $('#editpZ').val(data[0].size);
                    $('#editbC').val(data[0].barcode);
                    $('#editFDA').val(data[0].fda_number);
                    $('#editwsp1').val(data[0].cost_price1);
                    $('#editwsp2').val(data[0].cost_price2);
                    $('#editwsp3').val(data[0].cost_price3);
                    $('#editwsp4').val(data[0].cost_price4);
                    $('#editwsp5').val(data[0].cost_price5);
                    $('#editrtp1').val(data[0].price1);
                    $('#editrtp2').val(data[0].price2);
                    $('#editrtp3').val(data[0].price3);
                    $('#editrtp4').val(data[0].price4);
                    $('#editrtp5').val(data[0].price5);
                }
            });
        });

        $('#productImg').click(function(){
            $('#editImg').hide();
        });

        // Update Products
        // $('#updateProduct').click(function(){
        //     //alert($('#dataID').val());
        //     var id = $('#dataID').val();

        //     $.ajax({
        //         type : "POST",
        //         enctype: 'multipart/form-data',
        //         url : '/updateProduct',
        //         data : {
        //           '_token' : $('input[name=_token]').val(),
        //           'id' : id,
        //           'productImg': $('input[name=editProductImg]').val(),
        //           'productNameTh': $('input[name=productNameTh]').val(),
        //           'productNameEn': $('input[name=productNameEn]').val(),
        //           'productSize': $('input[name=productSize]').val(),
        //           'barcode': $('input[name=barcode]').val(),
        //           'fdaNumber': $('input[name=fdaNumber]').val(),
        //           'wholesalePrice1': $('input[name=wholesalePrice1]').val(),
        //           'wholesalePrice2': $('input[name=wholesalePrice2]').val(),
        //           'wholesalePrice3': $('input[name=wholesalePrice3]').val(),
        //           'wholesalePrice4': $('input[name=wholesalePrice4]').val(),
        //           'wholesalePrice5': $('input[name=wholesalePrice5]').val(),
        //           'retailPrice1': $('input[name=retailPrice1]').val(),
        //           'retailPrice2': $('input[name=retailPrice2]').val(),
        //           'retailPrice3': $('input[name=retailPrice3]').val(),
        //           'retailPrice4': $('input[name=retailPrice4]').val(),
        //           'retailPrice5': $('input[name=retailPrice5]').val(),
        //         },
        //         success:function(data){
        //             console.log(data);
        //             swal({
        //                 title: 'อัฟเดทข้อมูลเรียบร้อย !',
        //                 type: "success",
        //                 confirmButtonColor: "#069224",
        //                 confirmButtonText: "ตกลง",
        //             });
        //             //location.reload();
        //         }
        //     });
        // });

        // Delete Products
        $('.deleteProducts').click(function(){
            var id = $(this).data('id');
            console.log("ID="+id);
            var confirm = false;
            swal({
                title: "Are you sure?",
                text: "Your will not be able to recover this imaginary file!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "ตกลง, ลบเลย!",
                cancelButtonColor: "#DD6B55",
                cancelButtonText: "ไม่, ย้อนกลับ!",
                closeOnConfirm: false,
                closeOnCancel: false },
                function (isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            type : "POST",
                            url : "{{route('deleteProduct')}}",
                            data : {
                              '_token' : $('input[name=_token]').val(),
                              'id' : id,
                            },
                            success:function(data){
                                console.log("DATA ID ="+data);
                            }
                        });
                        swal("Deleted!", "Your imaginary file has been deleted.", "success");
                        location.reload();
                    } else {
                        swal("Cancelled", "Your imaginary file is safe :)", "error");
                    }
            });
        });

        var N = 1;
        var L = ++N;
        /*console.log(L);*/

        //add stocks
        $('.checkcustom').click(function(){
            var idProduct = $(this).val();
            //console.log('idProduct ='+idProduct);
            $( "input[name=check]" ).removeClass( "checkcustom" ).addClass( "customchecked" );
            $.ajax({
                type : "POST",
                url : "{{route('addProductsStock')}}",
                data : {
                  '_token' : $('input[name=_token]').val(),
                  'id' : idProduct,
                },
                success:function(data){
                    console.log("Return ID ="+data);
                    if (data = "success") {
                        swal({
                            title: "เพิ่มข้อมูลเรียบร้อย!",
                            type: "success",
                            showConfirmButton: false,
                            timer: 1000,
                        });
                        location.reload();
                    }
                }
            });
        });

        $('.customchecked').click(function(){
            var idProduct = $(this).val();
            $( "input[name=check]" ).removeClass( "customchecked" ).addClass( "checkcustom" );
            $.ajax({
                type : "POST",
                url : "{{route('deleteProductsStock')}}",
                data : {
                  '_token' : $('input[name=_token]').val(),
                  'id' : idProduct,
                },
                success:function(data){
                    console.log("Delete ID ="+data);
                    if (data = "success") {
                        swal({
                            title: "ลบข้อมูลเรียบร้อย!",
                            type: "warning",
                            showConfirmButton: false,
                            timer: 1000,
                        });
                        location.reload();
                    }
                }
            });
        });

        //alert($('.checkcustom').val());

    </script>
@endsection

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>สินค้า</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Home</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>สินค้า</strong>
            </li>
        </ol>
    </div>
</div>
@if (session('status'))
    <div class="alert alert-success mt-5">
        <h4 class="f_sarabun">{{ session('status') }}</h4>
    </div>
@endif

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-content p-md rounded">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                            <tr>
                                <th class="text-center">ลำดับ</th>
                                {{-- <th>รูป</th> --}}
                                <th>ชื่อไทย</th>
                                <th>ชื่ออังกฤษ</th>
                                <th class="text-center">เพิ่มเข้าสต๊อกหลัก</th>
                                <th class="text-center">ราคาส่ง</th>
                                <th class="text-center">ราคาปลีก</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            {{ csrf_field() }}
                            @php
                                $i=1;
                                $ch_proid_instocks = DB::select('SELECT `products_id` FROM `stocks` WHERE 1');
                                if($ch_proid_instocks == null){
                                    //echo "null";
                                }else{
                                    //echo "Not null";
                                    foreach ($ch_proid_instocks as $key => $value) {
                                        //echo $value->products_id."<br>";
                                        $in_stock[] = $value->products_id;
                                    }
                                }
                            @endphp
                            @foreach ($productsLists as $key => $value)
                            <tr>
                                <td class="text-center">{{$i++}}</td>
                                {{-- <td class="text-center"><img src="{{asset('productImages/'.$value->productImg)}}" width="200"></td> --}}
                                <td class="">{{$value->name_th}}</td>
                                <td class="">{{$value->name_en}}</td>
                                <td class="text-center">
                                    @if($ch_proid_instocks != null && in_array($value->id, $in_stock))
                                        <div class="custom-control custom-checkbox" style="cursor: pointer;">
                                            <input type="checkbox" class="custom-control-input customchecked" id="checkproductstock{{$value->id}}" value="{{$value->id}}" name="check" checked="checked">
                                            <label class="custom-control-label" for="checkproductstock{{$value->id}}"></label>
                                        </div>
                                        @else
                                            <div class="custom-control custom-checkbox" style="cursor: pointer;">
                                                <input type="checkbox" class="custom-control-input checkcustom" id="checkproductstock{{$value->id}}" value="{{$value->id}}" name="check">
                                                <label class="custom-control-label" for="checkproductstock{{$value->id}}"></label>
                                            </div>
                                    @endif
                                </td>
                                <td class="text-center">{{$value->cost_price1}} บาท</td>
                                <td class="text-center">{{$value->price1}} บาท</td>
                                <td class="text-center">
                                    <a href="#" class="btn btn-info btn-sm viewProducts" data-toggle="modal" data-target="#viewProduct" data-id="{{$value->id}}">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                    <a href="#" class="btn btn-warning btn-sm editProducts" data-toggle="modal2" data-target="#editProduct" data-id="{{$value->id}}"><i class="fa fa-edit"></i></a>
                                    @if($ch_proid_instocks != null && in_array($value->id, $in_stock))
                                    <button type="button" class="btn btn-danger btn-sm deleteProducts" data-toggle="modal3" data-target="#delete" data-id="{{$value->id}}" disabled="disabled" style="cursor: no-drop;"><i class="fa fa-trash-o"></i></button>
                                        @else
                                        <button type="button" class="btn btn-danger btn-sm deleteProducts" data-toggle="modal3" data-target="#delete" data-id="{{$value->id}}"><i class="fa fa-trash-o"></i></button>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>

                        </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    {{-- Form Modal --}}
    @include('pages.products.productsListsModal')
@endsection
