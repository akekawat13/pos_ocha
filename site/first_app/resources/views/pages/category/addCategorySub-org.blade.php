<form action="{{ url('/addCategorySub') }}" method="post" accept-charset="utf-8">
	@csrf
	<div class="row align-items-center">
		<div class="form-group col-lg-3">
			<label>ประเภทสินค้าหลัก</label>
			<select id="categor_id" name="mainCategory" class="form-control">
				<option value="0" id="select_category">-- เลือกประเภทสินค้าหลัก --</option>
				@for ($i = 0; $i < count($mainCategory); $i++)
				    <option value="{{$i+1}}" id="select_category{{$i}}">{{$mainCategory[$i]->name}}</option>
				@endfor
			</select>  
		</div>
		<div class="form-group col-lg-3">
			<label>ประเภทสินค้าย่อย</label>
			<input type="text" class="form-control" name="category_sub">
			{{-- <select id="addCategorySub" name="category_sub" class="form-control"></select>  --}} 
		</div>

		<div class="form-group col-lg-3 mt-1">
			<button type="submit" class="btn btn-primary mt-4"><i class="fa fa-save"></i> Save</button>
		</div>
	</div>
</form>

<div class="row">
	<div class="col-lg-3">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>Clothes</h5>
            </div>
            <div class="ibox-content">
                <div class="alert alert-primary"><h5>เสื้อแขนสั้น <span class="float-right"><i class="fa fa-check"></i></span></h5></div>
            </div>
        </div>
    </div>
</div>

@section('script_category_sub')
	
<script type="text/javascript">
    //alert();
    $(document).ready(function(){
    	
    	// $('#addCategorySub .category_sub1').hide();
   
   //    	$("select[name=mainCategory]").change(function() {
			// var value = "";
			// $( "select option:selected" ).each(function() {
			//     value += $( this ).val();
			//     if (value == 1) {
			//     	$('.category_sub2').remove();$('.category_sub3').remove();$('.category_sub4').remove();$('.category_sub5').remove();
   //          		$("select[name=category_sub]").append(
	  //           	'<option class="category_sub1" value="เสื้อแขนสั้น">เสื้อแขนสั้น</option>'+'<option class="category_sub1" value="เสื้อแขนยาว">เสื้อแขนยาว</option>'
	  //           	);
   //          	}
   //          	if (value == 2) {
   //          		$('.category_sub1').remove();$('.category_sub3').remove();$('.category_sub4').remove();$('.category_sub5').remove();
   //          		$("select[name=category_sub]").append(
	  //           	'<option class="category_sub2" value="สมุนไพรน้ำ">สมุนไพรน้ำ</option>'+'<option class="category_sub2" value="สมุนไพรแห้ง">สมุนไพรแห้ง</option>'
		 //            );
	  //           }
	  //           if (value == 3) {
	  //           	$('.category_sub1').remove();$('.category_sub2').remove();$('.category_sub4').remove();$('.category_sub5').remove();
	  //           	$("select[name=category_sub]").append(
		 //            	'<option class="category_sub3" value="อาหารสด" selected>อาหารสด</option>'+'<option class="category_sub3" value="อาหารแห้ง">อาหารแห้ง</option>'
		 //            );
	  //           }
	  //           if (value == 4) {
	  //           	$('.category_sub1').remove();$('.category_sub2').remove();$('.category_sub3').remove();$('.category_sub5').remove();
   //          		$("select[name=category_sub]").append(
	  //           	'<option class="category_sub4" value="เครื่องแอลกอฮอร์" selected>เครื่องแอลกอฮอร์</option>'+'<option class="category_sub4" value="เครื่องดื่มทั่วไป">เครื่องดื่มทั่วไป</option>'
		 //            );
	  //           }
	  //           if (value == 5) {
	  //           	$('.category_sub1').remove();$('.category_sub2').remove();$('.category_sub3').remove();$('.category_sub4').remove();
	  //           	$("select[name=category_sub]").append(
		 //            	'<option class="category_sub5" value="เค้ก" selected>เค้ก</option>'+'<option class="category_sub5" value="ขนมปัง">ขนมปัง</option>'
		 //            );
	  //           }
			// });
   //          console.log("categor_id ="+value);
   //      });
   //      

    });
</script>
@endsection