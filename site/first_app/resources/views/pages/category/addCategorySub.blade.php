
@csrf
<div class="row align-items-center">
	<div class="form-group col-lg-3">
		<label>ประเภทสินค้าหลัก</label>
		<select id="categor_id" name="mainCategory" class="form-control">
			<option value="0" id="select_category">-- เลือกประเภทสินค้าหลัก --</option>
			@for ($i = 0; $i < count($mainCategory); $i++)
			    <option value="{{$i+1}}" id="select_category{{$i}}">{{$mainCategory[$i]->name}}</option>
			@endfor
		</select>  
	</div>
	<div class="form-group col-lg-3">
		<label>ประเภทสินค้าย่อย</label>
		<input type="text" class="form-control" name="category_sub"> 
	</div>

	<div class="form-group col-lg-1 mt-1">
		<button id="submit_category_sub" type="button" class="btn btn-primary mt-4"> Save</button>
	</div>

	<div class="col-lg-3" id="section_2">
		
	</div>
</div>

<div class="col-lg-12">
	@php
		$category_join_data = DB::select('SELECT  category.id as maim_id, category.name as main_cate, category_sub.* FROM category JOIN category_sub ON category.id = category_sub.category_id WHERE 1');
		$main_cate = DB::select('SELECT * FROM `category` WHERE 1');
		
		foreach ($main_cate as $key => $value) {
			//echo $value->name."==";
			//echo "<br><pre>";
			$i=0;
			foreach ($category_join_data as $key => $value_join) {
				//echo $value_join->main_cate.'['.$value_join->name."]<br>";
				$i++;
				if ($value->name == $value_join->main_cate) {
					$data[$value->name][$i]=$value_join->name;
				}
			}
			//echo "</pre><br>";
		}
	@endphp
</div>

<div class="row">
	@foreach($data as $key => $value)
		<div class="col-lg-3">
	        <div class="ibox ">
	            <div class="ibox-title bg-muted">
	                <h5>{{$key}}</h5>
	            </div>
	            <div class="ibox-content">
	            	<ul>
	            		@foreach($value as $key2 => $value2)
							<li><h5>{{$value2}}</h5></li>
	            		@endforeach
	            	</ul>
	            </div>
	        </div>
	    </div>
	@endforeach
</div>

@section('script_category_sub')
	
<script type="text/javascript">
   
    	$('#submit_category_sub').click(function(){
    		//alert();
            var category_id = $("select[name=mainCategory]").val();
            var category_sub = $("input[name=category_sub]").val();
            console.log("send id="+category_id+category_sub);
            $.ajax({
                type : "POST",
                url : "{{route('addCategorySub')}}",
                data : {
                  '_token' : $('input[name=_token]').val(),
                  'category_id' : category_id,
                  'category_sub': category_sub
                },
                success:function(data){
                    console.log("DATA  ="+data.dataCategorySub.id);
                    if (data.stt == 'success') {
                    	$('#section_2').html('<div class="alert alert-success alert-dismissable mt-3"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h5>เพิ่มข้อมูลเรียบร้อย</h5></div>');
                    	// $('#completed').append('<li class="info-element"><h4>'+data.max.name+'<button type="button" class="float-right btn btn-xs btn-danger deleteMainCategory" data-id="'+data.max.id+'"><i class="fa fa-trash"></i></button></h4></li>');
                    	location.reload();
                    }
                }
            });
           
        });

</script>
@endsection