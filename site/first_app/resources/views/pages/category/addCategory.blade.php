<div class="row">
	<div class="col-lg-1"></div>
	<div class="col-lg-6">
		@csrf
		<div class="row align-items-center">
			<div class="form-group col-lg-6">
				<label>ประเภทสินค้าหลัก</label>
				<input type="text" class="form-control" name="mainCategory">
			</div>

			<div class="form-group col-lg-6 mt-1">
				<button id="submit" class="btn btn-primary mt-4"><i class="fa fa-save"></i> Save</button>
			</div>
		</div>
		<div id="section"></div>
	</div>
	<div class="col-lg-4">
        <div class="ibox">
            <div class="ibox-content">
                <h4>รายการประเภทสินค้าหลัก <i class="fa fa-hand-o-down"></i></h4>
                <ul class="sortable-list connectList agile-list" id="completed">
                	@php
			     		$mainCategory = DB::select('SELECT * FROM `category` WHERE 1');
                	@endphp
                	@for ($i = 0; $i < count($mainCategory); $i++)
                		<li class="info-element">
	                        <h4>{{$mainCategory[$i]->name}} 
	                        	<button type="button" class="float-right btn btn-xs btn-danger deleteMainCategory" data-id="{{$mainCategory[$i]->id}}"><i class="fa fa-trash"></i></button>
	                        </h4>
	                    </li>
					@endfor
                </ul>
            </div>
        </div>

    </div>
</div>

@section('script_category')
<script type="text/javascript">
    //alert();
    $(document).ready(function(){
        //alert();
    	// ADD
    	$('#submit').click(function(){
            var categoryName = $('input[name=mainCategory]').val();
            //console.log("send name="+categoryName);
            $.ajax({
                type : "POST",
                url : "{{route('addCategory')}}",
                data : {
                  '_token' : $('input[name=_token]').val(),
                  'name' : categoryName,
                },
                success:function(data){
                    //console.log("DATA  ="+data.max.id);
                    if (data.stt == 'success') {
                    	$('#section').html('<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button><h4>เพิ่มข้อมูลเรียบร้อย</h4></div>');
                    	$('#completed').append('<li class="info-element"><h4>'+data.max.name+'<button type="button" class="float-right btn btn-xs btn-danger deleteMainCategory" data-id="'+data.max.id+'"><i class="fa fa-trash"></i></button></h4></li>');
                    	location.reload();
                    }
                }
            });
        });

        // DELETE
    	$('.deleteMainCategory').click(function(){
            var id = $(this).data('id');
            console.log("ID="+id);
            $.ajax({
                type : "POST",
                url : "{{route('deleteMainCategory')}}",
                data : {
                  '_token' : $('input[name=_token]').val(),
                  'id' : id,
                },
                success:function(data){
                    console.log("DATA ID ="+data);
                    location.reload();
                }
            });
        });

    });
</script>
@endsection