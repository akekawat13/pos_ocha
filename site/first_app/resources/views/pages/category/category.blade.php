@extends('layouts.appPeanutHome')

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>กลุ่มสินค้า</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Home</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>กลุ่มสินค้า</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeIn">
    <div class="row mb-5">
        <div class="col-lg-12">
            <div class="tabs-container">
                <ul class="nav nav-tabs" role="tablist" id="tab-menu">
                    <li><a class="nav-link " data-toggle="tab" href="#tab-1"><i class="fa fa-plus" aria-hidden="true"></i> เพิ่มประเภทสินค้าหลัก</a></li>
                    <li><a id="tab002" class="nav-link active" data-toggle="tab" href="#tab-2"><i class="fa fa-plus" aria-hidden="true"></i> เพิ่มประเภทสินค้าย่อย</a></li>
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" id="tab-1" class="tab-pane ">
                        <div class="panel-body">
                            @include('pages.category.addCategory')
                        </div>
                    </div>
                    <div role="tabpanel" id="tab-2" class="tab-pane active">
                        <div class="panel-body">
                            @include('pages.category.addCategorySub')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('sc_cat')
<script type="text/javascript">
    //alert();
    // $('ul#tab-menu li a').click(function(){
    //     $('li a').removeClass("active");
    //     $(this).addClass("active");
    // });
    // $('#tab002').click(function(){
    //     location.reload();
    //     $('#tab-1').removeClass("active");
    //     $('#tab002').addclass('active');
    //     $('#tab-2').addclass('active');
    // });
</script>

@endsection