@extends('layouts.appPeanutHome')
@php
	$posAdmin = array(1,10);
	// echo '<pre>';
	// print_r($data);
	// echo '</pre>';
@endphp
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>แก้ไขการโอนสินค้า</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Home</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>แก้ไขการโอนสินค้า</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
	<form action="{{url('updateStocksTransfer')}}" method="post" name="form1">
    {{ csrf_field() }}
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-content p-md rounded">
                    <div class="row">
                        @foreach ($data as $key => $value)@endforeach
                        <input type="hidden" name="stocksTransferId" value="{{$value->branch_id_des}}">
                        <div class="form-group col-lg-6">
                            <label for="">ชื่อผู้โอนสินค้า :</label>
                            <input type="text" class="form-control" value="{{Auth::user()->name}}" name="nameTransfer">
                        </div>    
                        <div class="form-group col-lg-6">
                            <label for="">เลขที่ใบโอนสินค้า :</label>
                            <input type="text" class="form-control" value="{{$value->transferNumber}}" disabled>
                        </div>
                        <div class="form-group col-lg-6">
                            <label for="">ชื่อผู้รับสินค้า :</label>
                            <input type="text" class="form-control" name="nameRecipient" value="{{$value->nameRecipient}}">
                        </div>
                        <div class="form-group col-lg-6">
                            <label for="">วันที่โอน :</label>
                            <div class="form-group" id="data_1">
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="text" class="form-control" value="{{date('Y/m/d')}}" style="cursor: no-drop;" disabled>
                                    <input type="hidden" name="dateTo" value="{{date('Y/m/d')}}">
                                </div>
                            </div>
                        </div>  
                        <div class="form-group col-lg-12">
                            <label for="">หมายเหตุการโอน :</label>
                            <input type="text" class="form-control" name="noteTransfer" value="{{$value->noteTransfer}}">
                        </div>
                    </div>

                    <table class="table table-striped table-bordered table-hover dataTables-example2" >
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Lists</th>
                                <th>Unit 1(s)</th>
                                <th>Unit 2</th>
                                <th>Unit 3</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $i=1; @endphp
                            @foreach ($data as $key => $value)
                            <tr>
                            	<input type="hidden" value="{{$value->products_id}}" name="productsID[]">
                                <td class="text-center">{{$i++}}</td>
                                <td class="">{{$value->name_th}}</td>
                                <td class="">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon bg-secondary text-white" id="u_1_{{$value->productsId}}">{{$value->unit_1}}</span>
                                            <input type="hidden" name="summary_1_{{$value->productsId}}" id="s_1_{{$value->productsId}}" value="{{$value->unit_1}}">
                                            <input type="number" class="form-control show_text{{$value->productsId}}" name="transferUnit_1_{{$value->productsId}}" onblur="keyUpTransferStock({{$value->productsId}} ,'1');" value="{{$value->transferUnit_1}}">
                                        </div>
                                    </div>
                                </td>
                                <td class="">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon bg-secondary text-white" id="u_2_{{$value->productsId}}">{{$value->unit_2}}</span>
                                            <input type="hidden" name="summary_2_{{$value->productsId}}" id="s_2_{{$value->productsId}}" value="{{$value->unit_2}}">
                                            <input type="number" class="form-control show_text{{$value->productsId}}" name="transferUnit_2_{{$value->productsId}}" onblur="keyUpTransferStock({{$value->productsId}} ,'2');" value="{{$value->transferUnit_2}}">
                                        </div>
                                    </div>
                                </td>
                                <td class="">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon bg-secondary text-white" id="u_3_{{$value->productsId}}">{{$value->unit_3}}</span>
                                            <input type="hidden" name="summary_3_{{$value->productsId}}" id="s_3_{{$value->productsId}}" value="{{$value->unit_3}}">
                                            <input type="number" class="form-control show_text{{$value->productsId}}" name="transferUnit_3_{{$value->productsId}}" onblur="keyUpTransferStock({{$value->productsId}} ,'3');" value="{{$value->transferUnit_3}}">
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>No.</th>
                                <th>Lists</th>
                                <th>Unit 1(s)</th>
                                <th>Unit 2</th>
                                <th>Unit 3</th>
                            </tr>
                        </tfoot>
                    </table>
                    <div class="col-lg-12 text-center">
                    	<a href="{{url('listStockTransfer')}}" class="btn btn-danger"><i class="fa fa-close"></i> ยกเลิก</a>
                        <input type="hidden" value="0" name="status_id">
                        <button type="button" class="btn btn-info submit" value="1"><i class="fa fa-save"></i> Save Drafts</button>
                        <button type="button" class="btn btn-primary submit" value="2"><i class="fa fa-exchange"></i> Transfer</button>
                    </div>
                </div> 
            </div>      
        </div>
    </div>
    </form>
</div>

@endsection

@section('scr-editStockstransfer')
	<!-- FooTable -->
    <script src="{{asset('js/plugins/footable/footable.all.min.js')}}"></script>

    <script src="{{asset('js/plugins/dataTables/datatables.min.js')}}"></script>
    <script src="{{asset('js/plugins/dataTables/dataTables.bootstrap4.min.js')}}"></script>
    <!-- Data picker -->
    <script src="{{asset('js/plugins/datapicker/bootstrap-datepicker.js')}}"></script>

    <script>
        $('.footable').footable();
        $('.footable2').footable();
        
        $('.dataTables-example2').DataTable({
            pageLength: 25,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                // {extend: 'copy'},
                // {extend: 'csv'},
                // {extend: 'excel', title: 'ExampleFile'},
                // {extend: 'pdf', title: 'ExampleFile'},

                // {extend: 'print',
                //     customize: function (win){
                //         $(win.document.body).addClass('white-bg');
                //         $(win.document.body).css('font-size', '10px');

                //         $(win.document.body).find('table')
                //         .addClass('compact')
                //         .css('font-size', 'inherit');
                //     }
                // }
            ]
        });

        function keyUp(pro_id){
            var unit1 = $('input[name=unit_1_'+pro_id+']').val();
            var unit2 = $('input[name=unit_2_'+pro_id+']').val();
            var unit3 = $('input[name=unit_3_'+pro_id+']').val();
            console.log(pro_id,unit1,unit2,unit3);
            $.ajax({
                type : "post",
                url : '/updateMainstocks',
                data : {
                    "_token":$('input[name=_token]').val(),
                    'products_id': pro_id,
                    'unit_1': unit1,
                    'unit_2': unit2,
                    'unit_3': unit3,
                },
                dataType: "json",
                success : function(result){
                    console.log(result);
                }
            });
        }

        function keyUpTransferStock(pro_id,u){
            var unit = parseInt($('input[name=transferUnit_'+u+'_'+pro_id+']').val());
            var u_main_stk = parseInt($("#u_"+u+"_"+pro_id+"").text());

            if (unit > u_main_stk) {
                $('#u_'+u+'_'+pro_id).html(u_main_stk);
                alert('กรอกข้อมูลมากเกินไป');
            }else{
                var num1 = u_main_stk-unit;
                $('#u_'+u+'_'+pro_id).html(num1); 
                $('#s_'+u+'_'+pro_id).val(num1);
            }
            //console.log(u);
        }

        $("form").submit(function(e){
            e.preventDefault();
            //alert($('sub1').val());
            this.submit();
        });

        $('.submit').click(function(){
            console.log($(this).attr('value'));
            $('input[name=status_id]').attr('value',$(this).attr('value'));
            $("form[name=form1]").submit();
        });
    </script>
@endsection