@extends('layouts.appPeanutHome')
@php
	$posAdmin = array(1,10);
	// echo '<pre>';
	// print_r($data);
	// echo '</pre>';
@endphp
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>โอนสินค้า</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Home</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>โอนสินค้า</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
	<form action="{{url('addStocksBranch')}}" method="post" name="form1">
    @csrf
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-content p-md rounded">
                    <div class="row">
                        @foreach ($data as $key => $value)@endforeach
                        <input type="hidden" name="stocks_transfer_id" value="{{$value->stocksTransferId}}">
                        <input type="hidden" name="branch_id_des" value="{{$value->branch_id_des}}">
                        <input type="hidden" name="status_id" value="{{$value->status_id}}">

                        <div class="form-group col-lg-6">
                            <label for="">ชื่อผู้โอนสินค้า :</label>
                            <input type="text" class="form-control" value="{{$value->name}}" name="nameTransfer" disabled>
                        </div>    
                        <div class="form-group col-lg-6">
                            <label for="">เลขที่ใบโอนสินค้า :</label>
                            <input type="text" class="form-control" value="{{$value->transferNumber}}" disabled>
                        </div>
                        <div class="form-group col-lg-6">
                            <label for="">วันที่โอน :</label>
                            <div class="form-group" id="data_1">
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="text" class="form-control" value="{{$value->dateTo}}" disabled>
                                </div>
                            </div>
                        </div>  
                        <div class="form-group col-lg-12">
                            <label for="">หมายเหตุการโอน :</label>
                            <input type="text" class="form-control" name="noteTransfer" value="{{$value->noteTransfer}}" disabled>
                        </div>
                    </div>

                    <table class="table table-striped table-bordered table-hover dataTables-example2" >
                        <thead>
                            <tr>
                                <th class="text-center">No.</th>
                                <th class="text-center">Lists</th>
                                <th class="text-center">Unit 1(s)</th>
                                <th class="text-center">Unit 2</th>
                                <th class="text-center">Unit 3</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $i=1; @endphp
                            @foreach ($data as $key => $value)
                            <tr>
                            	<input type="hidden" value="{{$value->products_id}}" name="products_id[]">
                                <td class="text-center">{{$i++}}</td>
                                <td class="">{{$value->name_th}}</td>
                                <td class="text-center">
                                    {{$value->transferUnit_1}}
                                    <input type="hidden" name="unit_1_{{$value->products_id}}" value="{{$value->transferUnit_1}}">
                                </td>
                                <td class="text-center">
                                    {{$value->transferUnit_2}}
                                    <input type="hidden" name="unit_2_{{$value->products_id}}" value="{{$value->transferUnit_2}}">
                                </td>
                                <td class="text-center">
                                    {{$value->transferUnit_3}}
                                    <input type="hidden" name="unit_3_{{$value->products_id}}" value="{{$value->transferUnit_3}}">
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th class="text-center">No.</th>
                                <th class="text-center">Lists</th>
                                <th class="text-center">Unit 1(s)</th>
                                <th class="text-center">Unit 2</th>
                                <th class="text-center">Unit 3</th>
                            </tr>
                        </tfoot>
                    </table>
                    <div class="row">
                        <div class="form-group col-lg-6">
                            <label for="">ชื่อผู้รับสินค้า :</label>
                            @if($value->status_id != 3)
                                <input type="text" class="form-control" required="required" value="ผู้จัดการสาขา 2">
                                <input type="hidden" name="name_recipient" value="{{Auth::id()}}">
                                @else
                                    @php
                                        $NameRecipient = DB::SELECT('SELECT users.name FROM `users` JOIN stocks_transfer ON users.id = stocks_transfer.nameRecipient WHERE users.id = 4');
                                    @endphp
                                    <input type="text" class="form-control" value="{{$NameRecipient[0]->name}}" disabled="disabled"> 
                            @endif
                        </div>
                        <div class="form-group col-lg-3">
                            <label for="">วันที่รับสินค้า :</label>
                            <div class="form-group" id="data_1">
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    @if($value->status_id != 3)
                                        <input type="text" class="form-control" value="{{date('Y/m/d')}}" style="cursor: no-drop;" disabled>
                                        <input type="hidden" name="reveive_date" value="{{date('Y/m/d')}}">
                                        @else
                                            <input type="text" class="form-control" value="{{$value->receiveDate}}" style="cursor: no-drop;" disabled>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 text-center">
                    	<a href="{{url('listStockTransfer')}}" class="btn btn-danger"><i class="fa fa-close"></i> ยกเลิก</a>
                        @if($value->status_id != 3)
                            <button type="submit" name="status_id" class="btn btn-primary submit" value="3"><i class="fa fa-exchange"></i> Receive</button>
                        @endif
                    </div>
                </div> 
            </div>      
        </div>
    </div>
    </form>
</div>

@endsection