@php
    $posAdmin = array(1,10);
    $dateThai = date("d/m/".(date("Y")+543));
    $date = explode('-',$data[0]->dateTo);
    $summary = 0;
@endphp
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>PDF</title>

	<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('font-awesome/css/font-awesome.css')}}" rel="stylesheet">

	<style>
		@font-face{
			font-family: 'CSChatThai';
			font-style: normal;
			font-weight: bold;
			src: url("{{ url('/fonts/CSchatThai/CSChatThaiUI.ttf') }}") format('truetype');
		}
		@font-face{
			font-family: 'CSChatThai';
			font-style: normal;
			font-weight: normal;
			src: url("{{ url('/fonts/CSchatThai/CSChatThai.ttf') }}") format('truetype');
		}
		body{
			font-family: "CSChatThai";
			font-size: 14px;
		}
	  	table{
	  		border-collapse: collapse;
	  	}
	  	table,tr,td,th{
	  		border:0.5px solid;
	  		padding: -0.25rem !important;
	  	}
	  	th{
	  		font-weight: 500;
	  		background-color: #ddd;
	  	}
	</style>
</head>
<body onload="ExportPdf()">
<div id="myCanvas">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-sm-3">
				<div class="text-center">
					<img src="{{asset('img/logo/logo-peanutHome-3.png')}}" alt="logo" class="img-fluid" width="50%">
				</div>
			</div>
			<div class="col-sm-6 text-center">
				@if($data[0]->Assignee == 0)
	            <div class="mt-2">
	                <h5>บริษัท บ้านถั่วลิสง จำกัด</h5>
	                <h6>113 หมู่ 2 ต.ผาสิงห์ อ.เมือง จ.น่าน 55000<br>โทร.xxxxxxxxx เลขประจำตัวผู้เสียภาษี xxxxxxxxxxx</h6>
	            </div>
	            @endif
			</div>
			<div class="col-sm-3 text-center mt-5">
				<div>
	        		<span>เลขที่เอกสาร :</span>
	            	<span>{{$data[0]->transferNumber}}</span>	
	        	</div>
	        	<div>
	        		<span>วันที่เอกสาร :</span>
	                <span>{{$date[2]}}/{{$date[1]}}/{{$date[0]}}</span>	
	        	</div>
			</div>
		</div>

		<div class="row mt-3">
	        <div class="col-sm-6">
	            <div>
	                @php
	                    $transferAdd = DB::select('SELECT users.name FROM users JOIN stocks_transfer ON users.id = stocks_transfer.user_id WHERE stocks_transfer.user_id = '.$data[0]->SenderName);
	                @endphp
	                <span>ผู้โอน : <strong>{{$transferAdd[0]->name}}</strong></span>
	            </div>
	            <div>
	                <span>หมายเหตุ : <strong>{{$data[0]->noteTransfer}}</strong></span>
	            </div>
	        </div>
	        <div class="col-sm-6">
	            <div>
	                @if($data[0]->RecipientName != Null)
	                @php
	                    $RecipientName = DB::select('SELECT users.name FROM users JOIN stocks_transfer ON users.id = stocks_transfer.user_id WHERE stocks_transfer.user_id = '.$data[0]->RecipientName);
	                @endphp
	                    <span>ผู้รับโอน : <strong>{{$RecipientName[0]->name}}</strong></span>
	                    @else
	                    <span>ผู้รับโอน : <strong> - </strong></span>
	                @endif
	            </div>
	            <div>
	                <span>สาขา : <strong>{{$data[0]->shop_name}}</strong></span>
	            </div>
	        </div>
	    </div>

		<div class="row mt-3">
	        <div class="col-sm-12">
                <table class="">
                    <thead>
                        <tr>
                            <th class="text-center" style="width: 300px;">ลำดับ</th>
                            <th class="text-center" style="width: 300px;">รายการสินค้า</th>
                            <th class="text-center" style="width: 300px;">Unit 1</th>
                            <th class="text-center" style="width: 300px;">Unit 2</th>
                            <th class="text-center" style="width: 300px;">Unit 3</th>
                            <th class="text-center" style="width: 300px;">ต้นทุนต่อหน่วย</th>
                            <th class="text-center" style="width: 300px;">จำนวนเงิน</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $key =>$value)
                        <tr>
                            <td class="text-center">{{$key+1}}</td>
                            <td class="text-center">{{$value->productsName}}</td>
                            <td class="text-center">{{$value->transferUnit_1}}</td>
                            <td class="text-center">{{$value->transferUnit_2}}</td>
                            <td class="text-center">{{$value->transferUnit_3}}</td>
                            <td class="text-center">{{ number_format($value->price1, 2) }}</td>
							@php
								$num1 = $value->transferUnit_1+$value->transferUnit_2+$value->transferUnit_3;
								$money_amount = $num1*$value->price1;
								$summary+= $money_amount;
							@endphp
							<td class="text-center">{{ number_format($money_amount, 2) }}</td>
						</tr>
                    	@endforeach
                    	<tr>
                    		<td colspan="5"></td>
                    		<th class="text-center">รวม</th>
                    		<th class="text-center">{{ number_format($summary, 2) }}</th>
                    	</tr>
                    </tbody>
                </table>
	        </div>
	    </div>
		
		<div class="row mt-5">
	        <div class="col-sm-3 text-center">
	        	<hr class="hr-line-solid">
	        	<div><b>ผู้บันทึกรายการ</b></div>
	        	<div class="float-left mt-2">วันที่ : <span class="ml-4">{{$date[2]}}/{{$date[1]}}/{{$date[0]}}</span></div>
	        </div>
	        <div class="col-sm-1"></div>
	        <div class="col-sm-3 text-center">
	            <hr class="hr-line-solid">
	        	<div><b>ผู้รับของ</b></div>
	        	<div class="float-left mt-2">วันที่ : </div>
	        </div>
	    </div>

	</div><!-- ../container -->
	
</div>

	{{-- ************************************************************************************* --}}
	<script src="https://kendo.cdn.telerik.com/2017.2.621/js/jquery.min.js"></script>
 	<script src="https://kendo.cdn.telerik.com/2017.2.621/js/jszip.min.js"></script>
 	<script src="https://kendo.cdn.telerik.com/2017.2.621/js/kendo.all.min.js"></script>

	<script>
	    function ExportPdf(){ 
			kendo.drawing
		    .drawDOM("#myCanvas", 
		    { 
		        paperSize: "A4",
		        margin: { left: "0.1cm", right: "0.1cm", top: "0.1cm", bottom: "0.1cm" },
		        scale: 0.74,
		        height: 500
		    })
		        .then(function(group){
		        kendo.drawing.pdf.saveAs(group, "Exported.pdf")
		    });
		}
	</script>
</body>
</html>