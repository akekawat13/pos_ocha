@extends('layouts.appPeanutHome')
@php
	$posAdmin = array(1,10);
	$dateThai = date("d/m/".(date("Y")+543));
	$date = explode('-',$data_report[0]->dateTo);
	$summary = 0;
@endphp
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2>ใบโอนสินค้า</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Home</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>ใบโอนสินค้า</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-4">
        <div class="title-action">
        	<a href="{{url('/pdfTransferPaper',$data_report[0]->stocks_transfer_ID)}}" target="_blank" class="btn btn-danger"><i class="fa fa-file-pdf-o"></i> Export PDF</a>
            {{-- <a href="{{url('/printTransferPaper',$data_report[0]->stocks_transfer_ID)}}" target="_blank" class="btn btn-primary"><i class="fa fa-print"></i> Print ใบโอนสินค้า</a> --}}
        </div>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="ibox-content">
		<div class="row align-items-center">
			<div class="col-lg-1"></div>
			<div class="col-lg-2 text-center">
				<img src="{{asset('img/logo/logo-peanutHome-3.png')}}" alt="logo" class="img-fluid">
			</div>
			<div class="col-lg-5 text-center">
				@if($data_report[0]->Assignee == 0)
				<div class="ml-5">
					<h3>บริษัท บ้านถั่วลิสง จำกัด</h3>
					<p>113 หมู่ 2 ต.ผาสิงห์ อ.เมือง จ.น่าน 55000<br>โทร.xxxxxxxxx เลขประจำตัวผู้เสียภาษี xxxxxxxxxxx</p>
				</div>
				@endif
			</div>
			<div class="col-lg-3">
				<div class="float-right">
					<table class="table invoice-total">
	                    <tbody>
	                    <tr>
	                        <td><strong>เลขที่เอกสาร :</strong></td>
	                        <td>{{$data_report[0]->transferNumber}}</td>
	                    </tr>
	                    <tr>
	                        <td><strong>วันที่เอกสาร :</strong></td>
	                        <td>{{$date[2]}}/{{$date[1]}}/{{$date[0]}}</td>
	                    </tr>
	                    </tbody>
	                </table>
				</div>
			</div>
		</div>

		<div class="row mt-3">
			<div class="col-lg-1"></div>
			<div class="col-lg-5 card card-body">
				<div>
					@php
						$transferAdd = DB::select('SELECT users.name FROM users JOIN stocks_transfer ON users.id = stocks_transfer.user_id WHERE stocks_transfer.user_id = '.$data_report[0]->SenderName);
					@endphp
					<span><strong>ผู้โอน :</strong> {{$transferAdd[0]->name}}</span>
				</div>
				<div>
					<span><strong>หมายเหตุ :</strong> {{$data_report[0]->noteTransfer}}</span>
				</div>
			</div>
			<div class="col-lg-5 card card-body">
				<div>
					@if($data_report[0]->RecipientName != Null)
					@php
						$RecipientName = DB::select('SELECT users.name FROM users JOIN stocks_transfer ON users.id = stocks_transfer.user_id WHERE stocks_transfer.user_id = '.$data_report[0]->RecipientName);
					@endphp
						<span><strong>ผู้รับโอน :</strong> {{$RecipientName[0]->name}}</span>
						@else
						<span><strong>ผู้รับโอน :</strong> -</span>
					@endif
				</div>
				<div>
					<span><strong>สาขา :</strong> {{$data_report[0]->shop_name}}</span>
				</div>
			</div>
		</div>

		<div class="row mt-3">
			<div class="col-lg-1"></div>
			<div class="col-lg-10 card card-body">
				<div class="table-responsive m-t">
                    <table class="table invoice-table">
                        <thead>
	                        <tr>
	                            <th class="text-center">ลำดับ</th>
	                            <th class="text-center">รายการสินค้า</th>
	                            <th class="text-center">Unit 1</th>
	                            <th class="text-center">Unit 2</th>
	                            <th class="text-center">Unit 3</th>
	                            <th class="text-center">ต้นทุนต่อหน่วย</th>
	                            <th class="text-center">จำนวนเงิน</th>
	                        </tr>
                        </thead>
                        <tbody>
                        	@foreach($data_report as $key =>$value)
							<tr>
								<td class="text-center">{{$key+1}}</td>
								<td class="text-center">{{$value->productsName}}</td>
								<td class="text-center">{{$value->transferUnit_1}}</td>
								<td class="text-center">{{$value->transferUnit_2}}</td>
								<td class="text-center">{{$value->transferUnit_3}}</td>
								<td class="text-center">{{ number_format($value->price1, 2) }}</td>
								@php
									$num1 = $value->transferUnit_1+$value->transferUnit_2+$value->transferUnit_3;
									$money_amount = $num1*$value->price1;
									$summary+= $money_amount;
								@endphp
								<td class="text-center">{{ number_format($money_amount, 2) }}</td>
							</tr>
                        	@endforeach
                        	<tr>
                        		<td colspan="5"></td>
                        		<th class="text-center">รวม</th>
                        		<th class="text-center">{{ number_format($summary, 2) }} <hr class="hr-line-solid"></th>
                        	</tr>
                        </tbody>
                    </table>
                </div><!-- /table-responsive -->
			</div>
		</div>

		<div class="row mt-3">
			<div class="col-lg-1"></div>
			<div class="col-lg-4 card card-body">
				<table class="table">
                    <tbody>
                    <tr>
                        <td><strong>ผู้บันทึกรายการ :</strong></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td><strong>วันที่ :</strong></td>
                        <td>{{$date[2]}}/{{$date[1]}}/{{$date[0]}}</td>
                    </tr>
                    </tbody>
                </table>
			</div>
			<div class="col-lg-2"></div>
			<div class="col-lg-4 card card-body">
				<table class="table">
                    <tbody>
                    <tr>
                        <td><strong>ผู้รับของ :</strong></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td><strong>วันที่ :</strong></td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
			</div>
		</div>
	</div>
</div>
@endsection