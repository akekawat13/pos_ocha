@extends('layouts.appPeanutHome')
@php
	$posAdmin = array(1,10);
	$dateThai = date("d/m/".(date("Y")+543));
	if(in_array(Auth::user()->is_admin, $posAdmin)){
		$dataStockTransfer = DB::select('SELECT * FROM `stocks_transfer` WHERE 1');
	}else{
		$dataStockTransfer = DB::select('SELECT stocks_transfer.* FROM stocks_transfer JOIN pos_user_branch ON pos_user_branch.branch_id = stocks_transfer.branch_id_des JOIN users ON users.id = pos_user_branch.user_id WHERE users.id = '.Auth::id());
	}
@endphp
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>รายการโอนสินค้า</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Home</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>รายการโอนสินค้า</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="ibox-content">
		<div class="row">
			@if(in_array(Auth::user()->is_admin, $posAdmin))
			<div class="col-lg-12 mb-3">
				<a href="{{url('stockTransfer')}}" class="btn btn-primary"><i class="fa fa-plus"></i> เพิ่มรายการโอนสินค้า</a>
			</div>
			@endif
			<div class="col-lg-12">
				<div class="table-responsive">
	                <table class="table table-striped table-bordered table-hover dataTables-example" >
	                    <thead>
	                        <tr class="text-center">
					            <th>เลขที่เอกสาร</th>
					            <th>จาก</th>
					            <th>ไปยังสาขา</th>
					            <th>Status</th>
					            <th>Action</th>
					        </tr>
	                    </thead>
	                    <tbody>
	                    	@foreach($dataStockTransfer as $key =>$value)
							@if(in_array(Auth::user()->is_admin, $posAdmin))
							<tr>
								<td class="text-center">{{$value->transferNumber}}</td> 
								<td class="text-center">
									@if($value->branch_id_src == 0)
										Main Stocks
									@endif
								</td>
								<td class="text-center">
									@php
										$des_name = DB::select('SELECT branch.shop_name FROM `stocks_transfer` JOIN branch ON stocks_transfer.branch_id_des = branch.id WHERE branch_id_des = '.$value->branch_id_des);
									@endphp
									{{$des_name[0]->shop_name}}
								</td>
								<td class="text-center">
									@php
										$dataStatus = DB::select('SELECT stocks_transfer_status.* FROM `stocks_transfer_status` JOIN stocks_transfer ON stocks_transfer_status.id = stocks_transfer.status_id WHERE stocks_transfer_status.id ='.$value->status_id);
									@endphp
									{{$dataStatus[0]->title}}
								</td>
								<td class="text-center">
									@if($value->status_id == 1)
										<a href="{{url('/editStockTransfer',$value->id)}}" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i></a>
									@endif
									@if($value->status_id == 2)
										<a href="{{url('/transferPaperReport',$value->id)}}" class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="top" title="ใบโอนสินค้า"><i class="fa fa-file-o"></i></a>
									@endif
									<a href="{{url('/viewStockTransfer',$value->id)}}" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
								</td>
							</tr>
							@endif

							@if(Auth::user()->is_admin != in_array(Auth::user()->is_admin, $posAdmin))
								@if($value->status_id != 1)
								<tr>
									<td class="text-center">{{$value->transferNumber}}</td> 
									<td class="text-center">
										@if($value->branch_id_src == 0)
											Main Stocks
										@endif
									</td>
									<td class="text-center">
										@php
											$des_name = DB::select('SELECT branch.shop_name FROM `stocks_transfer` JOIN branch ON stocks_transfer.branch_id_des = branch.id WHERE branch_id_des = '.$value->branch_id_des);
										@endphp
										{{$des_name[0]->shop_name}}
									</td>
									<td class="text-center">
										@php
											$dataStatus = DB::select('SELECT stocks_transfer_status.* FROM `stocks_transfer_status` JOIN stocks_transfer ON stocks_transfer_status.id = stocks_transfer.status_id WHERE stocks_transfer_status.id ='.$value->status_id);
										@endphp
										{{$dataStatus[0]->title}}
									</td>
									<td class="text-center">
										<a href="{{url('/viewStockTransfer',$value->id)}}" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>	
									</td>
								</tr>
								@endif
							@endif
							@endforeach
	                    </tbody>
	                </table>
	            </div>
				
			</div>
		</div>
	</div>
</div>
@endsection

@section('scriptStockTransfer')
	<!-- FooTable -->
    <script src="js_insa/plugins/footable/footable.all.min.js"></script>

    <script src="js_insa/plugins/dataTables/datatables.min.js"></script>
    <script src="js_insa/plugins/dataTables/dataTables.bootstrap4.min.js"></script>

    <script type="text/javascript">

        $('.footable').footable();
        $('.footable2').footable();
        
        $('.dataTables-example').DataTable({
            pageLength: 25,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                // {extend: 'copy'},
                // {extend: 'csv'},
                // {extend: 'excel', title: 'ExampleFile'},
                // {extend: 'pdf', title: 'ExampleFile'},

                // {extend: 'print',
                //     customize: function (win){
                //         $(win.document.body).addClass('white-bg');
                //         $(win.document.body).css('font-size', '10px');

                //         $(win.document.body).find('table')
                //         .addClass('compact')
                //         .css('font-size', 'inherit');
                //     }
                // }
            ]
        });
    </script>
@endsection